<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CMF extends Model
{
    use HasFactory;
    protected $table = 'tr_cmf';
    protected $fillable = [
        "kodeCMF",
        "pemilikProsesCMF",
        "dateCMF",
        "departmentCMF",
        "areaCMF",
        "judulPerubahanCMF",
        "dateImplementasiCMF",
        "typePerubahanCMF",
        "alasanPerubahanCMF",
        "dampakPerubahanCMF",
        "deskripsiPerubahanCMF",
        "txtUploadDocument",
        "encryKodeCMF",
        "nomorCMF",
        "approveCMF1",
        "nomorCapex",
        "statusProsesCMF",
        "bitActive",
        "txtInsertedBy",
        "dtmInsertedDate",
    ];
    const UPDATED_AT = null; //and updated by default null set
    const CREATED_AT = null; //and updated by default null set
}
