<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class Auth extends Controller
{
    public function index(){
        return view('Auth.login');
    }

    public function actionlogin(Request $request){
        $nik = $request->nik;
        $password = $request->password;
        
        $password = md5(sha1(crc32($password)));
        $user = DB::table('m_user')->where('nik', $nik)->first();

        if($user){
            if($user->password == $password){
                if($user->bitActive ==1){
                    $data = [
                        'cmf_nik' => $user->nik,
                        'cmf_levelCMF' => $user->levelCMF,
                        'cmf_namaKaryawan' => $user->namaKaryawan,
                        'cmf_department' => $user->department,
                        'cmf_area' => $user->area,
                    ];
                    $request->session()->put($data);
                    if($user->levelCMF == 'LVL01'){
                        return redirect('Pic/home');  
                    }else if($user->levelCMF == 'LVL02'){
                        return redirect('Depthead/home');
                    }else if($user->levelCMF == 'LVL03'){
                        return redirect('Verifikator/home');
                    }else if($user->levelCMF == 'LVL04'){
                        return redirect('Mr/home');
                    }else if($user->levelCMF == 'LVL05'){
                        return redirect('Manufacturing/home');
                    }else if($user->levelCMF == 'LVL06'){
                        return redirect('Dc/home');
                    }else{
                        return redirect("/")->withFail('Your account has not activated in CMF Online !');
                    }
                }else{
                    return redirect("/")->withFail('Your account has not activated in CMF Online !');
                }
            }else{
                return redirect("/")->withFail('Password is not registered !');
            }
        }else{
            return redirect("/")->withFail('NIK is not registered in CMF Online !');
        }
    }

    public function DashboardCMF(Request $request){
        $statusProsesCMF = array(-1, -2, -4, -5, -6, -7, -8);
        $data['cmf'] = DB::table('tr_cmf')
                          ->selectRaw('nomorCMF, tr_cmf.dtmInsertedDate, namaKaryawan, namaDepartment, statusProsesCMF')
                          ->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
                          ->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
                          ->whereIn('statusProsesCMF', $statusProsesCMF)
                          ->get();
        $data['cmf2'] = DB::table('tr_cmf')
                          ->select(DB::raw('COUNT(kodeCMF) as jumlahCMF'), 'namaDepartment')
                          ->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
                          ->where('statusProsesCMF', 1)
                          ->groupBy('departmentCMF')
                          ->get();
        $data['cmf3'] = DB::table('tr_department_terkait')
                          ->selectRaw('namaDepartment, kodeDepartmentCMF')
                          ->join('m_department', 'tr_department_terkait.kodeDepartmentCMF', '=', 'm_department.kodeDepartmentMaster')
                            ->join('tr_cmf', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
                            ->where('statusProsesCMF', 2)
                          ->where('adjustmentDepartment1CMF', 0)
                          ->orderBy('namaDepartment', 'ASC')
                          ->distinct()
                          ->get();
        $statusProses4 = array(3, 4, 5);
        $data['cmf4'] = DB::table('tr_cmf')
                            ->selectRaw('nomorCMF, tr_cmf.dtmUpdatedDate, namaKaryawan, namaDepartment, statusProsesCMF')
                            ->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
                            ->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
                            ->whereIn('statusProsesCMF', $statusProses4)
                            ->get();
        $data['cmf5'] = DB::table('tr_cmf')
                            ->selectRaw('nomorCMF, tr_cmf.dateApproveCMF4, namaKaryawan, namaDepartment, statusProsesCMF')
                            ->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
                            ->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
                            ->where('statusProsesCMF', 6)
                            ->get();
        $statusProses6 = array(-9, -10, -11, -12, -13, 7);
        $data['cmf6'] = DB::table('tr_cmf')
                            ->selectRaw('nomorCMF, tr_cmf.dateApproveCMF5, namaKaryawan, namaDepartment, statusProsesCMF')
                            ->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
                            ->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
                            ->whereIn('statusProsesCMF', $statusProses6)
                            ->get();
        $data['cmf7'] = DB::table('tr_cmf')
                            ->select(DB::raw('COUNT(kodeCMF) as jumlahCMF'), 'namaDepartment')
                            ->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
                            ->where('statusProsesCMF', 8)
                            ->groupBy('namaDepartment')
                            ->get();
        $data['cmf8'] = DB::table('tr_department_terkait')
                            ->selectRaw('namaDepartment, kodeDepartmentCMF')
                            ->join('m_department', 'tr_department_terkait.kodeDepartmentCMF', '=', 'm_department.kodeDepartmentMaster')
                            ->join('tr_cmf', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
                            ->where('statusProsesCMF', 9)
                            ->where('adjustmentDepartment2CMF', 0)
                            // ->orderBy('namaDepartment', 'ASC')
                            ->distinct()
                            ->get();
        $statusProses9 = array(10, 11, 12);
        $data['cmf9'] = DB::table('tr_cmf')
                            ->selectRaw('nomorCMF, tr_cmf.dtmUpdatedDate, namaKaryawan, namaDepartment, statusProsesCMF')
                            ->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
                            ->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
                            ->whereIn('statusProsesCMF', $statusProses9)
                            ->get();
        return view('Auth.Dashboard', $data);
    }

    public function logout(Request $request){
        $request->session()->forget('cmf_nik');
        $request->session()->forget('cmf_levelCMF');
        $request->session()->forget('cmf_namaKaryawan');
        $request->session()->forget('cmf_department');
        $request->session()->forget('cmf_area');
        return redirect("/")->withSuccess('You have been logged out !');
    }

    public function AllertSession(Request $request){
        $request->session()->forget('cmf_nik');
        $request->session()->forget('cmf_levelCMF');
        $request->session()->forget('cmf_namaKaryawan');
        $request->session()->forget('cmf_department');
        $request->session()->forget('cmf_area');
        return redirect("/")->withFail('You have to login first !');
    }
}
