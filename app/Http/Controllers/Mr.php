<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Mr extends Controller
{
    public function Home(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL04'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$statusProses = array(1, 1.5, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12);
				// NOTIF
				$data['notif4'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 4)
								->count();
				$data['notif12'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 12)
								->count();
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "active";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA VIEW
				$data['title'] = "CMF ONLINE | Home Page";
				$data['role'] = 'Mr';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['nik'] = $data_user->nik;
				$data['kodeUser'] = $data_user->kodeUser;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->whereIn('statusProsesCMF', $statusProses)
						->orderBy('tr_cmf.dtmInsertedDate', 'DESC')
						->get();
				$data['member'] = DB::table('m_user')
						->where('bitActive', 1)
						->where('department', $data_user->department)
						->get();
                return view('Mr.home', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	//////////////////////////////////////////////// FITUR DEPT HEAED ///////////////////////////////////////////
	public function DeptheadApprovalCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL04'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				// NOTIF
				$data['notif4'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 4)
								->count();
				$data['notif12'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 12)
								->count();
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "active";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA VIEW
				$data['title'] = "CMF ONLINE | CMF DITERIMA";
				$data['role'] = 'Mr';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;

                // DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->where('departmentCMF', $department)
						->where('statusProsesCMF', 1)
						->orderBy('tr_cmf.dtmInsertedDate', 'DESC')
						->get();
				return view('Depthead.cmf_approval', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function OpenCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL04'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif4'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 4)
								->count();
				$data['notif12'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 12)
								->count();
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "active";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA VIEW
				$data['title'] = "CMF ONLINE | Open CMF Page";
				$data['role'] = 'Mr';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
                // DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				return view('Depthead.cmf_open', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function AfterChangeCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL04'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				// NOTIF
				$data['notif4'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 4)
								->count();
				$data['notif12'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 12)
								->count();
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "active";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA VIEW
				$data['title'] = "CMF ONLINE | CMF Setelah Perubahan";
				$data['role'] = 'Mr';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
                // DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->where('departmentCMF', $department)
						->where('statusProsesCMF', 8)
						->orderBy('tr_cmf.dtmInsertedDate', 'DESC')
						->get();
				return view('Depthead.cmf_after_change', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function OpenAfterCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL04'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif4'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 4)
								->count();
				$data['notif12'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 12)
								->count();
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "active";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA VIEW
				$data['title'] = "CMF ONLINE | Open CMF Page";
				$data['role'] = 'Mr';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
                // DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				return view('Depthead.cmf_open_after', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function DeptheadReviewCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL04'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				// NOTIF
				$data['notif4'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 4)
								->count();
				$data['notif12'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 12)
								->count();
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "active";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA VIEW
				$data['title'] = "CMF ONLINE | CMF REVIEW";
				$data['role'] = 'Mr';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
                // DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
						->where('kodeDepartmentCMF', $department)
						->where('statusProsesCMF', 2)
						->where('adjustmentDepartment1CMF', 0)
						->orderBy('tr_cmf.dtmInsertedDate', 'DESC')
						->get();
				return view('Depthead.cmf_review', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function ReviewCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL04'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif4'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 4)
								->count();
				$data['notif12'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 12)
								->count();
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "active";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA VIEW
				$data['title'] = "CMF ONLINE | Open CMF Page";
				$data['role'] = 'Mr';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
                // DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				return view('Depthead.cmf_review_open', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function DeptheadEvaluasiCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL04'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				// NOTIF
				$data['notif4'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 4)
								->count();
				$data['notif12'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 12)
								->count();
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "active";
				$data['active8'] = "";
				// DATA VIEW
				$data['title'] = "CMF ONLINE | CMF Evaluasi";
				$data['role'] = 'Mr';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
                // DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
						->where('kodeDepartmentCMF', $department)
						->where('statusProsesCMF', 9)
						->where('adjustmentDepartment2CMF', 0)
						->orderBy('tr_cmf.dtmInsertedDate', 'DESC')
						->get();
				return view('Depthead.cmf_evaluasi', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function EvaluasiCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL04'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif4'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 4)
								->count();
				$data['notif12'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 12)
								->count();
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "active";
				$data['active8'] = "";
				// DATA VIEW
				$data['title'] = "CMF ONLINE | CMF Evaluasi";
				$data['role'] = 'Mr';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
                // DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['evaluasi'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->orderBy('dtmUpdatedDate', 'ASC')
											->get();
				$data['evaluasi_count'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->count();
				return view('Depthead.cmf_evaluasi_open', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}


	//////////////////////////////////////////////// FITUR MR //////////////////////////////////////////////////

	public function MrApprovalCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL04'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$nik = $request->session()->get('cmf_nik');
				// NOTIF
				$data['notif4'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 4)
								->count();
				$data['notif12'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 12)
								->count();
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "active";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA VIEW
				$data['title'] = "CMF ONLINE | CMF Sudah Direview";
				$data['role'] = 'Mr';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
							->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
							->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
							->where('statusProsesCMF', 4)
							->orderBy('tr_cmf.dtmInsertedDate', 'DESC')
							->get();
                return view('Mr.cmf_reviewed', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function MrOpenCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL04'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif4'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 4)
								->count();
				$data['notif12'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 12)
								->count();
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "active";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA VIEW
				$data['title'] = "CMF ONLINE | CMF Sudah Direview";
				$data['role'] = 'Mr';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
				// DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
                return view('Mr.cmf_open', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function MrVerifikasiCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL04'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$nik = $request->session()->get('cmf_nik');
				// NOTIF
				$data['notif4'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 4)
								->count();
				$data['notif12'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 12)
								->count();
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "active";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA VIEW
				$data['title'] = "CMF ONLINE | CMF Verifikasi";
				$data['role'] = 'Mr';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
							->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
							->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
							->where('statusProsesCMF', 12)
							->orderBy('tr_cmf.dtmInsertedDate', 'DESC')
							->get();
                return view('Mr.cmf_verifikasi', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function OpenVerifikasiCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL04'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif4'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 4)
								->count();
				$data['notif12'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 12)
								->count();
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "active";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA VIEW
				$data['title'] = "CMF ONLINE | OPEN CMF";
				$data['role'] = 'Mr';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['evaluasi'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->orderBy('dtmUpdatedDate', 'ASC')
											->get();
				$data['evaluasi_count'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->count();
				$data['dokumen'] = DB::table('tr_dokumen') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('postDokumenCMF', 'ASC')
											->get();
                return view('Mr.cmf_open_verifikasi', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function SemuaCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL04'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$nik = $request->session()->get('cmf_nik');
				$statusRiwayat = array(13, 14);
				// NOTIF
				$data['notif4'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 4)
								->count();
				$data['notif12'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 12)
								->count();
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "active";
				// DATA VIEW
				$data['title'] = "CMF ONLINE | Semua CMF";
				$data['role'] = 'Mr';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
							->selectRaw('YEAR(dateCMF) as tahun')
							->whereIn('statusProsesCMF', $statusRiwayat)
							->distinct()
							->get();
				$data['cmf_count'] = DB::table('tr_cmf')
							->whereIn('statusProsesCMF', $statusRiwayat)
							->count();
                return view('Dc.cmf_all', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function SemuaCMFAllYear(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL04'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$nik = $request->session()->get('cmf_nik');
				$statusRiwayat = array(13, 14);
				// NOTIF
				$data['notif4'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 4)
								->count();
				$data['notif12'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 12)
								->count();
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "active";
				// DATA VIEW
				$data['title'] = "CMF ONLINE | Semua CMF";
				$data['role'] = 'Mr';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
							->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
							->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
							->whereIn('statusProsesCMF', $statusRiwayat)
							->orderBy('tr_cmf.dtmInsertedDate', 'DESC')
							->get();
                return view('Dc.cmf_semua_all_year', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function OpenRiwayatCMFTahun(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL04'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif4'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 4)
								->count();
				$data['notif12'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 12)
								->count();
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "active";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA VIEW
				$data['title'] = "CMF ONLINE | Open CMF";
				$data['role'] = 'Mr';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['tahun'] = date('Y', strtotime($dt->dateCMF));
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['evaluasi'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->orderBy('dtmUpdatedDate', 'ASC')
											->get();
				$data['evaluasi_count'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->count();
				$data['dokumen'] = DB::table('tr_dokumen') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('postDokumenCMF', 'ASC')
											->get();
                return view('Dc.cmf_open_riwayat_tahun', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function SemuaCMFTahun(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL04'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$nik = $request->session()->get('cmf_nik');
				$statusRiwayat = array(13, 14);
				$tahun = $request->d;
				$data['tahun'] = $tahun;
				// NOTIF
				$data['notif4'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 4)
								->count();
				$data['notif12'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 12)
								->count();
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "active";
				// DATA VIEW
				$data['title'] = "CMF ONLINE | Semua CMF";
				$data['role'] = 'Mr';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
							->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
							->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
							->whereIn('statusProsesCMF', $statusRiwayat)
							->whereYear('dateCMF', $tahun)
							->orderBy('tr_cmf.dtmInsertedDate', 'DESC')
							->get();
                return view('Dc.cmf_semua_tahun', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}
}
