<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Pic extends Controller
{
    public function Home(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL01'){
				$department = $request->session()->get('cmf_department');
				$statusProsesDashboard = array(1, 2, 1.5, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12);
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "active";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Dashboard ";
				$data['role'] = 'Pic';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['nik'] = $data_user->nik;
				$data['kodeUser'] = $data_user->kodeUser;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->where('departmentCMF', $department)
						->whereIn('statusProsesCMF', $statusProsesDashboard)
						->orderBy('tr_cmf.dateCMF', 'DESC')
						->get();
				$data['member'] = DB::table('m_user')
						->where('bitActive', 1)
						->where('department', $data_user->department)
						->get();
                return view('Pic.home', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function ListCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL01'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "active";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Buat CMF";
				$data['role'] = 'Pic';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->where('departmentCMF', $department)
						->where('areaCMF', $area)
						->where('statusProsesCMF', 0)
						->orderBy('tr_cmf.dateCMF', 'ASC')
						->get();
                return view('Pic.cmf_list', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function CreateCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL01'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "active";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Create CMF Page";
				$data['role'] = 'Pic';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
				// DATABASE
				$data['area'] = DB::table('m_area_terkait')
						->where('kodeDepartment', '!=', $department)
						->where('bitActiveArea', '1')
						->orderBy('namaAreaTerkait', 'ASC')
						->get();
				$data['jenisPerubahan'] = DB::table('m_jenis_perubahan')
						->where('bitActiveJenisPerubahan', 1)
						->get();
                return view('Pic.cmf_create', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function EditCreateCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL01'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "active";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Edit CMF Page";
				$data['role'] = 'Pic';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
				// DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['role'] = 'Pic';
                return view('Pic.cmf_create_edit', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function OpenCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL01'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "active";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Open CMF Page";
				$data['role'] = 'Pic';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
				// DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('m_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('bitActiveJenisPerubahan', '1')
											->get();
				$data['role'] = 'Pic';
                return view('Pic.cmf_open', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function PenolakanCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL01'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "active";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Penolakan CMF";
				$data['role'] = 'Pic';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->where('departmentCMF', $department)
						->whereIn('statusProsesCMF', $statusProses)
						->orderBy('tr_cmf.dateCMF', 'ASC')
						->get();
						// echo $department;die;
				return view('Pic.cmf_penolakan', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function PenolakanCMFAfter(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL01'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "active";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | CMF Dikoreksi";
				$data['role'] = 'Pic';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->where('departmentCMF', $department)
						->whereIn('statusProsesCMF', $statusProsesAfter)
						->orderBy('tr_cmf.dateCMF', 'ASC')
						->get();
						// echo $department;die;
				return view('Pic.cmf_penolakan_after', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function EditReviewedCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL01'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "active";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Edit CMF Page";
				$data['role'] = 'Pic';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
				// DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['cmf_ditolak_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '-1')
											->count();
                return view('Pic.cmf_reviewed_edit', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function AfterChangeCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL01'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "active";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Penolakan CMF";
				$data['role'] = 'Pic';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->where('departmentCMF', $department)
						->where('statusProsesCMF', 7)
						->orderBy('tr_cmf.dateCMF', 'ASC')
						->get();
				return view('Pic.cmf_after_change', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function OpenAfterCMF (Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL01'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "active";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Open CMF Page";
				$data['role'] = 'Pic';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
				// DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['role'] = 'Pic';
                return view('Pic.cmf_open_after', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function RiwayatCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL01'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "active";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Riwayat CMF";
				$data['role'] = 'Pic';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$statusRiwayat = array(13, 14);
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->where('departmentCMF', $department)
						->whereIn('statusProsesCMF', $statusRiwayat)
						->orderBy('tr_cmf.dateCMF', 'ASC')
						->get();
				return view('Pic.cmf_riwayat', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function OpenRiwayatCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL01'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "active";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Open CMF Page";
				$data['role'] = 'Pic';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
				// DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['evaluasi'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->orderBy('dtmUpdatedDate', 'ASC')
											->get();
				$data['evaluasi_count'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->count();
				$data['dokumen'] = DB::table('tr_dokumen') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('postDokumenCMF', 'ASC')
											->get();
                return view('Pic.cmf_open_riwayat', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}
}
