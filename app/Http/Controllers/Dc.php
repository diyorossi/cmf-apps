<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Dc extends Controller
{
    public function Home(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProsesDashboard = array(1, 2, 1.5, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12);
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "active";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Dashboard";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['nik'] = $data_user->nik;
				$data['kodeUser'] = $data_user->kodeUser;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->whereIn('statusProsesCMF', $statusProsesDashboard)
						->orderBy('tr_cmf.dateCMF', 'DESC')
						->get();
				$data['member'] = DB::table('m_user')
						->where('bitActive', 1)
						->where('department', $data_user->department)
						->get();
                return view('Dc.home', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function OpenEditCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "active";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Edit CMF";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
                // DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->join('m_department', 'tr_department_terkait.kodeDepartmentCMF', '=', 'm_department.kodeDepartmentMaster')
											->where('kodeCMF', $kodeCMF)
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['evaluasi'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->join('m_department', 'tr_department_terkait.kodeDepartmentCMF', '=', 'm_department.kodeDepartmentMaster')
											->where('kodeCMF', $kodeCMF)
											->orderBy('dtmUpdatedDate', 'ASC')
											->get();
				$data['evaluasi_count'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->count();
				$data['dokumen'] = DB::table('tr_dokumen') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('kodeDokumenCMF', 'ASC')
											->get();
				$data['dokumen_count'] = DB::table('tr_dokumen') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->count();
				$data['dokumen_master'] = DB:: table('m_dokumen')
											->where('bitActiveDokumen', 1)
											->orderBy('kodeDokumen')
											->get();
				$data['namaKaryawan'] = DB::table('m_user')
											->where('bitActive', 1)
											->orderBy('namaKaryawan', 'ASC')
											->get();
				$data['department'] = DB::table('m_department')
											->where('bitActiveDepartment', 1)
											->orderBy('namaDepartment', 'ASC')
											->get();
				$data['area'] = DB::table('m_area_terkait')
											->where('bitActiveArea', 1)
											->orderBy('namaAreaTerkait', 'ASC')
											->get();
				return view('Dc.cmf_open_edit', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	///////////////////////////////////////////////// FITUR USER ///////////////////////////////////////////
	public function ListCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "active";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Create CMF Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
                // DATABASE
				$data['cmf'] = DB::table('tr_cmf')
							->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
							->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
							->where('departmentCMF', $department)
							->where('areaCMF', $area)
							->where('statusProsesCMF', 0)
							->orderBy('tr_cmf.dateCMF', 'DESC')
							->get();
				return view('Pic.cmf_list', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function CreateCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "active";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Create CMF Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
                // DATABASE
				$data['area'] = DB::table('m_area_terkait')
						->where('kodeDepartment', '!=', $department)
						->where('bitActiveArea', '1')
						->orderBy('namaAreaTerkait', 'ASC')
						->get();
				$data['jenisPerubahan'] = DB::table('m_jenis_perubahan')
						->where('bitActiveJenisPerubahan', 1)
						->get();
				return view('Pic.cmf_create', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function EditCreateCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "active";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Edit CMF Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
                // DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				return view('Pic.cmf_create_edit', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function AfterChangeCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "active";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | CMF Setelah Perubahan";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
                // DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->where('departmentCMF', $department)
						->where('statusProsesCMF', 7)
						->orderBy('tr_cmf.dateCMF', 'DESC')
						->get();
				return view('Pic.cmf_after_change', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function OpenAfterCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "active";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | CMF Setelah Perubahan";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
                // DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				return view('Pic.cmf_open_after', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function RiwayatCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "active";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Riwayat CMF";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
                // DATABASE
				$statusRiwayat = array(13, 14);
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->where('departmentCMF', $department)
						->whereIn('statusProsesCMF', $statusRiwayat)
						->orderBy('tr_cmf.dateCMF', 'DESC')
						->get();
				return view('Pic.cmf_riwayat', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function OpenRiwayatCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "active";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Riwayat CMF";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
                // DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['evaluasi'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->orderBy('dtmUpdatedDate', 'ASC')
											->get();
				$data['evaluasi_count'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->count();
				$data['dokumen'] = DB::table('tr_dokumen') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('postDokumenCMF', 'ASC')
											->get();
				return view('Pic.cmf_open_riwayat', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function PenolakanCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "active";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Penolakan CMF";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
                // DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->where('departmentCMF', $department)
						->whereIn('statusProsesCMF', $statusProses)
						->orderBy('tr_cmf.dateCMF', 'DESC')
						->get();
				return view('Pic.cmf_penolakan', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function PenolakanCMFAfter(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "active";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | CMF Dikoreksi";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
                // DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->where('departmentCMF', $department)
						->whereIn('statusProsesCMF', $statusProsesAfter)
						->orderBy('tr_cmf.dateCMF', 'DESC')
						->get();
				return view('Pic.cmf_penolakan_after', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function EditReviewedCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "active";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Open  CMF";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
                // DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['cmf_ditolak_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '-1')
											->count();
				return view('Pic.cmf_reviewed_edit', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}
	

	///////////////////////////////////////////////// FITUR DOCUMENT CONTROLLER ////////////////////////////	

	public function DcPengajuanCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "active";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Pengajuan CMF";
				$data['role'] = "Dc";
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
							->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
							->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
							->where('statusProsesCMF', 1.5)
							->orderBy('tr_cmf.dateCMF', 'DESC')
							->get();
                return view('Dc.cmf_pengajuan', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function OpenPengajuanCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "active";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Pengajuan CMF";
				$data['role'] = "Dc";
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
				// DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
                return view('Dc.cmf_open_pengajuan', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function DcApprovalCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "active";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | CMF Sudah Direview";
				$data['role'] = "Dc";
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
							->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
							->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
							->where('statusProsesCMF', 6)
							->orderBy('tr_cmf.dateCMF', 'DESC')
							->get();
                return view('Dc.cmf_reviewed', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function OpenCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "active";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | CMF Sudah Direview";
				$data['role'] = "Dc";
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
				// DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['cmf_ditolak_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '-1')
											->count();
                return view('Dc.cmf_open', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function VerifikasiDocumentCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "active";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Verifikasi Dokumen";
				$data['role'] = "Dc";
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
							->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
							->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
							->where('statusProsesCMF', 11)
							->orderBy('tr_cmf.dateCMF', 'DESC')
							->get();
                return view('Dc.cmf_verifikasi_dokumen', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function OpenVerifikasiCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "active";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | OPEN CMF";
				$data['role'] = "Dc";
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
				// DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['evaluasi'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->orderBy('dtmUpdatedDate', 'ASC')
											->get();
				$data['evaluasi_count'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->count();
				$data['dokumen'] = DB::table('m_dokumen') //DATA DEPARTMENT TERKAIT
											->where('bitActiveDokumen', 1)
											->orderBy('kodeDokumen', 'ASC')
											->get();
                return view('Dc.cmf_open_verifikasi', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function SemuaCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$statusRiwayat = array(13, 14);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "active";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Semua CMF";
				$data['role'] = "Dc";
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
							->selectRaw('YEAR(dateCMF) as tahun')
							->whereIn('statusProsesCMF', $statusRiwayat)
							->distinct()
							->get();
				$data['cmf_count'] = DB::table('tr_cmf')
							->whereIn('statusProsesCMF', $statusRiwayat)
							->count();
                return view('Dc.cmf_all', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function SemuaCMFTahun(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$statusRiwayat = array(13, 14);
				$area = $request->session()->get('cmf_area');
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$area = $request->session()->get('cmf_area');
				$tahun = $request->d;
				$data['tahun'] = $tahun;
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "active";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | SEMUA CMF";
				$data['role'] = "Dc";
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
							->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
							->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
							->whereIn('statusProsesCMF', $statusRiwayat)
							->whereYear('dateCMF', $tahun)
							->orderBy('tr_cmf.dateCMF', 'ASC')
							->get();
                return view('Dc.cmf_semua_tahun', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function OpenRiwayatCMFTahun(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "active";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Open CMF";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
                // DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['tahun'] = date('Y', strtotime($dt->dateCMF));
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['evaluasi'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->orderBy('dtmUpdatedDate', 'ASC')
											->get();
				$data['evaluasi_count'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->count();
				$data['dokumen'] = DB::table('tr_dokumen') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('postDokumenCMF', 'ASC')
											->get();
				return view('Dc.cmf_open_riwayat_tahun', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function SemuaCMFAllYear(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$statusRiwayat = array(13, 14);
				$area = $request->session()->get('cmf_area');
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "active";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | SEMUA CMF";
				$data['role'] = "Dc";
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
							->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
							->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
							->whereIn('statusProsesCMF', $statusRiwayat)
							->orderBy('tr_cmf.dateCMF', 'DESC')
							->get();
                return view('Dc.cmf_semua_all_year', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function User(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$nik = $request->session()->get('cmf_nik');
				// NOTIF NAVIGATION
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE NAVIGATION
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "active";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | User Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['allUser'] = DB::table('m_user')//DATA M_USER
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->where('kodeUser', '!=', '')
						->orderBy('namaKaryawan', 'ASC')
						->get();
                return view('Dc.User', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function AddUser(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$nik = $request->session()->get('cmf_nik');
				// NOTIF NAVIGATION
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE NAVIGATION
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "active";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Add User Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['allDepAktif'] = DB::table('m_department')//DATA M_DEPARTMENT
						->where('bitActiveDepartment', '1')
						->orderBy('namaDepartment', 'ASC')
						->get();
				$data['allAreaAktif2'] = DB::table('m_area_terkait')//DATA M_AREA TERKAIT
						->where('bitActiveArea', '1')
						->where('kodeDepartment', '!=', '1')
						->orderBy('namaAreaTerkait', 'ASC')
						->get();
				$data['allLevel'] = DB::table('m_level')//DATA M_LEVEL
						->orderBy('namaLevel', 'ASC')
						->get();
                return view('Dc.User_add', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function UserEdit(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$nik = $request->session()->get('cmf_nik');
				$kodeUser = $request->d;
				// NOTIF NAVIGATION
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE NAVIGATION
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "active";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Edit User Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['allDepAktif'] = DB::table('m_department')//DATA M_DEPARTMENT
						->where('bitActiveDepartment', '1')
						->orderBy('namaDepartment', 'ASC')
						->get();
				$data['allAreaAktif2'] = DB::table('m_area_terkait')//DATA M_AREA TERKAIT
						->where('bitActiveArea', '1')
						->where('kodeDepartment', '!=', '1')
						->orderBy('namaAreaTerkait', 'ASC')
						->get();
				$data['allLevel'] = DB::table('m_level')//DATA M_LEVEL
						->orderBy('namaLevel', 'ASC')
						->get();
				$data['dt'] = DB::table('m_user')
							->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
							->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
							->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
							->where('kodeUser', $kodeUser)
							->first();
                return view('Dc.User_edit', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function AreaTerkait(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$nik = $request->session()->get('cmf_nik');
				// NOTIF NAVIGATION
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE NAVIGATION
				$data['active1'] = "";
				$data['active2'] = "active";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Area Terkait Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['allAreaTerkait'] = DB::table('m_area_terkait')//DATA M_USER
						->join('m_department', 'm_area_terkait.kodeDepartment', '=', 'm_department.kodeDepartmentMaster')
						->orderBy('namaAreaTerkait', 'ASC')
						->get();
                return view('Dc.area_terkait', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function AreaTerkaitAdd(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$nik = $request->session()->get('cmf_nik');
				// NOTIF NAVIGATION
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE NAVIGATION
				$data['active1'] = "";
				$data['active2'] = "active";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Add Area Terkait Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['allDepAktif'] = DB::table('m_department')//DATA M_DEPARTMENT
						->where('bitActiveDepartment', '1')
						->orderBy('namaDepartment', 'ASC')
						->get();
                return view('Dc.area_terkait_add', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function AreaTerkaitEdit(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$nik = $request->session()->get('cmf_nik');
				$encryKodeAreaTerkait = $request->d;
				// NOTIF NAVIGATION
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE NAVIGATION
				$data['active1'] = "";
				$data['active2'] = "active";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Area Terkiat Edit Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['allDepAktif'] = DB::table('m_department')//DATA M_DEPARTMENT
						->where('bitActiveDepartment', '1')
						->orderBy('namaDepartment', 'ASC')
						->get();
				$data['dt'] = DB::table('m_area_terkait')
							->join('m_department', 'm_area_terkait.kodeDepartment', '=', 'm_department.kodeDepartmentMaster')
							->where('encryKodeAreaTerkait', $encryKodeAreaTerkait)
							->first();
                return view('Dc.area_terkait_edit', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function Department(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$nik = $request->session()->get('cmf_nik');
				// NOTIF NAVIGATION
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE NAVIGATION
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "active";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Department Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['allDepartment'] = DB::table('m_department')//DATA M_USER
						->where('kodeDepartmentMaster', '!=', '')
						->orderBy('namaDepartment', 'ASC')
						->get();
                return view('Dc.department', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function DepartmentAdd(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$nik = $request->session()->get('cmf_nik');
				// NOTIF NAVIGATION
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE NAVIGATION
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "active";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Add Department Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				
                return view('Dc.department_add', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function DepartmentEdit(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$nik = $request->session()->get('cmf_nik');
				$encryKodeDepartment = $request->d;
				// NOTIF NAVIGATION
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE NAVIGATION
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "active";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Department Edit Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['dt'] = DB::table('m_department')
							->where('encryKodeDepartment', $encryKodeDepartment)
							->first();
                return view('Dc.department_edit', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function Dokumen(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$nik = $request->session()->get('cmf_nik');
				// NOTIF NAVIGATION
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE NAVIGATION
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "active";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Dokumen Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['allDokumen'] = DB::table('m_dokumen')//DATA M_USER
						->orderBy('postDokumen', 'ASC')
						->get();
                return view('Dc.dokumen', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function DokumenAdd(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$nik = $request->session()->get('cmf_nik');
				// NOTIF NAVIGATION
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE NAVIGATION
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "active";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Add Dokumen Revisi Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				
                return view('Dc.dokumen_add', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function DokumenEdit(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$nik = $request->session()->get('cmf_nik');
				$kodeDokumen = $request->d;
				// NOTIF NAVIGATION
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE NAVIGATION
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "active";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Edit Dokumen Revisi Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['dt'] = DB::table('m_dokumen')
							->where('kodeDokumen', $kodeDokumen)
							->first();
                return view('Dc.dokumen_edit', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function JenisPerubahan(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$nik = $request->session()->get('cmf_nik');
				// NOTIF NAVIGATION
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE NAVIGATION
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "active";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Jenis Perubahan Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['allJenisPerubahan'] = DB::table('m_jenis_perubahan')//DATA M_USER
						->orderBy('namaJenisPerubahan', 'ASC')
						->get();
                return view('Dc.jenis_perubahan', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function JenisPerubahanAdd(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$nik = $request->session()->get('cmf_nik');
				// NOTIF NAVIGATION
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE NAVIGATION
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "active";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Add Jenis Perubahan Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				
                return view('Dc.jenis_perubahan_add', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function JenisPerubahanEdit(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$nik = $request->session()->get('cmf_nik');
				$kodeJenisPerubahan = $request->d;
				// NOTIF NAVIGATION
				$data['notif11'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 11)
								->count();
				$data['notif6'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 6)
								->count();
				$data['notif1_5'] = DB::table('tr_cmf')
								->where('statusProsesCMF', 1.5)
								->count();
				$notifPenolakan = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProses)
							->count();
				$data['notifPenolakan'] = $notifPenolakan;
				$notifPenolakanAfter = DB::table('tr_cmf')
							->where('departmentCMF', $department)
							->whereIn('statusProsesCMF', $statusProsesAfter)
							->count();
				$data['notifPenolakanAfter'] = $notifPenolakanAfter;
				$notif7 = DB::table('tr_cmf')
							->where('areaCMF', $area)
							->where('statusProsesCMF', 7)
							->count();
				$data['notif7'] = $notif7;
				// ACTIVE NAVIGATION
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "active";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				$data['active8'] = "";
				$data['active9'] = "";
				$data['active10'] = "";
				$data['active11'] = "";
				$data['active12'] = "";
				$data['active13'] = "";
				$data['active14'] = "";
				$data['active15'] = "";
				$data['active16'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Edit Jenis Perubahan Page";
				$data['role'] = 'Dc';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['dt'] = DB::table('m_jenis_perubahan')
							->where('kodeJenisPerubahan', $kodeJenisPerubahan)
							->first();
                return view('Dc.jenis_perubahan_edit', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	public function ExportExcel(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
				// DATABASE
        		$data['nameFile2'] = 'ALL CMF';
        		$data['role'] = 'Dc';
				$statusRiwayat = array(13, 14);
				$data['cmf'] = DB::table('tr_cmf')
							->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
							->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
							->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
							->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
							// ->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
							->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
							// ->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
							->whereIn('statusProsesCMF', $statusRiwayat)
							->orderBy('tr_cmf.dateCMF', 'DESC')
							->get();
                return view('Dc.cmf_export_excel', $data);
			}else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
	}

	
}
