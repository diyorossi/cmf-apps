<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\CMF;

class Query extends Controller
{
    public function Approve(Request $request){
        if($request->session()->get('cmf_nik')){
            $encryKodeCMF = $request->encryKodeCMF;
            $statusProses = $request->statusProses;
            $feedBack = $request->feedBack;
            // STATUS PROSES 1 (CMF DIKIRIM KE DEPT HEAD) || CANCEL CMF BY DC AFTER REVIEW
            if($statusProses == 1 || $statusProses == 14){
                $updateArray = array(
                    "statusProsesCMF" => "$statusProses",
                    "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                    "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                );
                $update = DB::table('tr_cmf')
                        ->where('encryKodeCMF', $encryKodeCMF)
                        ->update($updateArray);
                if($update > 0){
                    return redirect($feedBack)->withSuccess('CMF Berhasil Terkirim !');
                }else{
                    return redirect($feedBack)->withFail('CMF Gagal Terkirim ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                }
            // STATUS PROSES 1.5 (APPROVED BY DEPT HEAD)
            }elseif($statusProses == 1.5){
                $catatanDeptheadCMF1 = $request->catatanDeptheadCMF1;
                $updateArray = array(
                    "statusProsesCMF" => "$statusProses",
                    "catatanDeptheadCMF1" => "$catatanDeptheadCMF1",
                    "approveCMF1" => $request->session()->get('cmf_namaKaryawan'),
                    "dateApproveCMF1" => date('Y-m-d H:i:s'),
                    "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                    "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                );
                $update = DB::table('tr_cmf')
                        ->where('encryKodeCMF', $encryKodeCMF)
                        ->update($updateArray);
                if($update > 0){
                    return redirect($request->feedBack)->withSuccess('CMF Berhasil Terkirim !');
                }else{
                    return redirect($request->feedBack)->withFail('CMF Gagal Terkirim ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                }
            // STATUS PROSES 2 (APPROVED DC)
            }elseif($statusProses == 2){
                $dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKodeCMF)
						->first();
				$kodeCMF = $dt->kodeCMF;
                $cek_review = DB::table('tr_department_terkait')
                                ->where('kodeCMF', $kodeCMF)
                                ->where('adjustmentDepartment1CMF', 0)
                                ->count();
                if($cek_review == 0){
                    $statusProses = 3;
                }
                $updateArray = array(
                    "statusProsesCMF" => "$statusProses",
                    "approveCMFDC" => $request->session()->get('cmf_namaKaryawan'),
                    "dateApproveCMFDC" => date('Y-m-d H:i:s'),
                    "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                    "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                );
                $update = DB::table('tr_cmf')
                        ->where('encryKodeCMF', $encryKodeCMF)
                        ->update($updateArray);
                if($update > 0){
                    return redirect($request->feedBack)->withSuccess('CMF Berhasil Terkirim !');
                }else{
                    return redirect($request->feedBack)->withFail('CMF Gagal Terkirim ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                }
            // STATUS PROSES 3 (APPROVED DEPT HEAD TERKAIT)
            }elseif($statusProses == 3){
                $reviewDepartmentCMF = $request->reviewDepartmentCMF;
                $dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKodeCMF)
						->first();
				$kodeCMF = $dt->kodeCMF;
                $cek_review = DB::table('tr_department_terkait')
                                ->where('kodeCMF', $kodeCMF)
                                ->where('adjustmentDepartment1CMF', 0)
                                ->count();
                if($cek_review == 1){
                    $updateArray = array(
                        "statusProsesCMF" => "$statusProses",
                        "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                        "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                    );
                    $update1 = DB::table('tr_cmf')
                                    ->where('encryKodeCMF', $encryKodeCMF)
                                    ->update($updateArray);
                    if($update1 > 0){
                        $updateArray2 = array(
                            "reviewDepartmentCMF" => "$reviewDepartmentCMF",
                            "adjustmentDepartment1CMF" => 1,
                            "approvedBy1" => $request->session()->get('cmf_namaKaryawan'),
                            "dateReviewCMF" => date('Y-m-d H:i:s'), 
                            "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                            "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                        );
                        $kodeDepartmentCMF = $request->session()->get('cmf_department');
                        $update2 = DB::table('tr_department_terkait')
                                    ->where('kodeDepartmentCMF', $kodeDepartmentCMF)
                                    ->where('kodeCMF', $kodeCMF)    
                                    ->update($updateArray2);
                        if($update2 > 0){
                            return redirect($request->feedBack)->withSuccess('CMF Berhasil Terkirim !');
                        }else{
                            return redirect($request->feedBack)->withFail('Departement Terkait Gagal Diupdate ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                        }
                    }else{
                        return redirect($request->feedBack)->withFail('CMF Gagal Diupdate ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                    }
                }else{
                    $updateArray = array(
                        "reviewDepartmentCMF" => "$reviewDepartmentCMF",
                        "adjustmentDepartment1CMF" => 1,
                        "approvedBy1" => $request->session()->get('cmf_namaKaryawan'),
                        "dateReviewCMF" => date('Y-m-d H:i:s'), 
                        "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                        "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                    );
                    $kodeDepartmentCMF = $request->session()->get('cmf_department');
                    $update1 = DB::table('tr_department_terkait')
                                ->where('kodeDepartmentCMF', $kodeDepartmentCMF)
                                ->where('kodeCMF', $kodeCMF)    
                                ->update($updateArray);
                    if($update1 > 0){
                        return redirect($request->feedBack)->withSuccess('CMF Berhasil Terkirim !');
                    }else{
                        return redirect($request->feedBack)->withFail('Departement Terkait Gagal Diupdate ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                    }
                }
            // STATUS PROSES 4 (APPROVED SPV SYSTEM)
            }elseif($statusProses == 4){
                $updateArray = array(
                    "statusProsesCMF" => "$statusProses",
                    "approveCMF2" => $request->session()->get('cmf_namaKaryawan'),
                    "dateApproveCMF2" => date('Y-m-d H:i:s'),
                    "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                    "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                );
                $update1 = DB::table('tr_cmf')
                            ->where('encryKodeCMF', $encryKodeCMF)
                            ->update($updateArray);
                if($update1 > 0){
                    return redirect($request->feedBack)->withSuccess('CMF Berhasil Terkirim !');
                }else{
                    return redirect($request->feedBack)->withFail('CMF Gagal Diupdate ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                }
            // STATUS PROSES 5 (APPROVED MR)
            }elseif($statusProses == 5){
                $updateArray = array(
                    "statusProsesCMF" => "$statusProses",
                    "approveCMF3" => $request->session()->get('cmf_namaKaryawan'),
                    "dateApproveCMF3" => date('Y-m-d H:i:s'),
                    "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                    "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                );
                $update1 = DB::table('tr_cmf')
                            ->where('encryKodeCMF', $encryKodeCMF)
                            ->update($updateArray);
                if($update1 > 0){
                    return redirect($request->feedBack)->withSuccess('CMF Berhasil Terkirim !');
                }else{
                    return redirect($request->feedBack)->withFail('CMF Gagal Diupdate ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                }
            // STATUS PROSES 6 (APPROVED MNF)
            }elseif($statusProses == 6){
                $updateArray = array(
                    "statusProsesCMF" => "$statusProses",
                    "approveCMF4" => $request->session()->get('cmf_namaKaryawan'),
                    "dateApproveCMF4" => date('Y-m-d H:i:s'),
                    "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                    "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                );
                $update1 = DB::table('tr_cmf')
                            ->where('encryKodeCMF', $encryKodeCMF)
                            ->update($updateArray);
                if($update1 > 0){
                    return redirect($request->feedBack)->withSuccess('CMF Berhasil Terkirim !');
                }else{
                    return redirect($request->feedBack)->withFail('CMF Gagal Diupdate ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                }
            // STATUS PROSES 7 (APPROVED DC)
            }elseif($statusProses == 7){
                $updateArray = array(
                    "statusProsesCMF" => "$statusProses",
                    "approveCMF5" => $request->session()->get('cmf_namaKaryawan'),
                    "dateApproveCMF5" => date('Y-m-d H:i:s'),
                    "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                    "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                );
                $update1 = DB::table('tr_cmf')
                            ->where('encryKodeCMF', $encryKodeCMF)
                            ->update($updateArray);
                if($update1 > 0){
                    return redirect($request->feedBack)->withSuccess('CMF Berhasil Terkirim !');
                }else{
                    return redirect($request->feedBack)->withFail('CMF Gagal Diupdate ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                }
            // STATUS PROSES 8 (DETAIL AKTIVITAS PEMILIK PROSES)
            }elseif($statusProses == 8){
                $detailAktifitasCMF = $request->detailAktifitasCMF;
                $txtUploadCommissioningReject = $request->txtUploadCommissioningReject;
                if($detailAktifitasCMF){
                    $dt = DB::table('tr_cmf')//DATA GET CMF
                            ->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
                            ->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
                            ->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
                            ->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
                            ->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
                            ->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
                            ->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
                            ->where('encryKodeCMF', $encryKodeCMF)
                            ->first();
                    $kodeCMF = $dt->kodeCMF;
                    $dateCMF = $dt->dateCMF;
                    $departmentCMF = $dt->departmentCMF;
                    // UPLOAD DOKUMEN
                    $namaFile = $_FILES ['txtUploadCommissioning']['name'];
                    $ukuranFile = $_FILES['txtUploadCommissioning']['size'];
                    $error = $_FILES['txtUploadCommissioning']['error'];
                    $tmpName =$_FILES['txtUploadCommissioning']['tmp_name'];

                    if($error === 4){
                            return redirect('Pic/AfterChangeCMF')->withFail('Anda Belum Mengupload Dokumen Commissioning, Mohon untuk Menguploa Terlebih Dahulu Sebelum Mengirim CMF !');
                    }else{
                        // cek pastikan yg di upload adalah gambar
                        $ekstensiDocumentValid = ['pdf'];
                        $ekstensiDocument = explode('.', $namaFile);
                        $ekstensiDocument= strtolower(end($ekstensiDocument));

                        if(!in_array($ekstensiDocument, $ekstensiDocumentValid)){
                            return redirect('Pic/AfterChangeCMF')->withFail('Dokumen Yang Diupload Bukan PDF !');
                        }

                        $uniq = $kodeCMF;
                        $namaFileBaru = $uniq.'.'.$ekstensiDocument;
                        $tahunCMF = date('Y', strtotime($dateCMF));
                        $dpt = DB::table('m_department')
                            ->where('kodeDepartmentMaster', $departmentCMF)
                            ->first();
                        $department = $dpt->namaDepartment;
                        $pathTahun = 'upload/commissioning/'.$tahunCMF;
                        $pathDepartment = 'upload/commissioning/'.$tahunCMF.'/'.$department;
                        $pathNamaFileBaru = 'upload/commissioning/'.$tahunCMF.'/'.$department.'/'.$namaFileBaru;
                        $documentReject = 'upload/commissioning/'.$tahunCMF.'/'.$department.'/'.$txtUploadCommissioningReject;

                        if(is_dir($pathTahun)){
                            if(is_dir($pathDepartment)){
                                move_uploaded_file($tmpName, $pathNamaFileBaru);
                                if(!file_exists($pathNamaFileBaru)){
                                    return redirect($feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                            }
                            }else{
                                mkdir($pathDepartment);
                                $file = fopen($pathDepartment."/"."index.html", "w");
                                echo fwrite($file,"Ngeyel Banget dah hahaha !");
                                fclose($file);
                                move_uploaded_file($tmpName, $pathNamaFileBaru);
                                if(!file_exists($pathNamaFileBaru)){
                                    return redirect($feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                                }
                            }
                        }else{
                            $file = fopen("upload/commissioning/"."index.html", "w");
                            echo fwrite($file,"Dilarang Masuk Bro !");
                            fclose($file);
                            mkdir($pathTahun);
                            $file = fopen($pathTahun."/"."index.html", "w");
                            echo fwrite($file,"Dibilangin Ga Boleh Masuk !");
                            fclose($file);
                            mkdir($pathDepartment);
                            $file = fopen($pathDepartment."/"."index.html", "w");
                            echo fwrite($file,"Ngeyel Banget dah hahaha !");
                            fclose($file);
                            move_uploaded_file($tmpName, $pathNamaFileBaru);
                            if(!file_exists($pathNamaFileBaru)){
                                return redirect($feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                            }
                        }
                    }
                    $updateArray = array(
                        "statusProsesCMF" => "$statusProses",
                        "detailAktifitasCMF" => "$detailAktifitasCMF",
                        "txtUploadCommissioning" => "$namaFileBaru",
                        "approveCMF6" => $request->session()->get('cmf_namaKaryawan'),
                        "dateApproveCMF6" => date('Y-m-d H:i:s'),
                        "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                        "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                    );
                    $update1 = DB::table('tr_cmf')
                                ->where('encryKodeCMF', $encryKodeCMF)
                                ->update($updateArray);
                    if($update1 > 0){
                        return redirect($request->feedBack)->withSuccess('CMF Berhasil Terkirim !');
                    }else{
                        return redirect($request->feedBack)->withFail('CMF Gagal Diupdate ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                    }
                }else{
                    $updateArray = array(
                        "statusProsesCMF" => "$statusProses",
                        "approveCMF6" => $request->session()->get('cmf_namaKaryawan'),
                        "dateApproveCMF6" => date('Y-m-d H:i:s'),
                        "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                        "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                    );
                    $update1 = DB::table('tr_cmf')
                                ->where('encryKodeCMF', $encryKodeCMF)
                                ->update($updateArray);
                    if($update1 > 0){
                        return redirect($request->feedBack)->withSuccess('CMF Berhasil Terkirim !');
                    }else{
                        return redirect($request->feedBack)->withFail('CMF Gagal Diupdate ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                    }
                }
            // STATUS PROSES 9 (APPROVED DEPTHEAD PEMILIK PROSES)
            }elseif($statusProses == 9){
                $catatanDeptheadCMF2 = $request->catatanDeptheadCMF2;
                $updateArray = array(
                    "statusProsesCMF" => "$statusProses",
                    "catatanDeptheadCMF2" => "$catatanDeptheadCMF2",
                    "approveCMF7" => $request->session()->get('cmf_namaKaryawan'),
                    "dateApproveCMF7" => date('Y-m-d H:i:s'),
                    "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                    "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                );
                $update1 = DB::table('tr_cmf')
                            ->where('encryKodeCMF', $encryKodeCMF)
                            ->update($updateArray);
                if($update1 > 0){
                    return redirect($request->feedBack)->withSuccess('CMF Berhasil Terkirim !');
                }else{
                    return redirect($request->feedBack)->withFail('CMF Gagal Diupdate ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                }
            // STATUS PROSES 10 (EVALUASI DEPTHEAD TERKAIT)
            }elseif($statusProses == 10){
                $evaluasiDepartmentCMF = $request->evaluasiDepartmentCMF;
                $dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKodeCMF)
						->first();
				$kodeCMF = $dt->kodeCMF;
                $cek_evaluasi = DB::table('tr_department_terkait')
                                    ->where('kodeCMF', $kodeCMF)
                                    ->where('adjustmentDepartment2CMF', 0)
                                    ->count();
                if($cek_evaluasi == 1){
                    $updateArray = array(
                        "statusProsesCMF" => "$statusProses",
                        "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                        "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                    );
                    $update1 = DB::table('tr_cmf')
                            ->where('encryKodeCMF', $encryKodeCMF)
                            ->update($updateArray);
                    if($update1 > 0){
                        $updateArray2 = array(
                            "evaluasiDepartmentCMF" => "$evaluasiDepartmentCMF",
                            "adjustmentDepartment2CMF" => 1,
                            "approvedBy2" => $request->session()->get('cmf_namaKaryawan'),
                            "dateEvaluasiCMF" => date('Y-m-d H:i:s'), 
                            "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                            "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                        );
                        $kodeDepartmentCMF = $request->session()->get('cmf_department');
                        $update2 = DB::table('tr_department_terkait')
                                        ->where('kodeDepartmentCMF', $kodeDepartmentCMF)
                                        ->where('kodeCMF', $kodeCMF)
                                        ->update($updateArray2);
                        if($update2 > 0){
                            return redirect($request->feedBack)->withSuccess('CMF Berhasil Terkirim !');
                        }else{
                            return redirect($request->feedBack)->withFail('Department Terkait Gagal Diupdate ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                        }
                    }else{
                        return redirect($request->feedBack)->withFail('CMF Gagal Diupdate ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                    }
                }else{
                    $updateArray1 = array(
                        "evaluasiDepartmentCMF" => "$evaluasiDepartmentCMF",
                        "adjustmentDepartment2CMF" => 1,
                        "approvedBy2" => $request->session()->get('cmf_namaKaryawan'),
                        "dateEvaluasiCMF" => date('Y-m-d H:i:s'), 
                        "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                        "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                    );
                    $kodeDepartmentCMF = $request->session()->get('cmf_department');
                    $update1 = DB::table('tr_department_terkait')
                                        ->where('kodeDepartmentCMF', $kodeDepartmentCMF)
                                        ->where('kodeCMF', $kodeCMF)
                                        ->update($updateArray1);
                    if($update1 > 0){
                        return redirect($request->feedBack)->withSuccess('CMF Berhasil Terkirim !');
                    }else{
                        return redirect($request->feedBack)->withFail('Department Terkait Gagal Diupdate ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                    }
                }
            // STATUS PROSES 11 (APPROVED VERIFIKATOR)
            }elseif($statusProses == 11){
                $updateArray = array(
                    "statusProsesCMF" => "$statusProses",
                    "approveCMF8" => $request->session()->get('cmf_namaKaryawan'),
                    "dateApproveCMF8" => date('Y-m-d H:i:s'),
                    "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                    "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                );
                $update1 = DB::table('tr_cmf')
                            ->where('encryKodeCMF', $encryKodeCMF)
                            ->update($updateArray);
                if($update1 > 0){
                    return redirect($request->feedBack)->withSuccess('CMF Berhasil Terkirim !');
                }else{
                    return redirect($request->feedBack)->withFail('CMF Gagal Diupdate ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                }
            // STATUS PROSES 13 (APPROVED MR)
            }elseif($statusProses == 13){
                $updateArray = array(
                    "statusProsesCMF" => "$statusProses",
                    "approveCMF10" => $request->session()->get('cmf_namaKaryawan'),
                    "dateApproveCMF10" => date('Y-m-d H:i:s'),
                    "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                    "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                );
                $update1 = DB::table('tr_cmf')
                            ->where('encryKodeCMF', $encryKodeCMF)
                            ->update($updateArray);
                if($update1 > 0){
                    return redirect($request->feedBack)->withSuccess('CMF Berhasil Terkirim !');
                }else{
                    return redirect($request->feedBack)->withFail('CMF Gagal Diupdate ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                }
            }
        }else{
            return redirect('AllertSession');
        }
    }

    public function VerifikasiDokumen(Request $request){
        if($request->session()->get('cmf_nik')){
            // DEKLARASI
            $encryKodeCMF = $request->encryKodeCMF;
            $statusProses = $request->statusProses;
            $kodeCMF = $request->kodeCMF;
            $updateArray = array(
                "statusProsesCMF" => "$statusProses",
                "approveCMF9" => $request->session()->get('cmf_namaKaryawan'),
                "dateApproveCMF9" => date('Y-m-d H:i:s'),
                "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                "dtmUpdatedDate" => date('Y-m-d H:i:s'),
            );
            $update1 = DB::table('tr_cmf')
                            ->where('encryKodeCMF', $encryKodeCMF)
                            ->update($updateArray);
            if($update1 > 0){
                $dokumen = DB::table('m_dokumen') //DATA DEPARTMENT TERKAIT
											->where('bitActiveDokumen', 1)
											->orderBy('postDokumen', 'ASC')
											->get();
                foreach($dokumen as $dk){
                    //KODE DOKUMEN
                    $kodeDokumenCMF = DB::table('tr_dokumen')
                        ->max('kodeDokumenCMF');
                    $noUrut = (int) substr($kodeDokumenCMF, 3, 7);
                    $noUrut++;
                    $char = "CMF";
                    $kodeDokumenCMF = $char . sprintf("%07s", $noUrut);
                    $kodeDokumen = $dk->kodeDokumen;
                    $postDokumenCMF = $dk->postDokumen;
                    $valueDokumenCMF = $request->$postDokumenCMF;
                    if($valueDokumenCMF == 1){
                        $valueDokumenCMF = 1;
                    }else{
                        $valueDokumenCMF = 0;
                    }
                    $perubahanDokumenCMF = $request->$kodeDokumen;
                    $insertArray = array(
                        "kodeDokumenCMF" => "$kodeDokumenCMF",
                        "kodeCMF" => "$kodeCMF",
                        "postDokumenCMF" => "$postDokumenCMF",
                        "valueDokumenCMF" => "$valueDokumenCMF",
                        "perubahanDokumenCMF" => "$perubahanDokumenCMF",
                        "txtInsertedBy" => $request->session()->get('cmf_namaKaryawan'),
                        "dtmInsertedDate" => date('Y-m-d H:i:s'),
                    );
                    $insert1 = DB::table('tr_dokumen')->insert($insertArray);
                }
                return redirect($request->feedBack)->withSuccess('CMF Berhasil Terkirim !');
            }else{
                return redirect($request->feedBack)->withFail('CMF Gagal Diupdate ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
            }
        }else{
            return redirect('AllertSession');
        }
    }

    public function EditCMF(Request $request){
        if($request->session()->get('cmf_nik')){
            if($request->session()->get('cmf_levelCMF') == 'LVL06'){
                // DATA POST
                $departmentCMF = $request->departmentCMF;
                $areaCMF = $request->areaCMF;
                $pemilikProsesCMF = $request->pemilikProsesCMF;
                $kodeCMF = $request->kodeCMF;
                $dateCMF = $request->dateCMF;
                $judulPerubahanCMF = $request->judulPerubahanCMF;
                $bitMesin = $request->bitMesin;
                if($bitMesin == 1){
                    $bitMesin = 1;
                }else{
                    $bitMesin = 0;
                }
                $bitProses = $request->bitProses;
                if($bitProses == 1){
                    $bitProses = 1;
                }else{
                    $bitProses = 0;
                }
                $bitSystem = $request->bitSystem;
                if($bitSystem == 1){
                    $bitSystem = 1;
                }else{
                    $bitSystem = 0;
                }
                $bitLainnya = $request->bitLainnya;
                if($bitLainnya == 1){
                    $valueLainnya = $request->valueLainnya;
                }else{
                    $valueLainnya = "";
                }
                $bitInstallasi = $request->bitInstallasi;
                $bitFormula = $request->bitFormula;
                $bitRawMaterial = $request->bitRawMaterial;
                $bitPackagingMaterial = $request->bitPackagingMaterial;
                $bitSpesifikasiProduk = $request->bitSpesifikasiProduk;
                $bitFoodSafetyManagement = $request->bitFoodSafetyManagement;
                $bitOracle = $request->bitOracle;
                $bitSmk3 = $request->bitSmk3;
                $bitHalalManagement = $request->bitHalalManagement;
                $bitDigitalisasi = $request->bitDigitalisasi;
                $dateImplementasiCMF = $request->dateImplementasiCMF;
                $typePerubahanCMF = $request->typePerubahanCMF;
                $alasanPerubahanCMF = $request->alasanPerubahanCMF;
                $dampakPerubahanCMF = $request->dampakPerubahanCMF;
                $deskripsiPerubahanCMF = $request->deskripsiPerubahanCMF;
                $resikoCMF = $request->resikoCMF;
                $mitigasiCMF = $request->mitigasiCMF;
                $picRiskAssesmentCMF = $request->picRiskAssesmentCMF;
                $deadLineCMF = $request->deadLineCMF;
                $approveCMF1 = $request->approveCMF1;
                $dateApproveCMF1 = $request->dateApproveCMF1;
                $catatanDeptheadCMF1 = $request->catatanDeptheadCMF1;
                $approveCMF2 = $request->approveCMF2;
                $dateApproveCMF2 = $request->dateApproveCMF2;
                $approveCMF3 = $request->approveCMF3;
                $dateApproveCMF3 = $request->dateApproveCMF3;
                $approveCMF4 = $request->approveCMF4;
                $dateApproveCMF4 = $request->dateApproveCMF4;
                $approveCMF6 = $request->approveCMF6;
                $dateApproveCMF6 = $request->dateApproveCMF6;
                $detailAktifitasCMF = $request->detailAktifitasCMF;
                $approveCMF7 = $request->approveCMF7;
                $dateApproveCMF7 = $request->dateApproveCMF7;
                $catatanDeptheadCMF2 = $request->catatanDeptheadCMF2;
                $approveCMF8 = $request->approveCMF8;
                $dateApproveCMF8 = $request->dateApproveCMF8;
                $approveCMF9 = $request->approveCMF9;
                $dateApproveCMF9 = $request->dateApproveCMF9;
                $approveCMF10 = $request->approveCMF10;
                $dateApproveCMF10 = $request->dateApproveCMF10;
                $picNOKCMF = $request->picNOKCMF;
                $dateNOKCMF = $request->dateNOKCMF;
                $comentNOKCMF = $request->comentNOKCMF;
                $statusProsesCMF = $request->statusProsesCMF;
                $nomorCMF = $request->nomorCMF;
                // UPLOAD DOKUMEN
                $txtUploadDocumentReject = $request->txtUploadDocumentReject;
                $namaFile = $_FILES ['txtUploadDocument']['name'];
                $ukuranFile = $_FILES['txtUploadDocument']['size'];
                $error = $_FILES['txtUploadDocument']['error'];
                $tmpName =$_FILES['txtUploadDocument']['tmp_name'];

                if($error === 4){
                    $namaFileBaru = $txtUploadDocumentReject;
                }else{
                    // cek pastikan yg di upload adalah gambar
                    $ekstensiDocumentValid = ['pdf'];
                    $ekstensiDocument = explode('.', $namaFile);
                    $ekstensiDocument= strtolower(end($ekstensiDocument));

                    if(!in_array($ekstensiDocument, $ekstensiDocumentValid)){
                        return redirect($request->feedBack)->withFail('Dokumen Yang Diupload Bukan PDF !');
                    }

                    $uniq = $kodeCMF;
                    $namaFileBaru = $uniq.'.'.$ekstensiDocument;
                    $tahunCMF = date('Y', strtotime($dateCMF));
                    $dpt = DB::table('m_department')
                        ->where('kodeDepartmentMaster', $departmentCMF)
                        ->first();
                    $department = $dpt->namaDepartment;
                    $pathTahun = 'upload/documents/'.$tahunCMF;
                    $pathDepartment = 'upload/documents/'.$tahunCMF.'/'.$department;
                    $pathNamaFileBaru = 'upload/documents/'.$tahunCMF.'/'.$department.'/'.$namaFileBaru;
                    $documentReject = 'upload/documents/'.$tahunCMF.'/'.$department.'/'.$txtUploadDocumentReject;

                    if(is_dir($pathTahun)){
                        if(is_dir($pathDepartment)){
                            move_uploaded_file($tmpName, $pathNamaFileBaru);
                            if(!file_exists($pathNamaFileBaru)){
                                return redirect($request->feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                        }
                        }else{
                            mkdir($pathDepartment);
                            $file = fopen($pathDepartment."/"."index.html", "w");
                            echo fwrite($file,"Ngeyel Banget dah hahaha !");
                            fclose($file);
                            move_uploaded_file($tmpName, $pathNamaFileBaru);
                            if(!file_exists($pathNamaFileBaru)){
                                return redirect($request->feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                            }
                        }
                    }else{
                        $file = fopen("upload/documents/"."index.html", "w");
                        echo fwrite($file,"Dilarang Masuk Bro !");
                        fclose($file);
                        mkdir($pathTahun);
                        $file = fopen($pathTahun."/"."index.html", "w");
                        echo fwrite($file,"Dibilangin Ga Boleh Masuk !");
                        fclose($file);
                        mkdir($pathDepartment);
                        $file = fopen($pathDepartment."/"."index.html", "w");
                        echo fwrite($file,"Ngeyel Banget dah hahaha !");
                        fclose($file);
                        move_uploaded_file($tmpName, $pathNamaFileBaru);
                        if(!file_exists($pathNamaFileBaru)){
                            return redirect($request->feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                        }
                    }
                }
                // UPLOAD COMMISSIONING
                $txtUploadCommissioningReject = $request->txtUploadCommissioningReject;
                $namaFile = $_FILES ['txtUploadCommissioning']['name'];
                $ukuranFile = $_FILES['txtUploadCommissioning']['size'];
                $error = $_FILES['txtUploadCommissioning']['error'];
                $tmpName =$_FILES['txtUploadCommissioning']['tmp_name'];

                if($error === 4){
                    $namaFileBaru2 = $txtUploadCommissioningReject;
                }else{
                    // cek pastikan yg di upload adalah gambar
                    $ekstensiCommissioningValid = ['pdf'];
                    $ekstensiCommissioning = explode('.', $namaFile);
                    $ekstensiCommissioning= strtolower(end($ekstensiCommissioning));

                    if(!in_array($ekstensiCommissioning, $ekstensiCommissioningValid)){
                        return redirect($request->feedBack)->withFail('Dokumen Yang Diupload Bukan PDF !');
                    }

                    $uniq = $kodeCMF;
                    $namaFileBaru2 = $uniq.'.'.$ekstensiCommissioning;
                    $tahunCMF = date('Y', strtotime($dateCMF));
                    $dpt = DB::table('m_department')
                        ->where('kodeDepartmentMaster', $departmentCMF)
                        ->first();
                    $department = $dpt->namaDepartment;
                    $pathTahun = 'upload/commissioning/'.$tahunCMF;
                    $pathDepartment = 'upload/commissioning/'.$tahunCMF.'/'.$department;
                    $pathNamaFileBaru = 'upload/commissioning/'.$tahunCMF.'/'.$department.'/'.$namaFileBaru2;
                    $documentReject = 'upload/commissioning/'.$tahunCMF.'/'.$department.'/'.$txtUploadDocumentReject;

                    if(is_dir($pathTahun)){
                        if(is_dir($pathDepartment)){
                            move_uploaded_file($tmpName, $pathNamaFileBaru);
                            if(!file_exists($pathNamaFileBaru)){
                                return redirect($request->feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                        }
                        }else{
                            mkdir($pathDepartment);
                            $file = fopen($pathDepartment."/"."index.html", "w");
                            echo fwrite($file,"Ngeyel Banget dah hahaha !");
                            fclose($file);
                            move_uploaded_file($tmpName, $pathNamaFileBaru);
                            if(!file_exists($pathNamaFileBaru)){
                                return redirect($request->feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                            }
                        }
                    }else{
                        $file = fopen("upload/commissioning/"."index.html", "w");
                        echo fwrite($file,"Dilarang Masuk Bro !");
                        fclose($file);
                        mkdir($pathTahun);
                        $file = fopen($pathTahun."/"."index.html", "w");
                        echo fwrite($file,"Dibilangin Ga Boleh Masuk !");
                        fclose($file);
                        mkdir($pathDepartment);
                        $file = fopen($pathDepartment."/"."index.html", "w");
                        echo fwrite($file,"Ngeyel Banget dah hahaha !");
                        fclose($file);
                        move_uploaded_file($tmpName, $pathNamaFileBaru);
                        if(!file_exists($pathNamaFileBaru)){
                            return redirect($request->feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                        }
                    }
                }
                // UPDATE TABLE TR_CMF
                $updateArray1 = array(
                    "pemilikProsesCMF" => "$pemilikProsesCMF",
                    "dateCMF" => "$dateCMF",
                    "departmentCMF" => "$departmentCMF",
                    "areaCMF" => "$areaCMF",
                    "judulPerubahanCMF" => "$judulPerubahanCMF",
                    "dateImplementasiCMF" => "$dateImplementasiCMF",
                    "typePerubahanCMF" => "$typePerubahanCMF",
                    "alasanPerubahanCMF" => "$alasanPerubahanCMF",
                    "dampakPerubahanCMF" => "$dampakPerubahanCMF",
                    "deskripsiPerubahanCMF" => "$deskripsiPerubahanCMF",
                    "txtUploadDocument" => "$namaFileBaru",
                    "approveCMF1" => "$approveCMF1",
                    "dateApproveCMF1" => "$dateApproveCMF1",
                    "catatanDeptheadCMF1" => "$catatanDeptheadCMF1",
                    "approveCMF2" => "$approveCMF2",
                    "dateApproveCMF2" => "$dateApproveCMF2",
                    "approveCMF3" => "$approveCMF3",
                    "dateApproveCMF3" => "$dateApproveCMF3",
                    "approveCMF4" => "$approveCMF4",
                    "dateApproveCMF4" => "$dateApproveCMF4",
                    "approveCMF6" => "$approveCMF6",
                    "dateApproveCMF6" => "$dateApproveCMF6",
                    "detailAktifitasCMF" => "$detailAktifitasCMF",
                    "approveCMF7" => "$approveCMF7",
                    "dateApproveCMF7" => "$dateApproveCMF7",
                    "catatanDeptheadCMF2" => "$catatanDeptheadCMF2",
                    "dateApproveCMF8" => "$dateApproveCMF8",
                    "approveCMF8" => "$approveCMF8",
                    "approveCMF9" => "$approveCMF9",
                    "dateApproveCMF9" => "$dateApproveCMF9",
                    "approveCMF10" => "$approveCMF10",
                    "dateApproveCMF10" => "$dateApproveCMF10",
                    "picNOKCMF" => "$picNOKCMF",
                    "dateNOKCMF" => "$dateNOKCMF",
                    "comentNOKCMF" => "$comentNOKCMF",
                    "statusProsesCMF" => "$statusProsesCMF",
                    "nomorCMF" => "$nomorCMF",
                    "txtUploadCommissioning" => "$namaFileBaru2",
                    "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                    "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                );
                $update1 = DB::table('tr_cmf')
                                    ->where('kodeCMF', $kodeCMF)
                                    ->update($updateArray1);
                if($update1 > 0){
                    // UDPATE TABLE TR_PERUBAHAN
                    $updateArray2 = array(
                        "bitMesin" => "$bitMesin",
                        "bitProses" => "$bitProses",
                        "bitSystem" => "$bitSystem",
                        "bitLainnya" => "$valueLainnya",
                        "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                        "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                    );
                    $update2 = DB::table('tr_perubahan')
                                    ->where('kodeCMF', $kodeCMF)
                                    ->update($updateArray2);
                    if($update2 > 0){
                        // UPDATE TABLE TR_JENIS_PERUBAHAN
                        $jenisPerubahan = DB::table('m_jenis_perubahan')
                                            ->where('bitActiveJenisPerubahan', 1)
                                            ->get();
                        foreach($jenisPerubahan as $jns){
                            $kodeJenisCMF = $jns->kodeJenisPerubahan;
                            $valueJenisPerubahan = $request->$kodeJenisCMF;
                            if($valueJenisPerubahan == 1){
                                $valueJenisPerubahan = 1;
                            }else{
                                $valueJenisPerubahan = 0;
                            }
                            $updateArray3 = array(
                                "valueJenisPerubahan" => "$valueJenisPerubahan",
                                "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                                "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                            );
                            $update3 = DB::table('tr_jenis_perubahan')
                                        ->where('kodeCMF', $kodeCMF)
                                        ->where('kodeJenisCMF', $kodeJenisCMF)
                                        ->update($updateArray3);
                        }
                        $bitLainnya2 = $request->bitLainnya2;
                        $valueLainnya2 = $request->valueLainnya2;
                        if($bitLainnya2 == 1){
                            $lainnya = $valueLainnya2;
                            $updateArrayLainnya = array(
                                "valueJenisPerubahan" => 1,
                                "lainnya" => "$lainnya",
                                "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                                "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                            );
                            $updateLainnya = DB::table('tr_jenis_perubahan')
                                        ->where('kodeCMF', $kodeCMF)
                                        ->where('namaJenisPerubahanCMF', 'Lainnya')
                                        ->update($updateArrayLainnya);
                        }else{
                            $lainnya = '';
                            $updateArrayLainnya = array(
                                "valueJenisPerubahan" => 0,
                                "lainnya" => "$lainnya",
                                "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                                "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                            );
                            $updateLainnya = DB::table('tr_jenis_perubahan')
                                        ->where('kodeCMF', $kodeCMF)
                                        ->where('namaJenisPerubahanCMF', 'Lainnya')
                                        ->update($updateArrayLainnya);
                        }
                        if($update3 > 0){
                            // UPDATE TABLE TR_RESIK_ASSESMENT
                            $updateArray4 = array(
                                "resikoCMF" => "$resikoCMF",
                                "mitigasiCMF" => "$mitigasiCMF",
                                "picRiskAssesmentCMF" => "$picRiskAssesmentCMF",
                                "deadLineCMF" => "$deadLineCMF",
                                "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                                "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                            );
                            $update4 = DB::table('tr_risk_assesment')
                                    ->where('kodeCMF', $kodeCMF)
                                    ->update($updateArray4);
                            if($update4 > 0){
                                // UPDATE TABLE TR_AREA_TREKAIT
                                $area = DB::table('tr_area_terkait')
                                            ->where('kodeCMF', $kodeCMF)
                                            ->orderBy('namaAreaCMF', 'ASC')
                                            ->get();
                                foreach($area as $dd){
                                    $kodeAreaCMF = $dd->kodeAreaCMF;
                                    $kodeDepartment = $dd->kodeDepartmentCMF;
                                    $valueAreaCMF = $request->$kodeAreaCMF;
                                    if($valueAreaCMF == 1){
                                        $valueAreaCMF = 1;
                                    }else{
                                        $valueAreaCMF = 0;
                                    }
                                    $updateArray5 = array(
                                        "valueAreaCMF" => "$valueAreaCMF",
                                        "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                                        "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                                    ); 
                                    // update tabel tr_area_terkait 
                                    $update5 = DB::table('tr_area_terkait')
                                            ->where('kodeCMF', $kodeCMF)
                                            ->where('kodeAreaCMF', $kodeAreaCMF)
                                            ->update($updateArray5);
                                    if($update5 > 0){
                                        // UPDATE TABLE TR_DEPARTMENT_TERKAIT
                                        if($valueAreaCMF == 1){
                                            $cek_dept = DB::table('tr_department_terkait')
                                                            ->where('kodeCMF', $kodeCMF)
                                                            ->where('kodeDepartmentCMF', $kodeDepartment)
                                                            ->count();
                                            // cek apakah departemen yang dipilih sudah di insert ke tr_department_terkait
                                            if($cek_dept == 0){
                                                // KODE DEPARTMENT TERKAIT
                                                $kodeDepartmentTerkaitCMF = DB::table('tr_department_terkait')
                                                                ->max('kodeDepartmentTerkaitCMF');
                                                $noUrut = (int) substr($kodeDepartmentTerkaitCMF, 7, 8);
                                                $noUrut++;
                                                $char = "DPRTCMF";
                                                $kodeDepartmentTerkaitCMF = $char . sprintf("%08s", $noUrut);
                                                $insertArray6 = array(
                                                    "kodeDepartmentTerkaitCMF" => "$kodeDepartmentTerkaitCMF",
                                                    "kodeCMF" => "$kodeCMF",
                                                    "kodeDepartmentCMF" => "$kodeDepartment",
                                                    "txtInsertedBy" => $request->session()->get('cmf_namaKaryawan'),
                                                    "dtmInsertedDate" => date('Y-m-d H:i:s'),
                                                );
                                                $insert1 = DB::table('tr_department_terkait')->insert($insertArray6);
                                                if($insert1 == 0){
                                                    return redirect($request->feedBack)->withFail('Proses Insert Department Terkait Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                                                }
                                            }
                                        }else{
                                            $cek_dept = DB::table('tr_department_terkait')
                                                            ->where('kodeCMF', $kodeCMF)
                                                            ->where('kodeDepartmentCMF', $kodeDepartment)
                                                            ->count();
                                            // cek apakah departemen yang dipilih sudah di insert ke tr_department_terkait
                                            if($cek_dept > 0){
                                                $cek_area = DB::table('tr_area_terkait')
                                                            ->where('kodeCMF', $kodeCMF)
                                                            ->where('kodeDepartmentCMF', $kodeDepartment)
                                                            ->where('valueAreaCMF', '1')
                                                            ->count();
                                                if($cek_area == 0){
                                                    $delete = DB::table('tr_department_terkait')
                                                                ->where('kodeCMF', $kodeCMF)
                                                                ->where('kodeDepartmentCMF', $kodeDepartment)
                                                                ->delete();
                                                    if($delete == 0){
                                                        return redirect($request->feedBack)->withFail('Proses Delete Department Terkait Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                                                    }
                                                }
                                            }
                                        }
                                    }else{
                                        return redirect($request->feedBack)->withFail('Proses Update Area Terkait Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                                    }
                                }
                                // UPDATE TR_DEPARTMENT_TERKAIT 
                                $departmentTerkait = DB::table('tr_department_terkait')
                                                ->where('kodeCMF', $kodeCMF)
                                                ->get();
                                foreach($departmentTerkait as $dt){
                                    $kodeDepartmentTerkaitCMF = $dt->kodeDepartmentTerkaitCMF;
                                    $approvedBy1_char = 'approvedBy1'.$kodeDepartmentTerkaitCMF;
                                    $approvedBy1 = $request->$approvedBy1_char;
                                    $dateReviewCMF_char = 'dateReviewCMF'.$kodeDepartmentTerkaitCMF;
                                    $dateReviewCMF = $request->$dateReviewCMF_char;
                                    $reviewDepartmentCMF_char = 'reviewDepartmentCMF'.$kodeDepartmentTerkaitCMF;
                                    $reviewDepartmentCMF = $request->$reviewDepartmentCMF_char;
                                    $adjustmentDepartment1CMF_char = 'adjustmentDepartment1CMF'.$kodeDepartmentTerkaitCMF;
                                    $adjustmentDepartment1CMF = $request->$adjustmentDepartment1CMF_char;
                                    $approvedBy2_char = 'approvedBy2'.$kodeDepartmentTerkaitCMF;
                                    $approvedBy2 = $request->$approvedBy2_char;
                                    $dateEvaluasiCMF_char = 'dateEvaluasiCMF'.$kodeDepartmentTerkaitCMF;
                                    $dateEvaluasiCMF = $request->$dateEvaluasiCMF_char;
                                    $evaluasiDepartmentCMF_char = 'evaluasiDepartmentCMF'.$kodeDepartmentTerkaitCMF;
                                    $evaluasiDepartmentCMF = $request->$evaluasiDepartmentCMF_char;
                                    $adjustmentDepartment2CMF_char = 'adjustmentDepartment2CMF'.$kodeDepartmentTerkaitCMF;
                                    $adjustmentDepartment2CMF = $request->$adjustmentDepartment2CMF_char;
                                    $updateArray = array(
                                        "approvedBy1" => $approvedBy1,
                                        "dateReviewCMF" => $dateReviewCMF,
                                        "reviewDepartmentCMF" => $reviewDepartmentCMF,
                                        "adjustmentDepartment1CMF" => $adjustmentDepartment1CMF,
                                        "approvedBy2" => $approvedBy2,
                                        "dateEvaluasiCMF" => $dateEvaluasiCMF,
                                        "evaluasiDepartmentCMF" => $evaluasiDepartmentCMF,
                                        "adjustmentDepartment2CMF" => $adjustmentDepartment2CMF,
                                    );
                                    $update = DB::table('tr_department_terkait')
                                                ->where('kodeDepartmentTerkaitCMF', $kodeDepartmentTerkaitCMF)
                                                ->update($updateArray);
                                }
                                // UDPATE VERIFIKASI DOKUMEN
                                $dokumen_count = DB::table('tr_dokumen') //DATA DEPARTMENT TERKAIT
                                                ->where('kodeCMF', $kodeCMF)
                                                ->count();
                                if($dokumen_count > 0){
                                    $dokumen = DB::table('tr_dokumen')
                                            ->where('kodeCMF', $kodeCMF)
                                            ->get();
                                    foreach($dokumen as $dk){
                                        $postDokumen = $dk->postDokumenCMF;
                                        $kodeDokumenCMF = $dk->kodeDokumenCMF;
                                        $valueDokumenCMF = $request->$postDokumen;
                                        if($valueDokumenCMF == 1){
                                            $valueDokumenCMF = 1;
                                        }else{
                                            $valueDokumenCMF = 0;
                                        }
                                        $perubahanDokumenCMF = $request->$kodeDokumenCMF;
                                        $updateArray = array(
                                            "valueDokumenCMF" => $valueDokumenCMF,
                                            "perubahanDokumenCMF" => $perubahanDokumenCMF,
                                        );
                                        $update = DB::table('tr_dokumen')
                                                    ->where('kodeDokumenCMF', $kodeDokumenCMF)
                                                    ->update($updateArray);
                                    }
                                }else{
                                    $dokumen = DB::table('m_dokumen') //DATA DEPARTMENT TERKAIT
                                            ->where('bitActiveDokumen', 1)
                                            ->orderBy('postDokumen', 'ASC')
                                            ->get();
                                    foreach($dokumen as $dk){
                                        //KODE DOKUMEN
                                        $kodeDokumenCMF = DB::table('tr_dokumen')
                                            ->max('kodeDokumenCMF');
                                        $noUrut = (int) substr($kodeDokumenCMF, 3, 7);
                                        $noUrut++;
                                        $char = "CMF";
                                        $kodeDokumenCMF = $char . sprintf("%07s", $noUrut);
                                        $kodeDokumen = $dk->kodeDokumen;
                                        $postDokumenCMF = $dk->postDokumen;
                                        $valueDokumenCMF = $request->$postDokumenCMF;
                                        if($valueDokumenCMF == 1){
                                            $valueDokumenCMF = 1;
                                        }else{
                                            $valueDokumenCMF = 0;
                                        }
                                        $perubahanDokumenCMF = $request->$kodeDokumen;
                                        $insertArray = array(
                                            "kodeDokumenCMF" => "$kodeDokumenCMF",
                                            "kodeCMF" => "$kodeCMF",
                                            "postDokumenCMF" => "$postDokumenCMF",
                                            "valueDokumenCMF" => "$valueDokumenCMF",
                                            "perubahanDokumenCMF" => "$perubahanDokumenCMF",
                                            "txtInsertedBy" => $request->session()->get('cmf_namaKaryawan'),
                                            "dtmInsertedDate" => date('Y-m-d H:i:s'),
                                        );
                                        $insert1 = DB::table('tr_dokumen')->insert($insertArray);
                                    }
                                }
                                return redirect($request->feedBack)->withSuccess('CMF Berhasil Diupdate !');
                            }else{
                                return redirect($request->feedBack)->withFail('Proses Update Risk Assesment Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                            }
                        }else{
                            return redirect($request->feedBack)->withFail('Proses Update Jenis Perubahan Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                        }
                    }else{
                        return redirect($request->feedBack)->withFail('Proses Update Perubahan Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                    }
                }else{
                    return redirect($request->feedBack)->withFail('Proses Update CMF Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                }
            }else{
                return redirect('AllertSession');
            }
        }else{
            return redirect('AllertSession');
        }
    }

    public function insertUser(Request $request){
        if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
                $kodeUser = DB::table('m_user')
                        ->max('kodeUser');
                $noUrut = (int) substr($kodeUser, 4, 4);
                $noUrut++;
                $char = "USER";
                $kodeUser = $char . sprintf("%04s", $noUrut);
                $nik = $request->nik;
                $username = $request->username;
                $posisi = $request->posisi;
                $department = $request->department;
                $namaKaryawan = strtoupper($request->namaKaryawan);
                $string = $request->password;
                $password = md5(sha1(crc32($string)));
                $date = date('Y-m-d', strtotime('+90 days', strtotime(date('Y-m-d H:i:s'))));
                $levelCMF = $request->levelCMF;
                $email = $request->email;
                $area = $request->area;
                $namaPanggilan = $request->namaPanggilan;
                $feedBack = $request->feedBack;
                // UPLOAD GAMBAR
                $gambarUserReject = $request->gambarUserReject;
                $namaFile = $_FILES ['gambarUser']['name'];
                $ukuranFile = $_FILES['gambarUser']['size'];
                $error = $_FILES['gambarUser']['error'];
                $tmpName =$_FILES['gambarUser']['tmp_name'];
                if($error === 4){
                    return redirect('Dc/User')->withFail('Anda Belum Mengupload Gambar !');
                }
                // cek pastikan yg di upload adalah gambar
                $ekstensiGambarValid = ['jpg','jpeg','png','gif','img'];
                $ekstensiGambar = explode('.', $namaFile);
                $ekstensiGambar= strtolower(end($ekstensiGambar));
                if(!in_array($ekstensiGambar, $ekstensiGambarValid)){
                    return redirect('Dc/User')->withFail('File yang Anda Upload Bukan Gambar !');
                }
                $uniq = $nik;
                $namaFileBaru = $uniq.'.'.$ekstensiGambar;
                $path = 'img/profile/'.$namaFileBaru;
                move_uploaded_file($tmpName, $path);
                // INSERT KE TABLE M_USER
                $insertArray = array(
                    "kodeUser" => "$kodeUser",
                    "nik" => "$nik",
                    "username" => "$username",
                    "posisi" => "$posisi",
                    "department" => "$department",
                    "namaKaryawan" => "$namaKaryawan",
                    "password" => "$password",
                    "datePassword" => "$date",
                    "levelCMF" => "$levelCMF",
                    "email" => "$email",
                    "area" => "$area",
                    "namaPanggilan" => "$namaPanggilan",
                    "gambarUser" => "$namaFileBaru",
                    "txtInsertedBy" => $request->session()->get('cmf_namaKaryawan'),
                    "dtmInsertedDate" => date('Y-m-d H:i:s'),
                );
                $insert1 = DB::table('m_user')->insert($insertArray);
                if($insert1 > 0){
                    return redirect($feedBack)->withSuccess('Data Berhasil Dibuat !');
                }else{
                    return redirect($feedBack)->withFail('Data Gagal Dibuat !');
                }
            }else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
    }

    public function updateUser(Request $request){
        if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
                $kodeUser = $request->kodeUser;
                $feedBack = $request->feedBack;
                $nik = $request->nik;
                $username = $request->username;
                $posisi = $request->posisi;
                $department = $request->department;
                $namaKaryawan = strtoupper($request->namaKaryawan);
                $string = $request->password;
                if($string == ""){
                    $dt = DB::table('m_user')
                            ->where('kodeUser', $kodeUser)
                            ->first();
                    $password = $dt->password;
                }else{
                    $password = md5(sha1(crc32($string)));
                }
                $date = date('Y-m-d', strtotime('+90 days', strtotime(date('Y-m-d H:i:s'))));
                $levelCMF = $request->levelCMF;
                $email = $request->email;
                $area = $request->area;
                $namaPanggilan = $request->namaPanggilan;
                $bitActive = $request->bitActive;
                // UPLOAD GAMBAR
                $gambarUserReject = $request->gambarUserReject;
                $namaFile = $_FILES ['gambarUser']['name'];
                $ukuranFile = $_FILES['gambarUser']['size'];
                $error = $_FILES['gambarUser']['error'];
                $tmpName =$_FILES['gambarUser']['tmp_name'];
                if($error === 4){
                    $namaFileBaru = $gambarUserReject;
                }else{
                    // cek pastikan yg di upload adalah gambar
                    $ekstensiGambarValid = ['jpg','jpeg','png','gif','img'];
                    $ekstensiGambar = explode('.', $namaFile);
                    $ekstensiGambar= strtolower(end($ekstensiGambar));
                    if(!in_array($ekstensiGambar, $ekstensiGambarValid)){
                        return redirect('Dc/User')->withFail('File yang Anda Upload Bukan Gambar !');
                    }
                    $uniq = $nik;
                    $namaFileBaru = $uniq.'.'.$ekstensiGambar;
                    $path = 'img/profile/'.$namaFileBaru;
                    $gambarObsolete = 'img/profile/'.$gambarUserReject;
                    if(file_exists($gambarObsolete)){
                        unlink($gambarObsolete);
                        move_uploaded_file($tmpName, $path);
                    }else{
                        move_uploaded_file($tmpName, $path);
                    }
                }
                $updateArray = array(
                    "kodeUser" => "$kodeUser",
                    "nik" => "$nik",
                    "username" => "$username",
                    "posisi" => "$posisi",
                    "department" => "$department",
                    "namaKaryawan" => "$namaKaryawan",
                    "password" => "$password",
                    "datePassword" => "$date",
                    "levelCMF" => "$levelCMF",
                    "email" => "$email",
                    "area" => "$area",
                    "namaPanggilan" => "$namaPanggilan",
                    "gambarUser" => "$namaFileBaru",
                    "bitActive" => "$bitActive",
                    "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                    "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                );
                $update1 = DB::table('m_user')
                                ->where('kodeUser', $kodeUser)
                                ->update($updateArray);
                if($update1 > 0){
                    return redirect($feedBack)->withSuccess('Data Berhasil Diupdate !');
                }else{
                    return redirect($feedBack)->withFail('Data Gagal Diupdate !');
                }
            }else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
    }      

    public function UpdatePassword(Request $request){
        if($request->session()->get('cmf_nik')){
            $kodeUser = $request->kodeUser;
            $feedBack = $request->feedBack;
            $nik = $request->nik;
            $string = $request->password;
            if($string == ""){
                $dt = DB::table('m_user')
                        ->where('kodeUser', $kodeUser)
                        ->first();
                $password = $dt->password;
            }else{
                $password = md5(sha1(crc32($string)));
            }
            // UPLOAD GAMBAR
            $gambarUserReject = $request->gambarUserReject;
            $namaFile = $_FILES ['gambarUser']['name'];
            $ukuranFile = $_FILES['gambarUser']['size'];
            $error = $_FILES['gambarUser']['error'];
            $tmpName =$_FILES['gambarUser']['tmp_name'];
            if($error === 4){
                $namaFileBaru = $gambarUserReject;
            }else{
                // cek pastikan yg di upload adalah gambar
                $ekstensiGambarValid = ['jpg','jpeg','png','gif','img'];
                $ekstensiGambar = explode('.', $namaFile);
                $ekstensiGambar= strtolower(end($ekstensiGambar));
                if(!in_array($ekstensiGambar, $ekstensiGambarValid)){
                    return redirect('Dc/User')->withFail('File yang Anda Upload Bukan Gambar !');
                }
                $uniq = $nik;
                $namaFileBaru = $uniq.'.'.$ekstensiGambar;
                $path = 'img/profile/'.$namaFileBaru;
                $gambarObsolete = 'img/profile/'.$gambarUserReject;
                if(file_exists($gambarObsolete)){
                    unlink($gambarObsolete);
                    move_uploaded_file($tmpName, $path);
                }else{
                    move_uploaded_file($tmpName, $path);
                }
            }
            $updateArray = array(
                "password" => "$password",
                "gambarUser" => "$namaFileBaru",
                "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                "dtmUpdatedDate" => date('Y-m-d H:i:s'),
            );
            $update1 = DB::table('m_user')
                                ->where('kodeUser', $kodeUser)
                                ->update($updateArray);
            if($update1 > 0){
                return redirect($feedBack)->withSuccess('Data Berhasil Diupdate !');
            }else{
                return redirect($feedBack)->withFail('Data Gagal Diupdate !');
            }
        }else{
			return redirect('AllertSession');
		}
    }

    public function insertAreaTerkait(Request $request){
        if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
                $namaAreaTerkait = $request->namaAreaTerkait;
                $kodeDepartment = $request->kodeDepartment;
                $feedBack = $request->feedBack;
                // KODE AREA TERKAIT
                $kodeAreaTerkait = DB::table('m_area_terkait')
                        ->max('kodeAreaTerkait');
                $noUrut = (int) substr($kodeAreaTerkait, 4, 2);
                $noUrut++;
                $char = "AREA";
                $kodeAreaTerkait = $char . sprintf("%02s", $noUrut);
                $encryKodeAreaTerkait = md5(sha1(crc32($kodeAreaTerkait)));
                // UPDATE
                $insertArray = array(
                    "kodeAreaTerkait" => "$kodeAreaTerkait",
                    "namaAreaTerkait" => "$namaAreaTerkait",
                    "encryKodeAreaTerkait" => "$encryKodeAreaTerkait",
                    "kodeDepartment" => "$kodeDepartment",
                    "bitActiveArea" => 1,
                );
                $insert1 = DB::table('m_area_terkait')->insert($insertArray);
                if($insert1 > 0){
                    return redirect($feedBack)->withSuccess('Data Berhasil Dibuat !');
                }else{
                    return redirect($feedBack)->withFail('Data Gagal Dibuat !');
                }
            }else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
    }      

    public function updateAreaTerkait(Request $request){
        if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
                $kodeAreaTerkait = $request->kodeAreaTerkait;
                $namaAreaTerkait = $request->namaAreaTerkait;
                $kodeDepartment = $request->kodeDepartment;
                $bitActiveArea = $request->bitActiveArea;
                $feedBack = $request->feedBack;
                $encryKodeAreaTerkait = md5(sha1(crc32($kodeAreaTerkait)));
                $updateArray = array(
                    "namaAreaTerkait" => "$namaAreaTerkait",
                    "kodeDepartment" => "$kodeDepartment", 
                    "encryKodeAreaTerkait" => "$encryKodeAreaTerkait", 
                    "bitActiveArea" => "$bitActiveArea", 
                );
                $update1 = DB::table('m_area_terkait')
                                ->where('kodeAreaTerkait', $kodeAreaTerkait)
                                ->update($updateArray);
                if($update1 > 0){
                    return redirect($feedBack)->withSuccess('Data Berhasil Diupdate !');
                }else{
                    return redirect($feedBack)->withFail('Data Gagal Diupdate !');
                }
            }else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
    }

    public function insertDepartment(Request $request){
        if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
                
                $namaDepartment = $request->namaDepartment;
                $feedBack = $request->feedBack;
                // KODE DEPARTMENT
                $kodeDepartmentMaster = DB::table('m_department')
                        ->max('kodeDepartmentMaster');
                $noUrut = (int) substr($kodeDepartmentMaster, 4, 2);
                $noUrut++;
                $char = "DPRT";
                $kodeDepartmentMaster = $char . sprintf("%02s", $noUrut);
                $encrykodeDepartment = md5(sha1(crc32($kodeDepartmentMaster)));
                $insertArray = array(
                    "kodeDepartmentMaster" => "$kodeDepartmentMaster",
                    "namaDepartment" => "$namaDepartment",
                    "encryKodeDepartment" => "$encrykodeDepartment",
                    "bitActiveDepartment" => 1,
                );
                $insert1 = DB::table('m_department')->insert($insertArray);
                if($insert1 > 0){
                    return redirect($feedBack)->withSuccess('Data Berhasil Dibuat !');
                }else{
                    return redirect($feedBack)->withFail('Data Gagal Dibuat !');
                }
            }else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
    }   

    public function updateDepartment(Request $request){
        if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
                $kodeDepartmentMaster = $request->kodeDepartmentMaster;
                $namaDepartment = $request->namaDepartment;
                $bitActiveDepartment = $request->bitActiveDepartment;
                $feedBack = $request->feedBack;
                $encryKodeDepartment = md5(sha1(crc32($kodeDepartmentMaster)));
                $updateArray = array(
                    "namaDepartment" => "$namaDepartment",
                    "bitActiveDepartment" => "$bitActiveDepartment",
                    "encryKodeDepartment" => "$encryKodeDepartment",
                );
                $update1 = DB::table('m_department')
                                ->where('kodeDepartmentMaster', $kodeDepartmentMaster)
                                ->update($updateArray);
                if($update1 > 0){
                    return redirect($feedBack)->withSuccess('Data Berhasil Diupdate !');
                }else{
                    return redirect($feedBack)->withFail('Data Gagal Diupdate !');
                }
            }else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
    }

    public function insertDokumen(Request $request){
        if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
                $postDokumen = $request->postDokumen;
                $feedBack = $request->feedBack;
                // KODE DEPARTMENT
                $kodeDokumen = DB::table('m_dokumen')
                        ->max('kodeDokumen');
                $noUrut = (int) substr($kodeDokumen, 3, 2);
                $noUrut++;
                $char = "DKM";
                $kodeDokumen = $char . sprintf("%02s", $noUrut);
                $insertArray = array(
                    "kodeDokumen" => "$kodeDokumen",
                    "postDokumen" => "$postDokumen",
                    "bitActiveDokumen" => 1,
                );
                $insert1 = DB::table('m_dokumen')->insert($insertArray);
                if($insert1 > 0){
                    return redirect($feedBack)->withSuccess('Data Berhasil Dibuat !');
                }else{
                    return redirect($feedBack)->withFail('Data Gagal Dibuat !');
                }
            }else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
    }   

    public function updateDokumen(Request $request){
        if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
                $kodeDokumen = $request->kodeDokumen;
                $postDokumen = $request->postDokumen;
                $bitActiveDokumen = $request->bitActiveDokumen;
                $feedBack = $request->feedBack;
                $updateArray = array(
                    "postDokumen" => "$postDokumen",
                    "bitActiveDokumen" => "$bitActiveDokumen", 
                );
                $update1 = DB::table('m_dokumen')
                                ->where('kodeDokumen', $kodeDokumen)
                                ->update($updateArray);
                if($update1 > 0){
                    return redirect($feedBack)->withSuccess('Data Berhasil Diupdate !');
                }else{
                    return redirect($feedBack)->withFail('Data Gagal Diupdate !');
                }
            }else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
    }

    public function insertJenisPerubahan(Request $request){
        if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
                $namaJenisPerubahan = $request->namaJenisPerubahan;
                $feedBack = $request->feedBack;
                // KODE DEPARTMENT
                $kodeJenisPerubahan = DB::table('m_jenis_perubahan')
                        ->max('kodeJenisPerubahan');
                $noUrut = (int) substr($kodeJenisPerubahan, 6, 2);
                $noUrut++;
                $char = "JNSPRB";
                $kodeJenisPerubahan = $char . sprintf("%02s", $noUrut);
                $insertArray = array(
                    "kodeJenisPerubahan" => "$kodeJenisPerubahan",
                    "namaJenisPerubahan" => "$namaJenisPerubahan",
                    "bitActiveJenisPerubahan" => 1,
                );
                $insert1 = DB::table('m_jenis_perubahan')->insert($insertArray);
                if($insert1 > 0){
                    return redirect($feedBack)->withSuccess('Data Berhasil Dibuat !');
                }else{
                    return redirect($feedBack)->withFail('Data Gagal Dibuat !');
                }
            }else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
    }   

    public function updateJenisPerubahan(Request $request){
        if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL06'){
                $kodeJenisPerubahan = $request->kodeJenisPerubahan;
                $namaJenisPerubahan = $request->namaJenisPerubahan;
                $bitActiveJenisPerubahan = $request->bitActiveJenisPerubahan;
                $feedBack = $request->feedBack;
                $updateArray = array(
                    "namaJenisPerubahan" => "$namaJenisPerubahan",
                    "bitActiveJenisPerubahan" => "$bitActiveJenisPerubahan", 
                );
                $update1 = DB::table('m_jenis_perubahan')
                                ->where('kodeJenisPerubahan', $kodeJenisPerubahan)
                                ->update($updateArray);
                if($update1 > 0){
                    return redirect($feedBack)->withSuccess('Data Berhasil Diupdate !');
                }else{
                    return redirect($feedBack)->withFail('Data Gagal Diupdate !');
                }
            }else{
				return redirect('AllertSession');
			}
		}else{
			return redirect('AllertSession');
		}
    }

    public function insertCMF(Request $request){
        if($request->session()->get('cmf_nik')){
            //KODE CMF
            $kodeCMF = DB::table('tr_cmf')
                ->max('kodeCMF');
            $noUrut = (int) substr($kodeCMF, 3, 7);
            $noUrut++;
            $char = "CMF";
            $kodeCMF = $char . sprintf("%07s", $noUrut);
            //KODE PERUBAHAN
            $kodePerubahan = DB::table('tr_perubahan')
                ->max('kodePerubahanCMF');
            $noUrut = (int) substr($kodePerubahan, 6, 7);
            $noUrut++;
            $char = "PRBCMF";
            $kodePerubahan = $char . sprintf("%07s", $noUrut);
            // KODE RISK ASSESMENT
            $kodeRiskAssesment = DB::table('tr_risk_assesment')
                ->max('kodeRiskAssesment');
            $noUrut = (int) substr($kodeRiskAssesment, 3, 7);
            $noUrut++;
            $char = "RSK";
            $kodeRiskAssesment = $char . sprintf("%07s", $noUrut);
            // DATA POST
            $dateCMF = $request->dateCMF;
            $feedBack = $request->feedBack;
            $judulPerubahanCMF = $request->judulPerubahanCMF;
            $bitMesin = $request->bitMesin;
            if($bitMesin == 1){
                $bitMesin = 1;
            }else{
                $bitMesin = 0;
            }
            $bitProses = $request->bitProses;
            if($bitProses == 1){
                $bitProses = 1;
            }else{
                $bitProses = 0;
            }
            $bitSystem = $request->bitSystem;
            if($bitSystem == 1){
                $bitSystem = 1;
            }else{
                $bitSystem = 0;
            }
            $bitLainnya = $request->bitLainnya;
            if($bitLainnya == 1){
                $valueLainnya = $request->valueLainnya;
            }else{
                $valueLainnya = "";
            }
            $bitInstallasi = $request->bitInstallasi;
            $bitFormula = $request->bitFormula;
            $bitRawMaterial = $request->bitRawMaterial;
            $bitPackagingMaterial = $request->bitPackagingMaterial;
            $bitSpesifikasiProduk = $request->bitSpesifikasiProduk;
            $bitFoodSafetyManagement = $request->bitFoodSafetyManagement;
            $bitOracle = $request->bitOracle;
            $bitSmk3 = $request->bitSmk3;
            $bitHalalManagement = $request->bitHalalManagement;
            $bitDigitalisasi = $request->bitDigitalisasi;
            $dateImplementasiCMF = $request->dateImplementasiCMF;
            $typePerubahanCMF = $request->typePerubahanCMF;
            $alasanPerubahanCMF = $request->alasanPerubahanCMF;
            $dampakPerubahanCMF = $request->dampakPerubahanCMF;
            $deskripsiPerubahanCMF = $request->deskripsiPerubahanCMF;
            $resikoCMF = $request->resikoCMF;
            $mitigasiCMF = $request->mitigasiCMF;
            $picRiskAssesmentCMF = $request->picRiskAssesmentCMF;
            $deadLineCMF = $request->deadLineCMF;
            $nomorCapex = $request->nomorCapex;
            $encryKodeCMF = md5(sha1(crc32($kodeCMF)));
            // DATA SESSION
            $departmentCMF = $request->session()->get('cmf_department');
            $areaCMF = $request->session()->get('cmf_area');
            $pemilikProsesCMF = $request->session()->get('cmf_nik');
            // NOMOR CMF
            $tahun = date('Y', strtotime($dateCMF));
		    $bulan = date('m', strtotime($dateCMF));
            $nomorCMF = DB::table('tr_cmf')
                ->select(DB::raw('MAX(nomorCMF) as nomor'))
                ->whereYear('dateCMF', $tahun)
                ->whereMonth('dateCMF', $bulan)
                ->where('kodeCMF', '!=', $kodeCMF)
                ->first();
            $exp = explode('/', $nomorCMF->nomor);
            $noUrut = $exp[0];
            $noUrut = (int) substr($noUrut, 0, 3);
            $no = $noUrut+1;
            $nmrCMF =sprintf("%03s", $no);
            switch ($bulan) {
                case '01':
                    $blnCMF = 'I';
                    break;
                case '02':
                    $blnCMF = 'II';
                    break;
                case '03':
                    $blnCMF = 'III';
                    break;
                case '04':
                    $blnCMF = 'IV';
                    break;
                case '05':
                    $blnCMF = 'V';
                    break;
                case '06':
                    $blnCMF = 'VI';
                    break;
                case '07':
                    $blnCMF = 'VII';
                    break;
                case '08':
                    $blnCMF = 'VIII';
                    break;
                case '09':
                    $blnCMF = 'IX';
                    break;
                case '10':
                    $blnCMF = 'X';
                    break;
                case '11':
                    $blnCMF = 'XI';
                    break;
                case '12':
                    $blnCMF = 'XII';
                    break;
                
                default:
                    # code...
                    break;
            }
            // tahun
            $thnCMF = substr($tahun, -2);
            $nomorCMF = $nmrCMF.'/CMF/'.$blnCMF.'/'.$thnCMF;
            // UPLOAD DOKUMEN
            $txtUploadDocumentReject = $request->txtUploadDocumentReject;
            $namaFile = $_FILES ['txtUploadDocument']['name'];
            $ukuranFile = $_FILES['txtUploadDocument']['size'];
            $error = $_FILES['txtUploadDocument']['error'];
            $tmpName =$_FILES['txtUploadDocument']['tmp_name'];

            if($error === 4){
                $namaFileBaru = $txtUploadDocumentReject;
            }else{
                // cek pastikan yg di upload adalah gambar
                $ekstensiDocumentValid = ['pdf'];
                $ekstensiDocument = explode('.', $namaFile);
                $ekstensiDocument= strtolower(end($ekstensiDocument));

                if(!in_array($ekstensiDocument, $ekstensiDocumentValid)){
                    return redirect($feedBack)->withFail('Dokumen Yang Diupload Bukan PDF !');
                }

                $uniq = $kodeCMF;
                $namaFileBaru = $uniq.'.'.$ekstensiDocument;
                $tahunCMF = date('Y', strtotime($dateCMF));
                $dpt = DB::table('m_department')
                    ->where('kodeDepartmentMaster', $departmentCMF)
                    ->first();
                $department = $dpt->namaDepartment;
                $pathTahun = 'upload/documents/'.$tahunCMF;
                $pathDepartment = 'upload/documents/'.$tahunCMF.'/'.$department;
                $pathNamaFileBaru = 'upload/documents/'.$tahunCMF.'/'.$department.'/'.$namaFileBaru;
                $documentReject = 'upload/documents/'.$tahunCMF.'/'.$department.'/'.$txtUploadDocumentReject;

                if(is_dir($pathTahun)){
                    if(is_dir($pathDepartment)){
                        move_uploaded_file($tmpName, $pathNamaFileBaru);
                        if(!file_exists($pathNamaFileBaru)){
                            return redirect($feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                    }
                    }else{
                        mkdir($pathDepartment);
                        $file = fopen($pathDepartment."/"."index.html", "w");
                        echo fwrite($file,"Ngeyel Banget dah hahaha !");
                        fclose($file);
                        move_uploaded_file($tmpName, $pathNamaFileBaru);
                        if(!file_exists($pathNamaFileBaru)){
                            return redirect($feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                        }
                    }
                }else{
                    $file = fopen("upload/documents/"."index.html", "w");
                    echo fwrite($file,"Dilarang Masuk Bro !");
                    fclose($file);
                    mkdir($pathTahun);
                    $file = fopen($pathTahun."/"."index.html", "w");
                    echo fwrite($file,"Dibilangin Ga Boleh Masuk !");
                    fclose($file);
                    mkdir($pathDepartment);
                    $file = fopen($pathDepartment."/"."index.html", "w");
                    echo fwrite($file,"Ngeyel Banget dah hahaha !");
                    fclose($file);
                    move_uploaded_file($tmpName, $pathNamaFileBaru);
                    if(!file_exists($pathNamaFileBaru)){
                        return redirect($feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                    }
                }
            }
            $insertArray = array(
                "kodeCMF" => "$kodeCMF",
                "pemilikProsesCMF" => "$pemilikProsesCMF",
                "dateCMF" => "$dateCMF",
                "departmentCMF" => "$departmentCMF",
                "areaCMF" => "$areaCMF",
                "judulPerubahanCMF" => "$judulPerubahanCMF",
                "dateImplementasiCMF" => "$dateImplementasiCMF",
                "typePerubahanCMF" => "$typePerubahanCMF",
                "alasanPerubahanCMF" => "$alasanPerubahanCMF",
                "dampakPerubahanCMF" => "$dampakPerubahanCMF",
                "deskripsiPerubahanCMF" => "$deskripsiPerubahanCMF",
                "txtUploadDocument" => "$namaFileBaru",
                "encryKodeCMF" => "$encryKodeCMF",
                "nomorCMF" => "$nomorCMF",
                "nomorCapex" => "$nomorCapex",
                "statusProsesCMF" => "0",
                "bitActive" => "1",
                "txtInsertedBy" => $request->session()->get('cmf_namaKaryawan'),
                "dtmInsertedDate" => date('Y-m-d H:i:s'),
            );
            $insert1 = DB::table('tr_cmf')->insert($insertArray);
            if($insert1 > 0){
                // INSERT KE TABLE TR_PERUBAHAN
                $insertArray2 = array(
                    "kodePerubahanCMF" => "$kodePerubahan",
                    "kodeCMF" => "$kodeCMF",
                    "bitMesin" => "$bitMesin",
                    "bitProses" => "$bitProses",
                    "bitSystem" => "$bitSystem",
                    "bitLainnya" => "$valueLainnya",
                    "txtInsertedBy" => $request->session()->get('cmf_namaKaryawan'),
                    "dtmInsertedDate" => date('Y-m-d H:i:s'),
                );
                $insert2 = DB::table('tr_perubahan')->insert($insertArray2);
                if($insert2 > 0){
                    // INSERT KE TABLE TR_JENIS_PERUBAHAN
                    $jenisPerubahan = DB::table('m_jenis_perubahan')
                                        ->where('bitActiveJenisPerubahan', 1)
                                        ->get();
                    foreach($jenisPerubahan as $jns){
                        // KODE JENIS PERUBAHAN
                        $kodeJenisPerubahan = DB::table('tr_jenis_perubahan')
                            ->max('kodeJenisPerubahanCMF');
                        $noUrut = (int) substr($kodeJenisPerubahan, 9, 7);
                        $noUrut++;
                        $char = "JNSPRBCMF";
                        $kodeJenisPerubahan = $char . sprintf("%07s", $noUrut);
                        $kodeJenisCMF = $jns->kodeJenisPerubahan;
                        $namaJenisPerubahanCMF = $jns->namaJenisPerubahan;
                        $valueJenisPerubahan = $request->$kodeJenisCMF;
                        if($valueJenisPerubahan == 1){
                            $valueJenisPerubahan = 1;
                        }else{
                            $valueJenisPerubahan = 0;
                        }
                        $insertArray3 = array(
                            "kodeJenisPerubahanCMF" => "$kodeJenisPerubahan",
                            "kodeCMF" => "$kodeCMF",
                            "kodeJenisCMF" => "$kodeJenisCMF",
                            "namaJenisPerubahanCMF" => "$namaJenisPerubahanCMF",
                            "valueJenisPerubahan" => "$valueJenisPerubahan",
                            "txtInsertedBy" => $request->session()->get('cmf_namaKaryawan'),
                            "dtmInsertedDate" => date('Y-m-d H:i:s'),
                        );
                        $insert3 = DB::table('tr_jenis_perubahan')->insert($insertArray3);
                    }
                    $bitLainnya2 = $request->bitLainnya2;
                    if($bitLainnya2 == 1){
                        $bitLainnya2 = $request->bitLainnya2;
                    }else{
                        $bitLainnya2 = 0;
                    }
                    $kodeJenisPerubahan = DB::table('tr_jenis_perubahan')
                        ->max('kodeJenisPerubahanCMF');
                    $noUrut = (int) substr($kodeJenisPerubahan, 9, 7);
                    $noUrut++;
                    $char = "JNSPRBCMF";
                    $kodeJenisPerubahan = $char . sprintf("%07s", $noUrut);
                    $valueLainnya2 = $request->valueLainnya2;
                    $insertArrayLainnya = array(
                        "kodeJenisPerubahanCMF" => "$kodeJenisPerubahan",
                        "kodeCMF" => "$kodeCMF",
                        "namaJenisPerubahanCMF" => "Lainnya",
                        "valueJenisPerubahan" => $bitLainnya2,
                        "lainnya" => "$valueLainnya2",
                        "txtInsertedBy" => $request->session()->get('cmf_namaKaryawan'),
                        "dtmInsertedDate" => date('Y-m-d H:i:s'),
                    );
                    $insertLainnya = DB::table('tr_jenis_perubahan')->insert($insertArrayLainnya);
                    if($insert3 > 0){
                        // INSRET KE TABLE TR_RISK_ASSESMENT
                        $insertArray4 = array(
                            "kodeRiskAssesment" => "$kodeRiskAssesment",
                            "kodeCMF" => "$kodeCMF",
                            "resikoCMF" => "$resikoCMF",
                            "mitigasiCMF" => "$mitigasiCMF",
                            "picRiskAssesmentCMF" => "$picRiskAssesmentCMF",
                            "deadLineCMF" => "$deadLineCMF",
                            "txtInsertedBy" => $request->session()->get('cmf_namaKaryawan'),
                            "dtmInsertedDate" => date('Y-m-d H:i:s'),
                        );
                        $insert4 = DB::table('tr_risk_assesment')->insert($insertArray4);
                        if($insert4 > 0){
                            // INSERT KE TABLE TR_AREA_TERKAIT
                            $area = DB::table('m_area_terkait')
                                        ->where('kodeDepartment', '!=', $departmentCMF)
                                        ->where('bitActiveArea', '1')
                                        ->orderBy('namaAreaTerkait', 'ASC')
                                        ->get();
                            foreach($area as $dd){
                                // KODE AREA TERKAIT
                                $kodeAreaTerkaitCMF = DB::table('tr_area_terkait')
                                                ->max('kodeAreaTerkaitCMF');
                                $noUrut = (int) substr($kodeAreaTerkaitCMF, 7, 8);
                                $noUrut++;
                                $char = "AREACMF";
                                $kodeAreaTerkaitCMF = $char . sprintf("%08s", $noUrut);
                                $kodeAreaTerkait = $dd->kodeAreaTerkait;
                                $namaAreaTerkait = $dd->namaAreaTerkait;
                                $kodeDepartment = $dd->kodeDepartment;
                                $valueAreaCMF = $request->$kodeAreaTerkait;
                                if($valueAreaCMF == 1){
                                    $valueAreaCMF = 1;
                                }else{
                                    $valueAreaCMF = 0;
                                }
                                $insertArray5 = array(
                                    "kodeAreaTerkaitCMF" => "$kodeAreaTerkaitCMF",
                                    "kodeCMF" => "$kodeCMF",
                                    "kodeAreaCMF" => "$kodeAreaTerkait",
                                    "kodeDepartmentCMF" => "$kodeDepartment",
                                    "namaAreaCMF" => "$namaAreaTerkait",
                                    "valueAreaCMF" => "$valueAreaCMF",
                                    "txtInsertedBy" => $request->session()->get('cmf_namaKaryawan'),
                                    "dtmInsertedDate" => date('Y-m-d H:i:s'),
                                );  
                                $insert5 = DB::table('tr_area_terkait')->insert($insertArray5);
                                if($insert5 > 0){
                                    // INSERT KE TABLE TR_DEPARTMENT_TERKAIT
                                    if($valueAreaCMF == 1){
                                        $cek_dept = DB::table('tr_department_terkait')
                                                    ->where('kodeCMF', $kodeCMF)
                                                    ->where('kodeDepartmentCMF', $kodeDepartment)
                                                    ->count();
                                        // cek apakah departemen yang dipilih sudah di insert ke tr_department_terkait
                                        if($cek_dept == 0){
                                            // KODE DEPARTMENT TERKAIT
                                            $kodeDepartmentTerkaitCMF = DB::table('tr_department_terkait')
                                                            ->max('kodeDepartmentTerkaitCMF');
                                            $noUrut = (int) substr($kodeDepartmentTerkaitCMF, 7, 8);
                                            $noUrut++;
                                            $char = "DPRTCMF";
                                            $kodeDepartmentTerkaitCMF = $char . sprintf("%08s", $noUrut);
                                            $insertArray6 = array(
                                                "kodeDepartmentTerkaitCMF" => "$kodeDepartmentTerkaitCMF",
                                                "kodeCMF" => "$kodeCMF",
                                                "adjustmentDepartment1CMF" => 0,
                                                "adjustmentDepartment2CMF" => 0,
                                                "kodeDepartmentCMF" => "$kodeDepartment",
                                                "txtInsertedBy" => $request->session()->get('cmf_namaKaryawan'),
                                                "dtmInsertedDate" => date('Y-m-d H:i:s'),
                                            );
                                            $insert6 = DB::table('tr_department_terkait')->insert($insertArray6);
                                            if($insert6 == 0){
                                                return redirect($feedBack)->withFail('Proses Insert Department Terkait Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                                            }
                                        }
                                    }
                                }else{
                                    return redirect($feedBack)->withFail('Proses Insert Area Terkait Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                                }
                            }
                            return redirect($feedBack)->withSuccess('CMF Berhasil Dibuat !');
                        }else{
                            return redirect($feedBack)->withFail('Proses Insert Risk Assesment Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                        }
                    }else{
                        return redirect($feedBack)->withFail('Proses Insert Jenis Perubahan Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                    }
                }else{
                    return redirect($feedBack)->withFail('Proses Insert Perubahan Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                }
            }else{
                return redirect($feedBack)->withFail('Proses Insert CMF Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
            }
        }else{
            return redirect('AllertSession');
        }
    }

    public function UpdateCMF(Request $request){
        if($request->session()->get('cmf_nik')){
            // DATA POST
            $kodeCMF = $request->kodeCMF;
            $dateCMF = $request->dateCMF;
            $judulPerubahanCMF = $request->judulPerubahanCMF;
            $bitMesin = $request->bitMesin;
            if($bitMesin == 1){
                $bitMesin = 1;
            }else{
                $bitMesin = 0;
            }
            $bitProses = $request->bitProses;
            if($bitProses == 1){
                $bitProses = 1;
            }else{
                $bitProses = 0;
            }
            $bitSystem = $request->bitSystem;
            if($bitSystem == 1){
                $bitSystem = 1;
            }else{
                $bitSystem = 0;
            }
            $bitLainnya = $request->bitLainnya;
            if($bitLainnya == 1){
                $valueLainnya = $request->valueLainnya;
            }else{
                $valueLainnya = "";
            }
            $bitInstallasi = $request->bitInstallasi;
            $bitFormula = $request->bitFormula;
            $bitRawMaterial = $request->bitRawMaterial;
            $bitPackagingMaterial = $request->bitPackagingMaterial;
            $bitSpesifikasiProduk = $request->bitSpesifikasiProduk;
            $bitFoodSafetyManagement = $request->bitFoodSafetyManagement;
            $bitOracle = $request->bitOracle;
            $bitSmk3 = $request->bitSmk3;
            $bitHalalManagement = $request->bitHalalManagement;
            $bitDigitalisasi = $request->bitDigitalisasi;
            $dateImplementasiCMF = $request->dateImplementasiCMF;
            $typePerubahanCMF = $request->typePerubahanCMF;
            $alasanPerubahanCMF = $request->alasanPerubahanCMF;
            $dampakPerubahanCMF = $request->dampakPerubahanCMF;
            $deskripsiPerubahanCMF = $request->deskripsiPerubahanCMF;
            $resikoCMF = $request->resikoCMF;
            $mitigasiCMF = $request->mitigasiCMF;
            $picRiskAssesmentCMF = $request->picRiskAssesmentCMF;
            $deadLineCMF = $request->deadLineCMF;
            $nomorCapex = $request->nomorCapex;
            // DATA SESSION
            $departmentCMF = $request->session()->get('cmf_department');
            $areaCMF = $request->session()->get('cmf_area');
            $pemilikProsesCMF = $request->session()->get('cmf_nik');
            // NOMOR CMF
            $tahun = date('Y', strtotime($dateCMF));
		    $bulan = date('m', strtotime($dateCMF));
            $nomorCMF = DB::table('tr_cmf')
                ->selectRaw(DB::raw('MAX(nomorCMF) as nomor'))
                ->whereYear('dateCMF', $tahun)
                ->whereMonth('dateCMF', $bulan)
                ->where('kodeCMF', '!=', $kodeCMF)
                ->first();
            $exp = explode('/', $nomorCMF->nomor);
            $noUrut = $exp[0];
            $noUrut = (int) substr($noUrut, 0, 3);
            $no = $noUrut+1;
            $nmrCMF =sprintf("%03s", $no);
            switch ($bulan) {
                case '01':
                    $blnCMF = 'I';
                    break;
                case '02':
                    $blnCMF = 'II';
                    break;
                case '03':
                    $blnCMF = 'III';
                    break;
                case '04':
                    $blnCMF = 'IV';
                    break;
                case '05':
                    $blnCMF = 'V';
                    break;
                case '06':
                    $blnCMF = 'VI';
                    break;
                case '07':
                    $blnCMF = 'VII';
                    break;
                case '08':
                    $blnCMF = 'VIII';
                    break;
                case '09':
                    $blnCMF = 'IX';
                    break;
                case '10':
                    $blnCMF = 'X';
                    break;
                case '11':
                    $blnCMF = 'XI';
                    break;
                case '12':
                    $blnCMF = 'XII';
                    break;
                
                default:
                    # code...
                    break;
            }
            // tahun
            $thnCMF = substr($tahun, -2);
            $nomorCMF = $nmrCMF.'/CMF/'.$blnCMF.'/'.$thnCMF;
            // UPLOAD DOKUMEN
            $txtUploadDocumentReject = $request->txtUploadDocumentReject;
            $namaFile = $_FILES ['txtUploadDocument']['name'];
            $ukuranFile = $_FILES['txtUploadDocument']['size'];
            $error = $_FILES['txtUploadDocument']['error'];
            $tmpName =$_FILES['txtUploadDocument']['tmp_name'];

            if($error === 4){
                $namaFileBaru = $txtUploadDocumentReject;
            }else{
                // cek pastikan yg di upload adalah gambar
                $ekstensiDocumentValid = ['pdf'];
                $ekstensiDocument = explode('.', $namaFile);
                $ekstensiDocument= strtolower(end($ekstensiDocument));

                if(!in_array($ekstensiDocument, $ekstensiDocumentValid)){
                    return redirect($request->feedBack)->withFail('Dokumen Yang Diupload Bukan PDF !');
                }

                $uniq = $kodeCMF;
                $namaFileBaru = $uniq.'.'.$ekstensiDocument;
                $tahunCMF = date('Y', strtotime($dateCMF));
                $dpt = DB::table('m_department')
                    ->where('kodeDepartmentMaster', $departmentCMF)
                    ->first();
                $department = $dpt->namaDepartment;
                $pathTahun = 'upload/documents/'.$tahunCMF;
                $pathDepartment = 'upload/documents/'.$tahunCMF.'/'.$department;
                $pathNamaFileBaru = 'upload/documents/'.$tahunCMF.'/'.$department.'/'.$namaFileBaru;
                $documentReject = 'upload/documents/'.$tahunCMF.'/'.$department.'/'.$txtUploadDocumentReject;

                if(is_dir($pathTahun)){
                    if(is_dir($pathDepartment)){
                        move_uploaded_file($tmpName, $pathNamaFileBaru);
                        if(!file_exists($pathNamaFileBaru)){
                            return redirect($request->feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                    }
                    }else{
                        mkdir($pathDepartment);
                        $file = fopen($pathDepartment."/"."index.html", "w");
                        echo fwrite($file,"Ngeyel Banget dah hahaha !");
                        fclose($file);
                        move_uploaded_file($tmpName, $pathNamaFileBaru);
                        if(!file_exists($pathNamaFileBaru)){
                            return redirect($request->feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                        }
                    }
                }else{
                    $file = fopen("upload/documents/"."index.html", "w");
                    echo fwrite($file,"Dilarang Masuk Bro !");
                    fclose($file);
                    mkdir($pathTahun);
                    $file = fopen($pathTahun."/"."index.html", "w");
                    echo fwrite($file,"Dibilangin Ga Boleh Masuk !");
                    fclose($file);
                    mkdir($pathDepartment);
                    $file = fopen($pathDepartment."/"."index.html", "w");
                    echo fwrite($file,"Ngeyel Banget dah hahaha !");
                    fclose($file);
                    move_uploaded_file($tmpName, $pathNamaFileBaru);
                    if(!file_exists($pathNamaFileBaru)){
                        return redirect($request->feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                    }
                }
            }
            // UPDATE TABLE TR_CMF
            $updateArray1 = array(
                "pemilikProsesCMF" => "$pemilikProsesCMF",
                "dateCMF" => "$dateCMF",
                "departmentCMF" => "$departmentCMF",
                "areaCMF" => "$areaCMF",
                "judulPerubahanCMF" => "$judulPerubahanCMF",
                "dateImplementasiCMF" => "$dateImplementasiCMF",
                "typePerubahanCMF" => "$typePerubahanCMF",
                "alasanPerubahanCMF" => "$alasanPerubahanCMF",
                "dampakPerubahanCMF" => "$dampakPerubahanCMF",
                "deskripsiPerubahanCMF" => "$deskripsiPerubahanCMF",
                "txtUploadDocument" => "$namaFileBaru",
                "nomorCMF" => "$nomorCMF",
                "nomorCapex" => "$nomorCapex",
                "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                "dtmUpdatedDate" => date('Y-m-d H:i:s'),
            );
            $update1 = DB::table('tr_cmf')
                                ->where('kodeCMF', $kodeCMF)
                                ->update($updateArray1);
            if($update1 > 0){
                // UDPATE TABLE TR_PERUBAHAN
                $updateArray2 = array(
                    "bitMesin" => "$bitMesin",
                    "bitProses" => "$bitProses",
                    "bitSystem" => "$bitSystem",
                    "bitLainnya" => "$valueLainnya",
                    "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                    "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                );
                $update2 = DB::table('tr_perubahan')
                                ->where('kodeCMF', $kodeCMF)
                                ->update($updateArray2);
                if($update2 > 0){
                    // UPDATE TABLE TR_JENIS_PERUBAHAN
                    $jenisPerubahan = DB::table('m_jenis_perubahan')
                                        ->where('bitActiveJenisPerubahan', 1)
                                        ->get();
                    foreach($jenisPerubahan as $jns){
                        $kodeJenisCMF = $jns->kodeJenisPerubahan;
                        $valueJenisPerubahan = $request->$kodeJenisCMF;
                        if($valueJenisPerubahan == 1){
                            $valueJenisPerubahan = 1;
                        }else{
                            $valueJenisPerubahan = 0;
                        }
                        $updateArray3 = array(
                            "valueJenisPerubahan" => "$valueJenisPerubahan",
                            "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                            "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                        );
                        $update3 = DB::table('tr_jenis_perubahan')
                                    ->where('kodeCMF', $kodeCMF)
                                    ->where('kodeJenisCMF', $kodeJenisCMF)
                                    ->update($updateArray3);
                    }
                    $bitLainnya2 = $request->bitLainnya2;
                    $valueLainnya2 = $request->valueLainnya2;
                    if($bitLainnya2 == 1){
                        $lainnya = $valueLainnya2;
                        $updateArrayLainnya = array(
                            "valueJenisPerubahan" => 1,
                            "lainnya" => "$lainnya",
                            "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                            "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                        );
                        $updateLainnya = DB::table('tr_jenis_perubahan')
                                    ->where('kodeCMF', $kodeCMF)
                                    ->where('namaJenisPerubahanCMF', 'Lainnya')
                                    ->update($updateArrayLainnya);
                    }else{
                        $lainnya = '';
                        $updateArrayLainnya = array(
                            "valueJenisPerubahan" => 0,
                            "lainnya" => "$lainnya",
                            "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                            "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                        );
                        $updateLainnya = DB::table('tr_jenis_perubahan')
                                    ->where('kodeCMF', $kodeCMF)
                                    ->where('namaJenisPerubahanCMF', 'Lainnya')
                                    ->update($updateArrayLainnya);
                    }
                    if($update3 > 0){
                        // UPDATE TABLE TR_RESIK_ASSESMENT
                        $updateArray4 = array(
                            "resikoCMF" => "$resikoCMF",
                            "mitigasiCMF" => "$mitigasiCMF",
                            "picRiskAssesmentCMF" => "$picRiskAssesmentCMF",
                            "deadLineCMF" => "$deadLineCMF",
                            "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                            "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                        );
                        $update4 = DB::table('tr_risk_assesment')
                                ->where('kodeCMF', $kodeCMF)
                                ->update($updateArray4);
                        if($update4 > 0){
                            // UPDATE TABLE TR_AREA_TREKAIT
                            $area = DB::table('tr_area_terkait')
                                        ->where('kodeCMF', $kodeCMF)
                                        ->orderBy('namaAreaCMF', 'ASC')
                                        ->get();
                            foreach($area as $dd){
                                $kodeAreaCMF = $dd->kodeAreaCMF;
                                $kodeDepartment = $dd->kodeDepartmentCMF;
                                $valueAreaCMF = $request->$kodeAreaCMF;
                                if($valueAreaCMF == 1){
                                    $valueAreaCMF = 1;
                                }else{
                                    $valueAreaCMF = 0;
                                }
                                $updateArray5 = array(
                                    "valueAreaCMF" => "$valueAreaCMF",
                                    "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                                    "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                                ); 
                                // update tabel tr_area_terkait 
                                $update5 = DB::table('tr_area_terkait')
                                        ->where('kodeCMF', $kodeCMF)
                                        ->where('kodeAreaCMF', $kodeAreaCMF)
                                        ->update($updateArray5);
                                if($update5 > 0){
                                    // UPDATE TABLE TR_DEPARTMENT_TERKAIT
                                    if($valueAreaCMF == 1){
                                        $cek_dept = DB::table('tr_department_terkait')
                                                        ->where('kodeCMF', $kodeCMF)
                                                        ->where('kodeDepartmentCMF', $kodeDepartment)
                                                        ->count();
                                        // cek apakah departemen yang dipilih sudah di insert ke tr_department_terkait
                                        if($cek_dept == 0){
                                            // KODE DEPARTMENT TERKAIT
                                            $kodeDepartmentTerkaitCMF = DB::table('tr_department_terkait')
                                                            ->max('kodeDepartmentTerkaitCMF');
                                            $noUrut = (int) substr($kodeDepartmentTerkaitCMF, 7, 8);
                                            $noUrut++;
                                            $char = "DPRTCMF";
                                            $kodeDepartmentTerkaitCMF = $char . sprintf("%08s", $noUrut);
                                            $updateArray6 = array(
                                                "kodeDepartmentTerkaitCMF" => "$kodeDepartmentTerkaitCMF",
                                                "kodeCMF" => "$kodeCMF",
                                                "adjustmentDepartment1CMF" => 0,
                                                "adjustmentDepartment2CMF" => 0,
                                                "kodeDepartmentCMF" => "$kodeDepartment",
                                                "txtInsertedBy" => $request->session()->get('cmf_namaKaryawan'),
                                                "dtmInsertedDate" => date('Y-m-d H:i:s'),
                                              );
                                            $insert1 = DB::table('tr_department_terkait')->insert($updateArray6);
                                            if($insert1 == 0){
                                                return redirect($request->feedBack)->withFail('Proses Insert Department Terkait Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                                            }
                                        }
                                    }else{
                                        $cek_dept = DB::table('tr_department_terkait')
                                                        ->where('kodeCMF', $kodeCMF)
                                                        ->where('kodeDepartmentCMF', $kodeDepartment)
                                                        ->count();
                                        // cek apakah departemen yang dipilih sudah di insert ke tr_department_terkait
                                        if($cek_dept > 0){
                                            $cek_area = DB::table('tr_area_terkait')
                                                        ->where('kodeCMF', $kodeCMF)
                                                        ->where('kodeDepartmentCMF', $kodeDepartment)
                                                        ->where('valueAreaCMF', '1')
                                                        ->count();
                                            if($cek_area == 0){
                                                $delete = DB::table('tr_department_terkait')
                                                            ->where('kodeCMF', $kodeCMF)
                                                            ->where('kodeDepartmentCMF', $kodeDepartment)
                                                            ->delete();
                                                if($delete == 0){
                                                    return redirect($request->feedBack)->withFail('Proses Delete Department Terkait Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                                                }
                                            }
                                        }
                                    }
                                }else{
                                    return redirect($request->feedBack)->withFail('Proses Update Area Terkait Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                                }
                            }
                            return redirect($request->feedBack)->withSuccess('CMF Berhasil Diupdate !');
                        }else{
                            return redirect($request->feedBack)->withFail('Proses Update Risk Assesment Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan ! !');
                        }
                    }else{
                        return redirect($request->feedBack)->withFail('Proses Update Jenis Perubahan Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                    }
                }else{
                    return redirect($request->feedBack)->withFail('Proses Update Perubahan Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                }
            }else{
                return redirect($request->feedBack)->withFail('Proses Update CMF Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
            }
        }else{
            return redirect('AllertSession');
        }
    }

    public function UpdateCMFAfter(Request $request){
        if($request->session()->get('cmf_nik')){
            $kodeCMF = $request->kodeCMF;
            $detailAktifitasCMF = $request->detailAktifitasCMF;
            $dateCMF = $request->dateCMF;
            $departmentCMF = $request->departmentCMF;
            $feedBack = $request->feedBack;
            // UPLOAD DOKUMEN
            $txtUploadCommissioningReject = $request->txtUploadCommissioningReject;
            $namaFile = $_FILES ['txtUploadCommissioning']['name'];
            $ukuranFile = $_FILES['txtUploadCommissioning']['size'];
            $error = $_FILES['txtUploadCommissioning']['error'];
            $tmpName =$_FILES['txtUploadCommissioning']['tmp_name'];

            if($error === 4){
                $namaFileBaru = $txtUploadCommissioningReject;
            }else{
                // cek pastikan yg di upload adalah gambar
                $ekstensiDocumentValid = ['pdf'];
                $ekstensiDocument = explode('.', $namaFile);
                $ekstensiDocument= strtolower(end($ekstensiDocument));

                if(!in_array($ekstensiDocument, $ekstensiDocumentValid)){
                    return redirect($request->feedBack)->withFail('Dokumen Yang Diupload Bukan PDF !');
                }

                $uniq = $kodeCMF;
                $namaFileBaru = $uniq.'.'.$ekstensiDocument;
                $tahunCMF = date('Y', strtotime($dateCMF));
                $dpt = DB::table('m_department')
                    ->where('kodeDepartmentMaster', $departmentCMF)
                    ->first();
                $department = $dpt->namaDepartment;
                $pathTahun = 'upload/commissioning/'.$tahunCMF;
                $pathDepartment = 'upload/commissioning/'.$tahunCMF.'/'.$department;
                $pathNamaFileBaru = 'upload/commissioning/'.$tahunCMF.'/'.$department.'/'.$namaFileBaru;
                $documentReject = 'upload/commissioning/'.$tahunCMF.'/'.$department.'/'.$txtUploadCommissioningReject;

                if(is_dir($pathTahun)){
                    if(is_dir($pathDepartment)){
                        move_uploaded_file($tmpName, $pathNamaFileBaru);
                        if(!file_exists($pathNamaFileBaru)){
                            return redirect($request->feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                    }
                    }else{
                        mkdir($pathDepartment);
                        $file = fopen($pathDepartment."/"."index.html", "w");
                        echo fwrite($file,"Ngeyel Banget dah hahaha !");
                        fclose($file);
                        move_uploaded_file($tmpName, $pathNamaFileBaru);
                        if(!file_exists($pathNamaFileBaru)){
                            return redirect($request->feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                        }
                    }
                }else{
                    $file = fopen("upload/commissioning/"."index.html", "w");
                    echo fwrite($file,"Dilarang Masuk Bro !");
                    fclose($file);
                    mkdir($pathTahun);
                    $file = fopen($pathTahun."/"."index.html", "w");
                    echo fwrite($file,"Dibilangin Ga Boleh Masuk !");
                    fclose($file);
                    mkdir($pathDepartment);
                    $file = fopen($pathDepartment."/"."index.html", "w");
                    echo fwrite($file,"Ngeyel Banget dah hahaha !");
                    fclose($file);
                    move_uploaded_file($tmpName, $pathNamaFileBaru);
                    if(!file_exists($pathNamaFileBaru)){
                        return redirect($request->feedBack)->withFail('Dokumen Gagal Disimpan di Directory System ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                    }
                }
            }
            // UPDATE TABLE TR_CMF
            $updateArray1 = array(
                "detailAktifitasCMF" => $detailAktifitasCMF,
                "txtUploadCommissioning" => $namaFileBaru,
                "approveCMF6" => $request->session()->get('cmf_namaKaryawan'),
                "dateApproveCMF6" => date('Y-m-d H:i:s'),
                "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                "dtmUpdatedDate" => date('Y-m-d H:i:s'),
            );
            $update1 = DB::table('tr_cmf')
                                ->where('kodeCMF', $kodeCMF)
                                ->update($updateArray1);
            if($update1 > 0){
                return redirect($feedBack)->withSuccess('Perubahan CMF Berhasil Disimpan !');
            }else{
                return redirect($feedBack)->withFail('Proses Update CMF Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
            }
        }else{
            return redirect('AllertSession');
        }
    }

    public function Penolakan(Request$request){
        if($request->session()->get('cmf_nik')){
            // DATA POST
            $encryKodeCMF = $request->encryKodeCMF;
            $statusProses = $request->statusProses;
            $comentNOKCMF = $request->comentNOKCMF;
            $feedBack = $request->feedBack;
            // KOREKSI DARI DEPTHEAD PEMILIK PROSES || KOREKSI DARI DC || DIKOREKSI DARI DEPT HEAD TERKAIT || KOREKSI DARI SPV SYSTEM || KOREKSI DARI MR || KOREKSI DARI MNF || KOREKSI DC AFTER REVIEW || KOREKSI DETAIL AKTIFITAS BY DEPT HEAD || KOREKSI EVALUASI BY DEPT HEAD TERKAIT || KOREKSI BY VERIFIKATOR || KOREKSI BY DC || KOREKSI BY MR
            if($statusProses == -1 || $statusProses == -2 || $statusProses == -4 || $statusProses == -5 || $statusProses == -6 || $statusProses == -7 || $statusProses == -8 || $statusProses == -9 || $statusProses == -10 || $statusProses == -11 || $statusProses == -12 || $statusProses == -13){
                // UPDATE TABLE TR_CMF
                $updateArray = array(
                    "statusProsesCMF" => "$statusProses",
                    "picNOKCMF" => $request->session()->get('cmf_namaKaryawan'),
                    "comentNOKCMF" => "$comentNOKCMF",
                    "dateNOKCMF" => date('Y-m-d H:i:s'),
                    "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                    "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                );
                $update = DB::table('tr_cmf')
                                ->where('encryKodeCMF', $encryKodeCMF)
                                ->update($updateArray);
                if($update > 0){
                    return redirect($feedBack)->withSuccess('CMF Berhasil Diupdate !');
                }else{
                    return redirect($feedBack)->withFail('Proses Update CMF Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                }
            // CMF DITOLAK DEPT HEAD TERKAIT
            }elseif($statusProses == -3){
                $dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKodeCMF)
						->first();
                $kodeCMF = $dt->kodeCMF;
                $updateArray = array(
                    "statusProsesCMF" => "6",
                    "picNOKCMF" => $request->session()->get('cmf_namaKaryawan'),
                    "comentNOKCMF" => "$comentNOKCMF",
                    "dateNOKCMF" => date('Y-m-d H:i:s'),
                    "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                    "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                );
                $update = DB::table('tr_cmf')
                            ->where('encryKodeCMF', $encryKodeCMF)
                            ->update($updateArray);
                if($update > 0){
                    $updateArray2 = array(
                                "reviewDepartmentCMF" => "$comentNOKCMF",
                                "adjustmentDepartment1CMF" => -1,
                                "approvedBy1" => $request->session()->get('cmf_namaKaryawan'),
                                "dateReviewCMF" => date('Y-m-d H:i:s'), 
                                "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                                "dtmUpdatedDate" => date('Y-m-d H:i:s'),
                    );
                    $kodeDepartmentCMF = $request->session()->get('cmf_department');
                    $update2 = DB::table('tr_department_terkait')
                            ->where('kodeCMF', $kodeCMF)
                            ->where('kodeDepartmentCMF', $kodeDepartmentCMF)
                            ->update($updateArray2);
                    if($update2 > 0){
                        return redirect($feedBack)->withSuccess('CMF Berhasil Ditolak ! CMF Terkirim Ke Document Control');
                    }else{
                        return redirect($feedBack)->withFail('Proses Update Departemen Terkait Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                    }
                }else{
                    return redirect($feedBack)->withFail('Proses Update CMF Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
                }
            }
        }else{
            return redirect('AllertSession');
        }
    }

    public function Batalkan(Request $request){
        if($request->session()->get('cmf_levelCMF')){
            $encryKodeCMF = $request->encryKodeCMF;
            $statusProses = $request->statusProses;
      
            $updateArray = array(
                "statusProsesCMF" => "$statusProses",
                "txtUpdatedBy" => $request->session()->get('cmf_namaKaryawan'),
                "dtmUpdatedDate" => date('Y-m-d H:i:s'),
            );
            $update = DB::table('tr_cmf')
                        ->where('encryKodeCMF', $encryKodeCMF)
                        ->update($updateArray);
            if($update > 0){
                return redirect($request->feedBack)->withSuccess('CMF Berhasil Dibatalkan !');
            }else{
                return redirect($request->feedBack)->withFail('Proses Update CMF Gagal ! Segera Hubungi Admin Document Controller (ext : 812) untuk melaporkan permasalahan !');
            }
        }else{
            return redirect('AllertSession');
        }
    }

    
}
