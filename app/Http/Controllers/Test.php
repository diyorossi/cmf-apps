<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class Test extends Controller
{
    // public function index(){
    //     $data = DB::table('test')->get();

    //     return response()->json($data);
    // }

    // public function store(Request $request){
    //     DB::table('test')->insert([
    //         'keterangan'=>$request->keterangan
    //     ]);
    //     return response()->json([
    //         'status'=>true,
    //         'message'=>'success'
    //     ]);
    // }

    public function update(Request $request,$id){
        DB::table('test')->where('id',$id)->update([
            'keterangan'=>$request->keterangan
        ]);
        return response()->json([
            'status'=>true,
            'message'=>'update success'
        ]);
    }

    // public function delete($id){
    //     DB::table('test')->where('id',$id)->delete();
    //     return response()->json([
    //         'status'=>true,
    //         'message'=>'delete success'
    //     ]);
    // }

    // public function filter(Request $request){
    //     $keyword = $request->keyword;
    //     $data = DB::table('test')->where('keterangan','like','%'.$keyword.'%')->get();
    //     return response()->json([
    //         'status'=>true,
    //         'message'=>'success',
    //         'data_filter'=>$data
    //     ]);
    // }

    public function sendtest(Request $request,$id){
        echo 'data';
        // $data = array(
        //     'id' => 2,
        //     'keterangan' => 'WH-CAPEX-2024-025',
        // );
        // $response = Http::post('http://192.168.252.248/kmibda_fui/kontak/index_post', $data);
        // return response()->json([
        //             'status'=>true,
        //             'message'=>'delete success'
        //         ]);
    }
}