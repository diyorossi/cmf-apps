<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Depthead extends Controller
{
    public function Home(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL02'){
				// DEKLARASI
				$statusProsesDashboard = array(1, 2, 1.5, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12);
				$department = $request->session()->get('cmf_department');
				// NOTIF
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "active";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Dashboard";
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['role'] = 'Depthead';
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['nik'] = $data_user->nik;
				$data['kodeUser'] = $data_user->kodeUser;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->where('departmentCMF', $department)
						->whereIn('statusProsesCMF', $statusProsesDashboard)
						->orderBy('tr_cmf.dateCMF', 'DESC')
						->get();
				$data['member'] = DB::table('m_user')
						->where('bitActive', 1)
						->where('department', $data_user->department)
						->get();
                return view('Pic.home', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function DeptheadApprovalCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL02'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				// NOTIF
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "active";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | CMF Diterima";
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['role'] = 'Depthead';
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->where('departmentCMF', $department)
						->where('statusProsesCMF', 1)
						->orderBy('tr_cmf.dtmInsertedDate', 'DESC')
						->get();
				return view('Depthead.cmf_approval', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function OpenCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL02'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "active";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | CMF Diterima";
				$role = 'Pic';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
				// DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['role'] = 'Depthead';
                return view('Depthead.cmf_open', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function DeptheadReviewCMF(Request $request){
        if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL02'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				// NOTIF
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "active";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | CMF Review";
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['role'] = 'Depthead';
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
						->where('kodeDepartmentCMF', $department)
						->where('statusProsesCMF', 2)
						->where('adjustmentDepartment1CMF', 0)
						->orderBy('tr_cmf.dtmInsertedDate', 'DESC')
						->get();
				return view('Depthead.cmf_review', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
    }

	public function ReviewCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL02'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "active";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Open CMF Page";
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['role'] = 'Depthead';
                return view('Depthead.cmf_review_open', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function AfterChangeCMF(Request $request){
        if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL02'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				// NOTIF
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "active";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | CMF DITERIMA";
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['role'] = 'Depthead';
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->where('departmentCMF', $department)
						->where('statusProsesCMF', 8)
						->orderBy('tr_cmf.dtmInsertedDate', 'DESC')
						->get();
				return view('Depthead.cmf_after_change', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
    }

	public function OpenAfterCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL02'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2);
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "active";
				$data['active5'] = "";
				$data['active6'] = "";
				$data['active7'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | CMF DITERIMA";
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['role'] = 'Depthead';
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
				// DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['role'] = 'Depthead';
                return view('Depthead.cmf_open_after', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function DeptheadEvaluasiCMF(Request $request){
        if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL02'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				// NOTIF
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "active";
				$data['active6'] = "";
				$data['active7'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | CMF Evaluasi";
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['role'] = 'Depthead';
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
						->where('kodeDepartmentCMF', $department)
						->where('statusProsesCMF', 9)
						->where('adjustmentDepartment2CMF', 0)
						->orderBy('tr_cmf.dtmInsertedDate', 'DESC')
						->get();
				return view('Depthead.cmf_evaluasi', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
    }

	public function EvaluasiCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL02'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2);
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "active";
				$data['active6'] = "";
				$data['active7'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | CMF Evaluasi";
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['role'] = 'Depthead';
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
				// DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['evaluasi'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->orderBy('dtmUpdatedDate', 'ASC')
											->get();
				$data['evaluasi_count'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->count();
				$data['role'] = 'Depthead';
                return view('Depthead.cmf_evaluasi_open', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function RiwayatCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL02'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				// NOTIF
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "active";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Riwayat CMF";
				$data['role'] = 'Depthead';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				// DATABASE
				$statusRiwayat = array(13, 14);
				$data['cmf'] = DB::table('tr_cmf')
						->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department', 'tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->where('departmentCMF', $department)
						->whereIn('statusProsesCMF', $statusRiwayat)
						->orderBy('tr_cmf.dtmInsertedDate', 'DESC')
						->get();
				return view('Pic.cmf_riwayat', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}

	public function OpenRiwayatCMF(Request $request){
		if($request->session()->get('cmf_nik')){
			if($request->session()->get('cmf_levelCMF') == 'LVL02'){
				// DEKLARASI
				$department = $request->session()->get('cmf_department');
				$statusProses = array(-1, -2, -4, -5, -6, -7, -8);
				$statusProsesAfter = array(-9, -10, -11, -12, -13);
				$area = $request->session()->get('cmf_area');
				$data['feedBack'] = $request->e;
				$data['back'] = $request->f;
				$encryKode = $request->d;
				// NOTIF
				$data['notif1'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 1)
								->count();
				$data['notif2'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 2)
								->where('adjustmentDepartment1CMF', 0)
								->count();
				$data['notif8'] = DB::table('tr_cmf')
								->where('departmentCMF', $department)
								->where('statusProsesCMF', 8)
								->count();
				$data['notif9'] = DB::table('tr_cmf')
								->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
								->where('kodeDepartmentCMF', $department)
								->where('statusProsesCMF', 9)
								->where('adjustmentDepartment2CMF', 0)
								->count();
				// ACTIVE
				$data['active1'] = "";
				$data['active2'] = "";
				$data['active3'] = "";
				$data['active4'] = "";
				$data['active5'] = "";
				$data['active6'] = "active";
				$data['active7'] = "";
				$data['active8'] = "";
				// DATA LOGIN
				$data['title'] = "CMF ONLINE | Open CMF Page";
				$data['role'] = 'Depthead';
				$nik = $request->session()->get('cmf_nik');
				$data_user = DB::table('m_user')
						->join('m_level', 'm_user.levelCMF', '=', 'm_level.kodeLevel')
						->join('m_department', 'm_user.department', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait', 'm_user.area', '=', 'm_area_terkait.kodeAreaTerkait')
						->where('nik', $nik)->first();
				$data['gambarUser'] = $data_user->gambarUser;
				$data['namaKaryawan'] = $data_user->namaKaryawan;
				$data['namaLevel'] = $data_user->namaLevel;
				$data['posisi'] = $data_user->posisi;
				$data['namaDepartment'] = $data_user->namaDepartment;
				$data['namaAreaTerkait'] = $data_user->namaAreaTerkait;
				// DATABASE
				$dt = DB::table('tr_cmf')//DATA GET CMF
						->join('m_user','tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
						->join('m_department','tr_cmf.departmentCMF', '=', 'm_department.kodeDepartmentMaster')
						->join('m_area_terkait','tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
						->join('tr_perubahan','tr_cmf.kodeCMF', '=', 'tr_perubahan.kodeCMF')
						->join('tr_jenis_perubahan','tr_cmf.kodeCMF', '=', 'tr_jenis_perubahan.kodeCMF')
						->join('tr_risk_assesment','tr_cmf.kodeCMF', '=', 'tr_risk_assesment.kodeCMF')
						->join('tr_area_terkait','tr_cmf.kodeCMF', '=', 'tr_area_terkait.kodeCMF')
						->where('encryKodeCMF', $encryKode)
						->first();
				$kodeCMF = $dt->kodeCMF;
				$data['dt'] = $dt;
				$data['review'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->orderBy('dtmUpdatedDate', 'DESC')
											->get();
				$data['review_count'] = DB::table('tr_department_terkait')//DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment1CMF', '!=', '0')
											->count();
				$data['area_terkait'] = DB::table('tr_area_terkait')//DATA AREA TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('namaAreaCMF', 'ASC')
											->get();
				$data['perubahan'] = DB::table('m_perubahan')//DATA PERUBAHAN
											->where('bitActivePerubahan', '1')
											->get();
				$data['jenisPerubahan'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->get();
				$data['jenisPerubahanLainnya'] = DB::table('tr_jenis_perubahan') //DATA JENIS PERUBAHAN
											->where('kodeCMF', $kodeCMF)
											->where('namaJenisPerubahanCMF', 'Lainnya')
											->first();
				$data['evaluasi'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->orderBy('dtmUpdatedDate', 'ASC')
											->get();
				$data['evaluasi_count'] = DB::table('tr_department_terkait') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->where('adjustmentDepartment2CMF', '!=', 0)
											->count();
				$data['dokumen'] = DB::table('tr_dokumen') //DATA DEPARTMENT TERKAIT
											->where('kodeCMF', $kodeCMF)
											->orderBy('postDokumenCMF', 'ASC')
											->get();
                return view('Pic.cmf_open_riwayat', $data);
			}else{
				return redirect('AllertSession');
			}   
		}else{
			return redirect('AllertSession');
		}
	}
}
