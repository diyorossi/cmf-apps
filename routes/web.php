<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth;
use App\Http\Controllers\Dc;
use App\Http\Controllers\Depthead;
use App\Http\Controllers\Manufacturing;
use App\Http\Controllers\Mr;
use App\Http\Controllers\Pic;
use App\Http\Controllers\Verifikator;
use App\Http\Controllers\Query;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [Auth::class, 'index'])->name('index');
Route::get('Auth/Login', [Auth::class, 'index'])->name('Auth/Login');
Route::get('/logout', [Auth::class, 'logout'])->name('logout');
Route::get('/AllertSession', [Auth::class, 'AllertSession'])->name('AllertSession');
Route::post('/actionlogin', [Auth::class, 'actionlogin'])->name('actionlogin');
Route::get('Auth/DashboardCMF', [Auth::class, 'DashboardCMF'])->name('Auth/DashboardCMF');

// Route Document Control
Route::get('Dc/home', [Dc::class, 'Home'])->name('Dc/home');
Route::get('Dc/OpenEditCMF', [Dc::class, 'OpenEditCMF'])->name('Dc/OpenEditCMF');
Route::get('Dc/DcPengajuanCMF', [Dc::class, 'DcPengajuanCMF'])->name('Dc/DcPengajuanCMF');
Route::get('Dc/OpenPengajuanCMF', [Dc::class, 'OpenPengajuanCMF'])->name('Dc/OpenPengajuanCMF');
Route::get('Dc/DcApprovalCMF', [Dc::class, 'DcApprovalCMF'])->name('Dc/DcApprovalCMF');
Route::get('Dc/OpenCMF', [Dc::class, 'OpenCMF'])->name('Dc/OpenCMF');
Route::get('Dc/VerifikasiDocumentCMF', [Dc::class, 'VerifikasiDocumentCMF'])->name('Dc/VerifikasiDocumentCMF');
Route::get('Dc/OpenVerifikasiCMF', [Dc::class, 'OpenVerifikasiCMF'])->name('Dc/OpenVerifikasiCMF');
Route::get('Dc/ListCMF', [Dc::class, 'ListCMF'])->name('Dc/ListCMF');
Route::get('Dc/CreateCMF', [Dc::class, 'CreateCMF'])->name('Dc/CreateCMF');
Route::get('Dc/OpenCMF', [Dc::class, 'OpenCMF'])->name('Dc/OpenCMF');
Route::get('Dc/EditCreateCMF', [Dc::class, 'EditCreateCMF'])->name('Dc/EditCreateCMF');
Route::get('Dc/PenolakanCMF', [Dc::class, 'PenolakanCMF'])->name('Dc/PenolakanCMF');
Route::get('Dc/PenolakanCMFAfter', [Dc::class, 'PenolakanCMFAfter'])->name('Dc/PenolakanCMFAfter');
Route::get('Dc/EditReviewedCMF', [Dc::class, 'EditReviewedCMF'])->name('Dc/EditReviewedCMF');
Route::get('Dc/AfterChangeCMF', [Dc::class, 'AfterChangeCMF'])->name('Dc/AfterChangeCMF');
Route::get('Dc/OpenAfterCMF', [Dc::class, 'OpenAfterCMF'])->name('Dc/OpenAfterCMF');
Route::get('Dc/RiwayatCMF', [Dc::class, 'RiwayatCMF'])->name('Dc/RiwayatCMF');
Route::get('Dc/OpenRiwayatCMF', [Dc::class, 'OpenRiwayatCMF'])->name('Dc/OpenRiwayatCMF');
Route::get('Dc/SemuaCMF', [Dc::class, 'SemuaCMF'])->name('Dc/SemuaCMF');
Route::get('Dc/SemuaCMFTahun', [Dc::class, 'SemuaCMFTahun'])->name('Dc/SemuaCMFTahun');
Route::get('Dc/OpenRiwayatCMFTahun', [Dc::class, 'OpenRiwayatCMFTahun'])->name('Dc/OpenRiwayatCMFTahun');
Route::get('Dc/SemuaCMFAllYear', [Dc::class, 'SemuaCMFAllYear'])->name('Dc/SemuaCMFAllYear');
Route::get('Dc/User', [Dc::class, 'User'])->name('Dc/User');
Route::get('Dc/AddUser', [Dc::class, 'AddUser'])->name('Dc/AddUser');
Route::get('Dc/UserEdit', [Dc::class, 'UserEdit'])->name('Dc/UserEdit');
Route::get('Dc/JenisPerubahan', [Dc::class, 'JenisPerubahan'])->name('Dc/JenisPerubahan');
Route::get('Dc/JenisPerubahanEdit', [Dc::class, 'JenisPerubahanEdit'])->name('Dc/JenisPerubahanEdit');
Route::get('Dc/JenisPerubahanAdd', [Dc::class, 'JenisPerubahanAdd'])->name('Dc/JenisPerubahanAdd');
Route::get('Dc/Dokumen', [Dc::class, 'Dokumen'])->name('Dc/Dokumen');
Route::get('Dc/DokumenAdd', [Dc::class, 'DokumenAdd'])->name('Dc/DokumenAdd');
Route::get('Dc/DokumenEdit', [Dc::class, 'DokumenEdit'])->name('Dc/DokumenEdit');
Route::get('Dc/Department', [Dc::class, 'Department'])->name('Dc/Department');
Route::get('Dc/DepartmentAdd', [Dc::class, 'DepartmentAdd'])->name('Dc/DepartmentAdd');
Route::get('Dc/DepartmentEdit', [Dc::class, 'DepartmentEdit'])->name('Dc/DepartmentEdit');
Route::get('Dc/AreaTerkait', [Dc::class, 'AreaTerkait'])->name('Dc/AreaTerkait');
Route::get('Dc/AreaTerkaitAdd', [Dc::class, 'AreaTerkaitAdd'])->name('Dc/AreaTerkaitAdd');
Route::get('Dc/AreaTerkaitEdit', [Dc::class, 'AreaTerkaitEdit'])->name('Dc/AreaTerkaitEdit');
Route::get('Dc/ExportExcel', [Dc::class, 'ExportExcel'])->name('Dc/ExportExcel');

// Route Dept Head
Route::get('Depthead/home', [Depthead::class, 'Home'])->name('Depthead/home');
Route::get('Depthead/DeptheadApprovalCMF', [Depthead::class, 'DeptheadApprovalCMF'])->name('Depthead/DeptheadApprovalCMF');
Route::get('Depthead/OpenCMF', [Depthead::class, 'OpenCMF'])->name('Depthead/OpenCMF');
Route::get('Depthead/DeptheadReviewCMF', [Depthead::class, 'DeptheadReviewCMF'])->name('Depthead/DeptheadReviewCMF');
Route::get('Depthead/ReviewCMF', [Depthead::class, 'ReviewCMF'])->name('Depthead/ReviewCMF');
Route::get('Depthead/AfterChangeCMF', [Depthead::class, 'AfterChangeCMF'])->name('Depthead/AfterChangeCMF');
Route::get('Depthead/OpenAfterCMF', [Depthead::class, 'OpenAfterCMF'])->name('Depthead/OpenAfterCMF');
Route::get('Depthead/DeptheadEvaluasiCMF', [Depthead::class, 'DeptheadEvaluasiCMF'])->name('Depthead/DeptheadEvaluasiCMF');
Route::get('Depthead/EvaluasiCMF', [Depthead::class, 'EvaluasiCMF'])->name('Depthead/EvaluasiCMF');
Route::get('Depthead/RiwayatCMF', [Depthead::class, 'RiwayatCMF'])->name('Depthead/RiwayatCMF');
Route::get('Depthead/OpenRiwayatCMF', [Depthead::class, 'OpenRiwayatCMF'])->name('Depthead/OpenRiwayatCMF');

// Manufacturing
Route::get('Manufacturing/home', [Manufacturing::class, 'Home'])->name('Manufacturing/home');
Route::get('Manufacturing/MnfApprovalCMF', [Manufacturing::class, 'MnfApprovalCMF'])->name('Manufacturing/MnfApprovalCMF');
Route::get('Manufacturing/OpenCMF', [Manufacturing::class, 'OpenCMF'])->name('Manufacturing/OpenCMF');
Route::get('Manufacturing/DeptheadApprovalCMF', [Manufacturing::class, 'DeptheadApprovalCMF'])->name('Manufacturing/DeptheadApprovalCMF');
Route::get('Manufacturing/SemuaCMF', [Manufacturing::class, 'SemuaCMF'])->name('Manufacturing/SemuaCMF');
Route::get('Manufacturing/SemuaCMFTahun', [Manufacturing::class, 'SemuaCMFTahun'])->name('Manufacturing/SemuaCMFTahun');
Route::get('Manufacturing/OpenRiwayatCMFTahun', [Manufacturing::class, 'OpenRiwayatCMFTahun'])->name('Manufacturing/OpenRiwayatCMFTahun');
Route::get('Manufacturing/SemuaCMFAllYear', [Manufacturing::class, 'SemuaCMFAllYear'])->name('Manufacturing/SemuaCMFAllYear');

// Verifikator
Route::get('Verifikator/home', [Verifikator::class, 'Home'])->name('Verifikator/home');
Route::get('Verifikator/SystemApprovalCMF', [Verifikator::class, 'SystemApprovalCMF'])->name('Verifikator/SystemApprovalCMF');
Route::get('Verifikator/OpenCMF', [Verifikator::class, 'OpenCMF'])->name('Verifikator/OpenCMF');
Route::get('Verifikator/DeptheadApprovalCMF', [Depthead::class, 'DeptheadApprovalCMF'])->name('Verifikator/DeptheadApprovalCMF');
Route::get('Verifikator/VerifikatorApprovalCMF', [Verifikator::class, 'VerifikatorApprovalCMF'])->name('Verifikator/VerifikatorApprovalCMF');
Route::get('Verifikator/OpenVerifikasiCMF', [Verifikator::class, 'OpenVerifikasiCMF'])->name('Verifikator/OpenVerifikasiCMF');
Route::get('Verifikator/SemuaCMF', [Verifikator::class, 'SemuaCMF'])->name('Verifikator/SemuaCMF');
Route::get('Verifikator/SemuaCMFTahun', [Verifikator::class, 'SemuaCMFTahun'])->name('Verifikator/SemuaCMFTahun');
Route::get('Verifikator/OpenRiwayatCMFTahun', [Verifikator::class, 'OpenRiwayatCMFTahun'])->name('Verifikator/OpenRiwayatCMFTahun');
Route::get('Verifikator/SemuaCMFAllYear', [Verifikator::class, 'SemuaCMFAllYear'])->name('Verifikator/SemuaCMFAllYear');

// Pic
Route::get('Pic/home', [Pic::class, 'Home'])->name('Pic/home');
Route::get('Pic/ListCMF', [Pic::class, 'ListCMF'])->name('Pic/ListCMF');
Route::get('Pic/CreateCMF', [Pic::class, 'CreateCMF'])->name('Pic/CreateCMF');
Route::get('Pic/OpenCMF', [Pic::class, 'OpenCMF'])->name('Pic/OpenCMF');
Route::get('Pic/EditCreateCMF', [Pic::class, 'EditCreateCMF'])->name('Pic/EditCreateCMF');
Route::get('Pic/PenolakanCMF', [Pic::class, 'PenolakanCMF'])->name('Pic/PenolakanCMF');
Route::get('Pic/PenolakanCMFAfter', [Pic::class, 'PenolakanCMFAfter'])->name('Pic/PenolakanCMFAfter');
Route::get('Pic/EditReviewedCMF', [Pic::class, 'EditReviewedCMF'])->name('Pic/EditReviewedCMF');
Route::get('Pic/AfterChangeCMF', [Pic::class, 'AfterChangeCMF'])->name('Pic/AfterChangeCMF');
Route::get('Pic/OpenAfterCMF', [Pic::class, 'OpenAfterCMF'])->name('Pic/OpenAfterCMF');
Route::get('Pic/RiwayatCMF', [Pic::class, 'RiwayatCMF'])->name('Pic/RiwayatCMF');
Route::get('Pic/OpenRiwayatCMF', [Pic::class, 'OpenRiwayatCMF'])->name('Pic/OpenRiwayatCMF');


// Mr
Route::get('Mr/home', [Mr::class, 'Home'])->name('Mr/home');
Route::get('Mr/MrApprovalCMF', [Mr::class, 'MrApprovalCMF'])->name('Mr/MrApprovalCMF');
Route::get('Mr/MrApprovalCMF', [Mr::class, 'MrApprovalCMF'])->name('Mr/MrApprovalCMF');
Route::get('Mr/MrOpenCMF', [Mr::class, 'MrOpenCMF'])->name('Mr/MrOpenCMF');
Route::get('Mr/SystemApprovalCMF', [Mr::class, 'SystemApprovalCMF'])->name('Mr/SystemApprovalCMF');
Route::get('Mr/DeptheadApprovalCMF', [Mr::class, 'DeptheadApprovalCMF'])->name('Mr/DeptheadApprovalCMF');
Route::get('Mr/MrVerifikasiCMF', [Mr::class, 'MrVerifikasiCMF'])->name('Mr/MrVerifikasiCMF');
Route::get('Mr/OpenVerifikasiCMF', [Mr::class, 'OpenVerifikasiCMF'])->name('Mr/OpenVerifikasiCMF');
Route::get('Mr/DeptheadApprovalCMF', [Mr::class, 'DeptheadApprovalCMF'])->name('Mr/DeptheadApprovalCMF');
Route::get('Mr/OpenCMF', [Mr::class, 'OpenCMF'])->name('Mr/OpenCMF');
Route::get('Mr/DeptheadReviewCMF', [Mr::class, 'DeptheadReviewCMF'])->name('Mr/DeptheadReviewCMF');
Route::get('Mr/ReviewCMF', [Mr::class, 'ReviewCMF'])->name('Mr/ReviewCMF');
Route::get('Mr/AfterChangeCMF', [Mr::class, 'AfterChangeCMF'])->name('Mr/AfterChangeCMF');
Route::get('Mr/OpenAfterCMF', [Mr::class, 'OpenAfterCMF'])->name('Mr/OpenAfterCMF');
Route::get('Mr/DeptheadEvaluasiCMF', [Mr::class, 'DeptheadEvaluasiCMF'])->name('Mr/DeptheadEvaluasiCMF');
Route::get('Mr/EvaluasiCMF', [Mr::class, 'EvaluasiCMF'])->name('Mr/EvaluasiCMF');
Route::get('Mr/SemuaCMF', [Mr::class, 'SemuaCMF'])->name('Mr/SemuaCMF');
Route::get('Mr/SemuaCMFTahun', [Mr::class, 'SemuaCMFTahun'])->name('Mr/SemuaCMFTahun');
Route::get('Mr/OpenRiwayatCMFTahun', [Mr::class, 'OpenRiwayatCMFTahun'])->name('Mr/OpenRiwayatCMFTahun');
Route::get('Mr/SemuaCMFAllYear', [Mr::class, 'SemuaCMFAllYear'])->name('Mr/SemuaCMFAllYear');

// Query
Route::post('/Query/Approve', [Query::class, 'Approve'])->name('Query/Approve');
Route::post('/Query/insertCMF', [Query::class, 'insertCMF'])->name('Query/insertCMF');
Route::post('/Query/UpdateCMF', [Query::class, 'UpdateCMF'])->name('Query/UpdateCMF');
Route::post('/Query/UpdateCMFAfter', [Query::class, 'UpdateCMFAfter'])->name('Query/UpdateCMFAfter');
Route::post('/Query/Penolakan', [Query::class, 'Penolakan'])->name('Query/Penolakan');
Route::post('/Query/Batalkan', [Query::class, 'Batalkan'])->name('Query/Batalkan');
Route::post('/Query/VerifikasiDokumen', [Query::class, 'VerifikasiDokumen'])->name('Query/VerifikasiDokumen');
Route::post('/Query/insertUser', [Query::class, 'insertUser'])->name('Query/insertUser');
Route::post('/Query/updateUser', [Query::class, 'updateUser'])->name('Query/updateUser');
Route::post('/Query/UpdatePassword', [Query::class, 'UpdatePassword'])->name('Query/UpdatePassword');
Route::post('/Query/insertAreaTerkait', [Query::class, 'insertAreaTerkait'])->name('Query/insertAreaTerkait');
Route::post('/Query/updateAreaTerkait', [Query::class, 'updateAreaTerkait'])->name('Query/updateAreaTerkait');
Route::post('/Query/insertDepartment', [Query::class, 'insertDepartment'])->name('Query/insertDepartment');
Route::post('/Query/updateDepartment', [Query::class, 'updateDepartment'])->name('Query/updateDepartment');
Route::post('/Query/insertDokumen', [Query::class, 'insertDokumen'])->name('Query/insertDokumen');
Route::post('/Query/updateDokumen', [Query::class, 'updateDokumen'])->name('Query/updateDokumen');
Route::post('/Query/insertJenisPerubahan', [Query::class, 'insertJenisPerubahan'])->name('Query/insertJenisPerubahan');
Route::post('/Query/updateJenisPerubahan', [Query::class, 'updateJenisPerubahan'])->name('Query/updateJenisPerubahan');
Route::post('/Query/EditCMF', [Query::class, 'EditCMF'])->name('Query/EditCMF');

