@include('Auth.header')
@include($role.'/navigation')
@include('Auth.topBar')

      <!-- Content wrapper -->
      <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
          <div class="row" >
            <div class="col-md" >
              <div
                id="carouselExample-cf"
                class="carousel carousel-dark slide carousel-fade"
                data-bs-ride="carousel"
              >
                <ol class="carousel-indicators">
                  <li data-bs-target="#carouselExample-cf" data-bs-slide-to="0" class="active"></li>
                  <li data-bs-target="#carouselExample-cf" data-bs-slide-to="1"></li>
                  <li data-bs-target="#carouselExample-cf" data-bs-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img class="d-block w-100" style="height: 630px;" src="<?php echo asset('img') ?>/gallery/slide1.PNG" alt="First slide" />
                  </div>
                  <div class="carousel-item">
                    <img class="d-block w-100" style="height: 630px;" src="<?php echo asset('img') ?>/gallery/slide2.PNG" alt="Second slide" />
                    <div class="carousel-caption d-none d-md-block">
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block w-100" style="height: 630px;" src="<?php echo asset('img') ?>/gallery/slide3.PNG" alt="Third slide" />
                    <div class="carousel-caption d-none d-md-block">
                    </div>
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExample-cf" role="button" data-bs-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExample-cf" role="button" data-bs-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Next</span>
                </a>
              </div>
            </div>
          </div><br>
          <div class="row">
            <div class="col-md">
              <div class="card accordion-item active">
                <h2 class="accordion-header" id="headingTwo">
                  <button
                    type="button"
                    class="accordion-button collapsed"
                    data-bs-toggle="collapse"
                    data-bs-target="#accordionOne"
                    aria-expanded="false"
                    aria-controls="accordionTwo"
                    style="font-size: 18px;"
                  >
                    Member Department {{ $namaDepartment }}
                  </button>
                </h2>
                <div
                  id="accordionOne"
                  class="accordion-collapse collapse show"
                  aria-labelledby="headingTwo"
                  data-bs-parent="#accordionExample"
                >
                  <div class="accordion-body">
                    <hr class="my-0" /><br>
                    <div class="row">
                      @foreach ($member as $m)
                      <div class="col-md-2" style="margin-left: auto; margin-right: auto; display: block; text-align: center;">
                        <img
                          src="<?php echo asset('img') ?>/profile/<?php echo $m->gambarUser ?>"
                          alt="user-avatar"
                          class="d-block rounded-circle"
                          height="150"
                          width="150"
                          id="uploadedAvatar"
                          style="margin-left: auto; margin-right: auto; display: block; text-align: center;"
                        />
                        <label for="lastName" style="font-size: 15px; margin-top: 10px;" class="form-label">{{ $m->namaKaryawan }}</label>
                      </div>
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div><br>
          @if(Session::has('fail'))
            <div class="alert alert-danger">
                {{Session::get('fail')}}
            </div>
          @elseif(Session::has('success'))
            <div class="alert alert-success">
                {{Session::get('success')}}
            </div>
          @endif
          <div class="row">
            <div class="col-md">
              <div class="card accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                  <button
                    type="button"
                    class="accordion-button collapsed"
                    data-bs-toggle="collapse"
                    data-bs-target="#accordionTwo"
                    aria-expanded="false"
                    aria-controls="accordionTwo"
                    style="font-size: 18px;"
                  >
                    Ganti Foto & Password
                  </button>
                </h2>
                <div
                  id="accordionTwo"
                  class="accordion-collapse collapse"
                  aria-labelledby="headingTwo"
                  data-bs-parent="#accordionExample"
                >
                  <div class="accordion-body">
                    <hr class="my-0" /><br>
                    <form method="POST" enctype="multipart/form-data" action="{{ route('Query/UpdatePassword') }}">
                      @csrf
                      <div class="row">
                        <div class="col-md-5">
                          <div class="d-flex align-items-start align-items-sm-center gap-4">
                            <img
                              src="<?php echo asset('img') ?>/profile/<?php echo $gambarUser ?>"
                              alt="user-avatar"
                              class="d-block rounded"
                              height="100"
                              width="100"
                              id="uploadedAvatar"
                            />
                            <div class="button-wrapper">
                              <label for="upload" class="btn btn-primary me-2 mb-4" tabindex="0">
                                <span class="d-none d-sm-block">Upload new photo</span>
                                <i class="bx bx-upload d-block d-sm-none"></i>
                                <input
                                  type="file"
                                  id="upload"
                                  class="account-file-input"
                                  hidden
                                  name="gambarUser"
                                  accept="image/png, image/jpeg"
                                />
                              </label>
                              <button type="button" class="btn btn-outline-secondary account-image-reset mb-4">
                                <i class="bx bx-reset d-block d-sm-none"></i>
                                <span class="d-none d-sm-block">Reset</span>
                              </button>
        
                              <p class="text-muted mb-0">Allowed JPG, GIF or PNG. Max size of 800K</p>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <label for="lastName" class="form-label">Nama Karyawan</label>
                          <input class="form-control" type="text" disabled name="lastName" id="lastName" value="{{ $namaKaryawan }}" />
                        </div>
                        <div class="col-md-3">
                          <label for="lastName" class="form-label">Password Baru</label>
                          <input class="form-control" type="text"  name="password" id="lastName"  />
                        </div>
                        <div class="mt-2">
                          <button type="submit" style="float: right" class="btn btn-primary me-2">Save changes</button>
                        </div>
                        <input type="hidden" name="feedBack" value="Dc/home">
                      <input type="hidden" name="gambarUserReject" value="<?php echo $gambarUser; ?>">
                      <input type="hidden" name="kodeUser" value="<?php echo $kodeUser; ?>">
                      <input type="hidden" name="nik" value="<?php echo $nik; ?>">
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div><br>
          <div class="row">
            <div class="col-md">
              <div class="card">
                <div class="tab-content">
                  <table id="example" class="display" style="width:100%">
                    <thead class="text-nowrap">
                      <tr>
                        <th style="font-weight: bold; font-size: 13px; text-align: center;">#</th>
                        <th style="font-weight: bold; font-size: 13px; text-align: center;">NOMOR CMF</th>
                        <th style="font-weight: bold; font-size: 13px; text-align: center;">JUDUL<br>PERUBAHAN</th>
                        <th style="font-weight: bold; font-size: 13px; text-align: center;">ALASAN PERUBAHAN</th>
                        <th style="font-weight: bold; font-size: 13px; text-align: center;">STATUS<br>PROSES</th>
                        <th style="font-weight: bold; font-size: 13px; text-align: center;">ACTION</th>
                      </tr>
                    </thead>
                    <tbody>
                      @php
                        $no = 1;
                      @endphp
                      @foreach ($cmf as $dt)
                      <tr>
                        <td style="text-align: center;">{{ $no++ }}</td>
                        <td style="text-align: center;">{{ $dt->nomorCMF }}</td>
                        <td>{{ $dt->judulPerubahanCMF }}</td>
                        <td>{{ $dt->alasanPerubahanCMF }}</td>
                        @if ($dt->statusProsesCMF == 1)
                        <td style="text-align: center;">02. Tunggu Approve Dept Head Pemilik Proses</td>
                        @elseif($dt->statusProsesCMF == 1.5)
                          <td style="text-align: center;">02. CMF Sudah Diapprove Dept Head Pemilik Proses, Tunggu Pengecekan DC</td>
                          @elseif($dt->statusProsesCMF == 2)
                          <td style="text-align: center;">03. Tunggu Review Dari Dept Head Terkait</td>
                        @elseif($dt->statusProsesCMF == 3)
                          <td style="text-align: center;">04. CMF Sudah Disetujui Oleh Dept Head Terkait, Tunggu Approve Spv. System</td>
                        @elseif($dt->statusProsesCMF == 4)
                          <td style="text-align: center;">04. Tunggu Approve MR</td>
                        @elseif($dt->statusProsesCMF == 5)
                          <td style="text-align: center;">04. Tunggu Approve Manufacturing Head</td>
                        @elseif($dt->statusProsesCMF == 6)
                          <td style="text-align: center;">05. Proses Review Selesai, Tunggu Perubahan Selesai</td>
                        @elseif($dt->statusProsesCMF == 7)
                          <td style="text-align: center;">06. Pengisian Detail Aktifitas & Upload Dokumen Commissioning Oleh Pemilik Proses</td>
                        @elseif($dt->statusProsesCMF == 8)
                          <td style="text-align: center;">07. Tunggu Apporve Dept Head Pemilik Proses untuk Dievaluasi</td>
                        @elseif($dt->statusProsesCMF == 9)
                          <td style="text-align: center;">08. Tunggu Evaluasi Dari Dept Head Terkait</td>
                        @elseif($dt->statusProsesCMF == 10)
                          <td style="text-align: center;">09. CMF Sudah Dievaluasi Oleh Dept Head Terkait, Tunggu Verifikasi Food Safety</td>
                        @elseif($dt->statusProsesCMF == 11)
                          <td style="text-align: center;">09. Tunggu Verifikasi Document</td>
                        @elseif($dt->statusProsesCMF == 12)
                          <td style="text-align: center;">09. Tunggu Approve MR untuk Closing CMF</td>
                        @elseif($dt->statusProsesCMF == -1)
                          <td style="text-align: center;">01. Dikoreksi Dept Head Pemilik Proses</td>
                          @elseif($dt->statusProsesCMF == -2)
                          <td style="text-align: center;">01. Dikoreksi Document Control</td>
                          @elseif($dt->statusProsesCMF == -3)
                          <td style="text-align: center;">05. CMF Tidak Disetuin Dept Head Area Terkait</td>
                          @elseif($dt->statusProsesCMF == -4)
                          <td style="text-align: center;">01. Dikoreksi Dept Head Area Terkait</td>
                          @elseif($dt->statusProsesCMF == -5)
                          <td style="text-align: center;">01. Dikoreksi Spv. System</td>
                          @elseif($dt->statusProsesCMF == -6)
                          <td style="text-align: center;">01. Dikoreksi MR</td>
                          @elseif($dt->statusProsesCMF == -7)
                          <td style="text-align: center;">01. Dikoreksi Manufacturing Head</td>
                          @elseif($dt->statusProsesCMF == -8)
                          <td style="text-align: center;">01. Dikoreksi Document Control Setelah Review</td>
                          @elseif($dt->statusProsesCMF == -9)
                          <td style="text-align: center;">06. Evaluasi Dari Dept Head Pemilik Proses</td>
                          @elseif($dt->statusProsesCMF == -10)
                          <td style="text-align: center;">06. Evaluasi Dari Dept Head Area Terkait</td>
                          @elseif($dt->statusProsesCMF == -11)
                          <td style="text-align: center;">06. Evaluasi Dari Verifikator Food Safety</td>
                          @elseif($dt->statusProsesCMF == -12)
                          <td style="text-align: center;">06. Eveluasi Dari Document Control</td>
                          @elseif($dt->statusProsesCMF == -13)
                          <td style="text-align: center;">06. Evaluasi Dari MR Sebelum CMF Close</td>
                        @endif
                        <td style="text-align: center;">
                          <div class="row">
                            <div class="col-md-3">
                                <a href="<?php echo route($role.'/OpenVerifikasiCMF'); ?>?d=<?php echo $dt->encryKodeCMF; ?>&&e=<?php echo $role; ?>/home&&f=Dashboard" data-bs-toggle="tooltip" data-bs-offset="0,4" data-bs-placement="top" data-bs-html="true" title="" data-bs-original-title="<i class='bx bx-log-in-circle bx-xs' ></i> <span>Open CMF</span>" class="btn rounded-pill btn-icon btn-primary">
                                  <span class="tf-icons bx bx-log-in-circle"></span>
                              </a>
                            </div>
                            <div class="col-md-3">
                              <?php 
                                if($dt->txtUploadDocument !== ""){ 
                                  $tahun = date('Y', strtotime($dt->dateCMF));
                                  $department = $dt->namaDepartment;
                              ?>
                                <a href="<?php echo asset('upload').'/documents/'.$tahun.'/'.$department.'/'.$dt->txtUploadDocument; ?>" target="_blank" data-bs-toggle="tooltip" data-bs-offset="0,4" data-bs-placement="top" data-bs-html="true" title="" data-bs-original-title="<i class='bx bx-bookmarks bx-xs' ></i> <span>Buka Lampiran</span>" class="btn rounded-pill btn-icon btn-danger" class="btn rounded-pill btn-icon btn-danger">
                                  <span class="tf-icons bx bx-bookmarks"></span>
                                </a>
                              <?php } ?>
                            </div>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!--/ Transactions -->
          </div>
        </div>
        <!-- / Content -->

        <!-- Footer -->
        @include('Auth/footer')
        <!-- / Footer -->

        <div class="content-backdrop fade"></div>
      </div>
      <!-- Content wrapper -->
    </div>
    <!-- / Layout page -->
  </div>

  <!-- Overlay -->
  <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->

<div class="buy-now">
  <!-- <a
    href="https://themeselection.com/products/sneat-bootstrap-html-admin-template/"
    target="_blank"
    class="btn btn-danger btn-buy-now"
    >Upgrade to Pro</a
  > -->
</div>

<!-- DATA TABLES ONLINE -->
<script src="https://code.jquery.com/jquery-3.7.0.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>

<!-- build:js assets/vendor/js/core.js -->
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/jquery/jquery.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/popper/popper.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/js/bootstrap.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/js/menu.js"></script>
<!-- endbuild -->

<!-- Page JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/pages-account-settings-account.js"></script>

<!-- Vendors JS -->
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/apex-charts/apexcharts.js"></script>

<!-- Main JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/main.js"></script>

<!-- Page JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/dashboards-analytics.js"></script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<script>new DataTable('#example');</script>
</body>
</html>
