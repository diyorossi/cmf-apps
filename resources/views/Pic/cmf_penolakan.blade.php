@include('Auth.header')
@include($role.'/navigation')
@include('Auth.topBar')

      <!-- Content wrapper -->
      <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
          <div class="row">
            <!-- KONTEN -->

            <div class="container-xxl flex-grow-1 container-p-y">
              <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Dashboard /</span> Dikoreksi (Sebelum Perubahan) 
              </h4>
              @if(Session::has('fail'))
              <div class="alert alert-danger">
                  {{Session::get('fail')}}
              </div>
              @elseif(Session::has('success'))
              <div class="alert alert-success">
                  {{Session::get('success')}}
              </div>
              @endif
              <div class="card">
                <h5 class="card-header">LIST PENGAJUAN CHANGE MANAGEMENT</h5>
                <div class="tab-content">
                  <table id="example" class="display" style="width:100%">
                    <thead class="text-nowrap">
                      <tr>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">#</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">NOMOR CMF</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">JUDUL<br>PERUBAHAN</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">PENGOREKSI</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">KOMENTAR<br>KOREKSI</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">TANGGAL<br>KOREKSI\</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">ACTION</th>
                      </tr>
                    </thead>
                    <tbody>
                      @php
                        $no = 1;
                      @endphp
                      @foreach ($cmf as $dt)
                      <tr>
                        <td style="text-align: center;">{{ $no++ }}</td>
                        <td style="text-align: center;">{{ $dt->nomorCMF }}</td>
                        <td>{{ $dt->judulPerubahanCMF }}</td>
                        <td style="text-align: center;">{{ $dt->picNOKCMF }}</td>
                        <td>{{ $dt->comentNOKCMF }}</td>
                        <td style="text-align: center;"><?php echo date('d M Y', strtotime($dt->dateNOKCMF)); ?></td>
                        <td style="text-align: center;">
                          <div class="row">
                            <div class="col-md-3">
                              <a href="<?php echo route($role.'/EditCreateCMF'); ?>?d=<?php echo $dt->encryKodeCMF; ?>&&e=<?php echo $role; ?>/PenolakanCMF&&f=Dikoreksi (Sebelum Perubahan)" data-bs-toggle="tooltip" data-bs-offset="0,4" data-bs-placement="top" data-bs-html="true" title="" data-bs-original-title="<i class='bx bx-pen bx-xs' ></i> <span>Edit CMF</span>" class="btn rounded-pill btn-icon btn-warning">
                                  <span class="tf-icons bx bx-pen"></span>
                              </a>
                            </div>
                            <div class="col-md-3">
                              <a href="#" data-bs-toggle="modal" data-bs-target="#modalToggle" data-bs-offset="0,4" data-bs-placement="top" onclick="detail('<?php echo $dt->encryKodeCMF; ?>')" data-bs-html="true" title="" data-bs-original-title="<i class='bx bx-send bx-xs' ></i> <span>Kirim CMF</span>" class="btn rounded-pill btn-icon btn-success">
                                  <span class="tf-icons bx bx-send"></span>
                              </a>
                            </div>
                            <div class="col-md-3">
                              <a href="#" data-bs-toggle="modal" data-bs-target="#modalToggle2" data-bs-offset="0,4" data-bs-placement="top" onclick="detail2('<?php echo $dt->encryKodeCMF; ?>')" data-bs-html="true" title="" data-bs-original-title="<i class='bx bx-send bx-xs' ></i> <span>Batalkan CMF</span>" class="btn rounded-pill btn-icon btn-secondary">
                                  <span class="tf-icons bx bx-x"></span>
                              </a>
                            </div>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          <!-- /KONTEN -->
          <div
            class="modal fade"
            id="modalToggle"
            aria-labelledby="modalToggleLabel"
            tabindex="-1"
            style="display: none"
            aria-hidden="true"
          >
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                <form action="<?php echo route('Query/Approve') ?>" method="POST">
                @csrf
                <div class="modal-header">
                  <h5 class="modal-title" id="modalToggleLabel"><b><span class="tf-icons bx bx-mail-send"></span> Konfirmasi Pengajuan</b></h5>
                  <button
                    type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  ></button>
                </div>
                <div class="modal-body">Pengajuan CMF Anda akan dikirim ke Dept Head. Cek kembali CMF yang akan diajukan. Apabila sudah sesuai , klik tombol <b>kirim</b> !</div>
                <input type="hidden" name="encryKodeCMF" id="dis-data">
                <input type="hidden" name="statusProses" value="1">
                <input type="hidden" name="feedBack" value="{{ route($role.'/PenolakanCMF') }}">
                <div class="modal-footer">
                  <button
                    class="btn btn-primary"
                    data-bs-target="#modalToggle3"
                    data-bs-toggle="modal"
                    data-bs-dismiss="modal"
                  >
                    Kirim
                  </button>
                </div>
                </form>
              </div>
            </div>
          </div>
          <div
            class="modal fade"
            id="modalToggle2"
            aria-labelledby="modalToggleLabel"
            tabindex="-1"
            style="display: none"
            aria-hidden="true"
          >
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                <form action="<?php echo route('Query/Batalkan') ?>" method="POST">
                @csrf
                <div class="modal-header">
                  <h5 class="modal-title" id="modalToggleLabel"><b><span class="tf-icons bx bx-x"></span> Batalkan Pengajuan CMF ?</b></h5>
                  <button
                    type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  ></button>
                </div>
                <div class="modal-body">Pengajuan CMF Anda tidak akan dilanjutkan ke step selanjutnya, namun riwayat CMF nya akan tetap tersimpan di system. Klik tombol <b>Batalkan CMF</b> jika Anda yakin !</div>
                <input type="hidden" name="encryKodeCMF" id="dis-data2">
                <input type="hidden" name="statusProses" value="23">
                <input type="hidden" name="feedBack" value="{{ route($role.'/PenolakanCMF') }}">
                <div class="modal-footer">
                  <button
                    class="btn btn-primary"
                    data-bs-target="#modalToggle2"
                    data-bs-toggle="modal"
                    data-bs-dismiss="modal"
                  >
                    Batalkan CMF
                  </button>
                </div>
                </form>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>

        @include('Auth/footer')
        <!-- / Footer -->
    </div>
    <!-- / Layout page -->
  </div>

  <!-- Overlay -->
  <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->


<!-- DATA TABLES ONLINE -->
<script src="https://code.jquery.com/jquery-3.7.0.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>

<script src="<?php echo asset('drf/'); ?>SB Admin/datatables-demo.js"></script>

<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/jquery/jquery.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/popper/popper.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/js/bootstrap.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/js/menu.js"></script>
<!-- endbuild -->

<!-- Vendors JS -->
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/apex-charts/apexcharts.js"></script>

<!-- Main JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/main.js"></script>

<!-- Page JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/dashboards-analytics.js"></script>
<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>

<script type="text/javascript">
  function detail(kode){
    var data = document.getElementById('dis-data');
    data.setAttribute('value', kode);
  }

  function detail2(kode){
    var data = document.getElementById('dis-data2');
    data.setAttribute('value', kode);
  }
</script>
<script>new DataTable('#example');</script>
</body>
</html>
