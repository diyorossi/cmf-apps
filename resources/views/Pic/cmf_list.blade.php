@include('Auth.header')
@include($role.'/navigation')
@include('Auth.topBar')

      <!-- Content wrapper -->
      <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
          <div class="row">
            <!-- KONTEN -->

            <div class="container-xxl flex-grow-1 container-p-y">
              <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Dashboard /</span> Buat CMF 
                <a href="{{ route($role.'/CreateCMF') }}" class="btn rounded-pill btn-primary" style="float: right;">
                  <span class="bx bx-plus me-sm-1"></span>&nbsp; Tambah
                </a>
              </h4>
              @if(Session::has('fail'))
                <div class="alert alert-danger">
                    {{Session::get('fail')}}
                </div>
              @elseif(Session::has('success'))
                <div class="alert alert-success">
                    {{Session::get('success')}}
                </div>
              @endif
              <div class="card">
                <h5 class="card-header">LIST PENGAJUAN CHANGE MANAGEMENT</h5>
                <div class="tab-content">
                  <table id="example" class="display" style="width:100%">
                    <thead class="text-nowrap">
                      <tr>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">#</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">NOMOR CMF</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">JUDUL PERUBAHAN</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">TARGET IMPLEMENTASI</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">STATUS CMF</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">ACTION</th>
                      </tr>
                    </thead>
                    <tbody>
                      @php
                        $no = 1;
                      @endphp
                      @foreach ($cmf as $dt)
                      <tr>
                        <td style="text-align: center;">{{ $no++ }}</td>
                        <td style="text-align: center;">{{ $dt->nomorCMF }}</td>
                        <td>{{ $dt->judulPerubahanCMF }}</td>
                        <td style="text-align: center;">{{ date('d M Y', strtotime($dt->dateImplementasiCMF)) }}</td>
                        <td style="text-align: center;">
                          @if ($dt->statusProsesCMF == 0)
                            <span class="badge rounded-pill bg-warning">Tunggu Kirim</span>
                          @elseif($dt->statusProsesCMF == 1)
                            <button
                              type="button"
                              class="btn btn-sm btn-primary text-nowrap"
                              data-bs-toggle="popover"
                              data-bs-offset="0,14"
                              data-bs-placement="left"
                              data-bs-html="true"
                              data-bs-content="<p style='margin-top: -20px;'><span class='badge rounded bg-warning' style='font-size: 10px;'>Tunggu Approve Dept Head.</span></p> <div class='d-flex justify-content-between'></div>"
                              title="<span class='badge rounded bg-primary' style='text-weight: bold; margin-top: 20px;'>Status Approval :</span>"
                            >
                              Approval
                            </button>
                          @elseif($dt->statusProsesCMF == 2)
                            <button
                              type="button"
                              class="btn btn-sm btn-success text-nowrap"
                              data-bs-toggle="popover"
                              data-bs-offset="0,14"
                              data-bs-placement="left"
                              data-bs-html="true"
                              data-bs-content="<p style='margin-top: -20px;'><span class='badge rounded bg-warning' style='font-size: 10px;'>Tunggu Approve Dept Head.</span></p> <div class='d-flex justify-content-between'></div>"
                              title="<span class='badge rounded bg-success' style='text-weight: bold; margin-top: 20px;'>Status Review :</span>"
                            >
                              Review
                            </button>
                          @elseif($dt->statusProsesCMF == 3)
                            <button
                              type="button"
                              class="btn btn-sm btn-primary text-nowrap"
                              data-bs-toggle="popover"
                              data-bs-offset="0,14"
                              data-bs-placement="left"
                              data-bs-html="true"
                              data-bs-content="<p style='margin-top: -20px;'><span class='badge rounded bg-warning' style='font-size: 10px;'>Tunggu Approve System.</span></p> <div class='d-flex justify-content-between'></div>"
                              title="<span class='badge rounded bg-primary' style='text-weight: bold; margin-top: 20px;'>Status Approval :</span>"
                            >
                              Approval
                            </button>
                          @endif
                        </td>
                        <td style="text-align: center;">
                          <div class="row">
                            <div class="col-md-3">
                              @if ($dt->statusProsesCMF == 0)
                                <a href="{{ route($role.'/EditCreateCMF') }}?d={{ $dt->encryKodeCMF }}&&e={{ $role }}/ListCMF&&f=Buat CMF" data-bs-toggle="tooltip" data-bs-offset="0,4" data-bs-placement="top" data-bs-html="true" title="" data-bs-original-title="<i class='bx bx-pen bx-xs' ></i> <span>Edit CMF</span>" class="btn rounded-pill btn-icon btn-warning">
                                  <span class="tf-icons bx bx-pen"></span>
                                </a>
                              @else
                                <a href="{{ route($role.'/OpenCMF') }}?d={{ $dt->encryKodeCMF }}" data-bs-toggle="tooltip" data-bs-offset="0,4" data-bs-placement="top" data-bs-html="true" title="" data-bs-original-title="<i class='bx bx-log-in-circle bx-xs' ></i> <span>Open CMF</span>" class="btn rounded-pill btn-icon btn-primary">
                                  <span class="tf-icons bx bx-log-in-circle"></span>
                                </a>
                              @endif
                            </div>
                            @if ($dt->statusProsesCMF == 0)
                              <div class="col-md-3" style="margin-left: 10px;">
                                <a href="#" data-bs-toggle="modal" data-bs-target="#modalToggle" data-bs-offset="0,4" data-bs-placement="top" onclick="detail('{{ $dt->encryKodeCMF }}')" data-bs-html="true" title="" data-bs-original-title="<i class='bx bx-send bx-xs' ></i> <span>Kirim CMF</span>" class="btn rounded-pill btn-icon btn-success">
                                    <span class="tf-icons bx bx-send"></span>
                                </a>
                              </div>
                            @endif
                            <div class="col-md-3" style="margin-left: 10px;">
                              @if ($dt->txtUploadDocument !== '')
                              @php
                                  $tahun = date('Y', strtotime($dt->dateCMF));
                                  $department = $dt->namaDepartment;
                              @endphp
                                  <a href="<?php echo asset('upload/documents').'/'.$tahun.'/'.$department.'/'.$dt->txtUploadDocument; ?>" target="_blank" data-bs-toggle="tooltip" data-bs-offset="0,4" data-bs-placement="top" data-bs-html="true" title="" data-bs-original-title="<i class='bx bx-bookmarks bx-xs' ></i> <span>Buka Lampiran</span>" class="btn rounded-pill btn-icon btn-danger" class="btn rounded-pill btn-icon btn-danger">
                                    <span class="tf-icons bx bx-bookmarks"></span>
                                  </a>
                              @endif
                            </div>
                          </div>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          <!-- /KONTEN -->
          <div
            class="modal fade"
            id="modalToggle"
            aria-labelledby="modalToggleLabel"
            tabindex="-1"
            style="display: none"
            aria-hidden="true"
          >
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                <form action="{{ route('Query/Approve') }}" method="POST">
                @csrf
                <div class="modal-header">
                  <h5 class="modal-title" id="modalToggleLabel"><b><span class="tf-icons bx bx-mail-send"></span> Konfirmasi Pengajuan</b></h5>
                  <button
                    type="button"
                    class="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  ></button>
                </div>
                <div class="modal-body">Pengajuan CMF Anda akan dikirim ke Dept Head. Cek kembali CMF yang akan diajukan. Apabila sudah sesuai , klik tombol <b>kirim</b> !</div>
                <input type="hidden" name="encryKodeCMF" id="dis-data">
                <input type="hidden" name="statusProses" value="1">
                <input type="hidden" name="feedBack" value="{{ route($role.'/ListCMF') }}">
                <div class="modal-footer">
                  <button
                    class="btn btn-primary"
                    data-bs-target="#modalToggle2"
                    data-bs-toggle="modal"
                    data-bs-dismiss="modal"
                  >
                    Kirim
                  </button>
                </div>
                </form>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>

        @include('Auth/footer')
        <!-- / Footer -->
    </div>
    <!-- / Layout page -->
  </div>

  <!-- Overlay -->
  <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->

<!-- DATA TABLES ONLINE -->
<script src="https://code.jquery.com/jquery-3.7.0.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/jquery/jquery.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/popper/popper.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/js/bootstrap.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/js/menu.js"></script>
<!-- endbuild -->

<!-- Vendors JS -->
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/apex-charts/apexcharts.js"></script>

<!-- Main JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/main.js"></script>

<!-- Page JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/dashboards-analytics.js"></script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<script type="text/javascript">
  function detail(kode){
    var data = document.getElementById('dis-data');
    data.setAttribute('value', kode);
  }

  function detail2(kode){
    var data = document.getElementById('dis-data2');
    data.setAttribute('value', kode);
  }
</script>

<script>new DataTable('#example');</script>
</body>
</html>
