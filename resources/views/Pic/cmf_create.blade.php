@include('Auth.header')
@include($role.'/navigation')
@include('Auth.topBar')

      <!-- Content wrapper -->
      <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
          <div class="row">
            <!-- KONTEN -->
            <!-- Pills -->
              <div class="container-xxl flex-grow-1 container-p-y">
              <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Dashboard / <a href="{{ route($role.'/ListCMF') }}" class="text-muted fw-light">Buat CMF</a> /</span> Tambah
              </h4>
              <div class="row">
                <div class="nav-align-top mb-4">
                    <div class="tab-content">
                      <div class="tab-pane fade show active" id="navs-pills-justified-home" role="tabpanel">
                      <form method="POST" action="{{ route('Query/insertCMF') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                          <div class="row">
                            <h4 class="card-header" style="text-align: left; margin-top: -30px;">Data CMF</h4><hr>
                            <div class="mb-3 col-md-2">
                              <label for="firstName" class="form-label">Department</label>
                              <input class="form-control" type="text" disabled value="<?php echo $namaDepartment; ?>" />
                            </div>
                            <div class="mb-3 col-md-2">
                              <label for="firstName" class="form-label">Area</label>
                              <input class="form-control" type="text" disabled value="<?php echo $namaAreaTerkait; ?>" />
                            </div>
                            <div class="mb-3 col-md-3">
                              <label for="lastName" class="form-label">Pemilik Proses</label>
                              <input class="form-control" type="text" disabled value="<?php echo $namaKaryawan; ?>" placeholder="Nama Lengkap Karyawan" />
                            </div>
                            <div class="mb-3 col-md-3">
                              <label for="lastName" class="form-label">Nomor Capex (Pengajuan FUI)</label>
                              <input class="form-control" type="text" name="nomorCapex" placeholder="Nomor Capex" />
                            </div>
                            <div class="mb-3 col-md-2">
                              <label for="lastName" class="form-label">Tanggal Pengajuan <small style="color: red;">*)</small></label>
                              <input class="form-control" type="date" name="dateCMF" id="dateCMF" required="required" placeholder="Tanggal Pengajuan" />
                            </div>
                            <div class="mb-3 col-md-12">
                              <label for="organization" class="form-label">Judul Perubahan <small style="color: red;">*)</small></label>
                              <input type="text" class="form-control" name="judulPerubahanCMF" placeholder="Judul Perubahan" required="required" />
                            </div>
                            <div class="mb-3 col-md-12">
                              <label class="form-label" for="country">Perubahan <small style="color: red;">*)</small></label>
                              <div class="table-responsive">
                                <table class="table table-striped table-borderless ">
                                  <tbody>
                                    <td>
                                      <input class="form-check-input" name="bitMesin" type="checkbox" value="1" />
                                      <label class="form-check-label" for="defaultCheck3"> Mesin </label>
                                    </td>
                                    <td>
                                      <input class="form-check-input" name="bitProses" type="checkbox" value="1" />
                                      <label class="form-check-label" for="defaultCheck3"> Proses </label>
                                    </td>
                                    <td>
                                      <input class="form-check-input" name="bitSystem" type="checkbox" value="1" />
                                      <label class="form-check-label" for="defaultCheck3"> System </label>
                                    </td>
                                    <td>
                                      <input class="form-check-input" type="checkbox" name="bitLainnya" value="1" id="lainnyaPerubahan" onclick="lainnya()" />
                                      <label class="form-check-label" for="defaultCheck3"> Lainnya </label>
                                    </td>
                                    <td>
                                      <input class="form-control" type="hidden" id="valuePerubahanCMF" placeholder="Lainnya ...." name="valueLainnya" />
                                    </td>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                            <div class="mb-3 col-md-12">
                              <label class="form-label" for="country">Jenis Perubahan <small style="color: red;">*)</small></label>
                              <div class="table-responsive">
                                <table class="table table-striped table-borderless ">
                                  <tbody>
                                    <tr>
                                      @php
                                          $no = 0;
                                      @endphp
                                      @foreach ($jenisPerubahan as $jns)
                                      
                                      @if ($no == 4 || $no == 8 || $no == 12 || $no == 16 || $no == 20)
                                    </tr>
                                    <tr>
                                      @endif
                                      <td>
                                        <input class="form-check-input" type="checkbox" name="{{ $jns->kodeJenisPerubahan  }}" value="1" />
                                        <label class="form-check-label" for="defaultCheck3"> {{ $jns->namaJenisPerubahan }} </label>
                                      </td>
                                      @php
                                          $no++
                                      @endphp
                                      @endforeach
                                      <td>
                                        <input class="form-check-input" type="checkbox" id="lainnyaJenisPerubahan" name="bitLainnya2" value="1" onclick="lainnya2()" />
                                        <label class="form-check-label" for="defaultCheck3"> Lainnya </label>
                                      </td>
                                      <td>
                                        <input class="form-control" type="hidden" id="valueJenisPerubahanCMF" placeholder="Lainnya ...." name="valueLainnya2" />
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="lastName" class="form-label">Target Implementasi <small style="color: red;">*)</small></label>
                              <input class="form-control" type="date" required="required" name="dateImplementasiCMF" />
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="lastName" class="form-label">Type Perubahan <small style="color: red;">*)</small></label>
                              <div class="table-responsive">
                                <table class="table table-striped table-borderless ">
                                  <tbody>
                                    <td>
                                      <div class="form-check">
                                        <input name="typePerubahanCMF" required class="form-check-input" type="radio" id="defaultRadio1" value="1">
                                        <label class="form-check-label" for="defaultRadio1"> Temporary </label>
                                      </div>
                                    </td>
                                    <td>
                                      <div class="form-check">
                                        <input name="typePerubahanCMF" required class="form-check-input" type="radio" id="defaultRadio2" value="2">
                                        <label class="form-check-label" for="defaultRadio2"> Permanent </label>
                                      </div>
                                    </td>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                            <div class="mb-3 col-md-12">
                              <label for="email" class="form-label">Alasan Perubahan <small style="color: red;">*)</small></label>
                              <textarea class="form-control" required="required" name="alasanPerubahanCMF" placeholder="Latar Belakang & Tujuan" ></textarea>
                            </div>
                            <div class="mb-3 col-md-12">
                              <label for="email" class="form-label">Dampak Terhadap Perubahan <small style="color: red;">*)</small></label>
                              <textarea class="form-control" required="required" name="dampakPerubahanCMF" placeholder="Quality, Biaya, Food Safety, Keselamatan & Kesehatan Lingkungan atau Regulasi" ></textarea>
                            </div>
                            <div class="mb-3 col-md-12">
                              <label for="email" class="form-label">Deskripsi Perubahan <small style="color: red;">*)</small></label>
                              <textarea class="form-control" required="required" name="deskripsiPerubahanCMF" placeholder="Keterangan Perubahan Produk" ></textarea>
                            </div>
                            <div class="mb-3 col-md-12">
                              <label for="email" class="form-label">Area Terkait <small style="color: red;">*)</small></label>
                            </div>
                            @foreach ($area as $dd)
                            <div class="mb-3 col-md-2">
                              <input class="form-check-input" type="checkbox" name="{{ $dd->kodeAreaTerkait }}" value="1" />
                              <label class="form-check-label" for="defaultCheck3"> {{ $dd->namaAreaTerkait }} </label>
                            </div>    
                            @endforeach
                            <h4 class="card-header" style="text-align: left;">Risk Assesment</h4><hr>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">Resiko <small style="color: red;">*)</small></label>
                              <textarea class="form-control" name="resikoCMF" required="required" placeholder="Resiko Dilakukannya Perubahan ..." ></textarea>
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">Rencana Mitigasi <small style="color: red;">*)</small></label>
                              <textarea class="form-control" name="mitigasiCMF" required="required" placeholder="Mitigasi Pencegahan dari Resiko ....." ></textarea>
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">PIC <small style="color: red;">*)</small></label>
                              <input type="text" name="picRiskAssesmentCMF" required="required" class="form-control" placeholder="PIC Mitigasi">
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">Dead Line <small style="color: red;">*)</small></label>
                              <input type="date" name="deadLineCMF" required="required" class="form-control" >
                            </div>
                            <h4 class="card-header" style="text-align: left;">Upload Dokumen</h4><hr>
                            <div class="mb-3 col-md-12">
                              <label for="email" class="form-label">Upload Dokumen  </label>
                              <input type="file" name="txtUploadDocument" class="form-control" accept=".pdf" >
                              <small style="color: red; "> *) Sistem hanya menerima dokumen dengan ekstensi .pdf</small>
                            </div>
                          </div>
                          <br><br>
                          <div class="mt-2">
                            <button type="submit" class="btn btn-lg btn-primary me-2" style="float: right;"><i class="menu-icon tf-icons bx bx-check"></i>Simpan</button>
                            <input type="hidden" name="feedBack" value="{{ route($role.'/ListCMF') }}">
                            <input type="hidden" name="txtUploadDocumentReject" value="">
                          </div>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Pills -->
            
            <!-- /KONTEN -->
          </div>
        </div>
      </div>

        @include('Auth/footer')
        <!-- / Footer -->
    </div>
    <!-- / Layout page -->
  </div>

  <!-- Overlay -->
  <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->


<!-- Core JS -->
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/jquery/jquery.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/popper/popper.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/js/bootstrap.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/js/menu.js"></script>
<!-- endbuild -->

<!-- Vendors JS -->
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/apex-charts/apexcharts.js"></script>

<!-- Main JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/main.js"></script>

<!-- Page JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/dashboards-analytics.js"></script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
</body>
</html>


<script>
  function lainnya(){
    var lainnyaPerubahan = document.getElementById('lainnyaPerubahan');
    var valuePerubahanCMF = document.getElementById('valuePerubahanCMF');
    if(lainnyaPerubahan.checked == true){
      valuePerubahanCMF.removeAttribute('type');
    }else{
      valuePerubahanCMF.setAttribute('type', 'hidden');
    }
  }
</script>

<script>
  function lainnya2(){
    var lainnyaJenisPerubahan = document.getElementById('lainnyaJenisPerubahan');
    var valueJenisPerubahanCMF = document.getElementById('valueJenisPerubahanCMF');
    if(lainnyaJenisPerubahan.checked == true){
      valueJenisPerubahanCMF.removeAttribute('type');
    }else{
      valueJenisPerubahanCMF.setAttribute('type', 'hidden');
    }
  }
</script>

<script>
  $('#btn_submit').click(function() {
        var klausul = $('#klausul').val();
        var tgl_kejadian = $('#tgl_kejadian_ncr').val();
        var uraian_kejadian = $('#uraian_kejadian_ncr').val();
        var jenis_masalah = $('#jenis_masalah').val();
        var gambar_ncr = $('#gambar_ncr').val();


  });
</script>
