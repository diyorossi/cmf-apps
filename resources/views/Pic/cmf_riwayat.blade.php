@include('Auth.header')
@include($role.'/navigation')
@include('Auth.topBar')

      <!-- Content wrapper -->
      <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
          <div class="row">
            <!-- KONTEN -->

            <div class="container-xxl flex-grow-1 container-p-y">
              <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Dashboard /</span> Riwayat CMF
              </h4>
              @if(Session::has('fail'))
                <div class="alert alert-danger">
                    {{Session::get('fail')}}
                </div>
              @elseif(Session::has('success'))
                <div class="alert alert-success">
                    {{Session::get('success')}}
                </div>
              @endif
              <div class="card">
                <h5 class="card-header">LIST CMF YANG SUDAH DISETUJUI MANAGEMENT</h5>
                <div class="tab-content">
                  <table id="example" class="display" style="width:100%">
                    <thead class="text-nowrap">
                      <tr>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">#</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">NOMOR CMF</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">JUDUL<br>PERUBAHAN</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">ALASAN<br>PERUBAHAN</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">DAMPAK<br>PERUBAHAN</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">DESKRIPSI<br>PERUBAHAN</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">STATUS</th>
                        <th style="font-weight: bold; font-size: 17px; text-align: center;">ACTION</th>
                      </tr>
                    </thead>
                    <tbody>
                      @php
                        $no = 1;
                      @endphp
                      @foreach ($cmf as $dt)
                      <tr>
                        <td style="text-align: center;">{{ $no++ }}</td>
                        <td style="text-align: center;">{{ $dt->nomorCMF }}</td>
                        <td>{{ $dt->judulPerubahanCMF }}</td>
                        <td>{{ $dt->alasanPerubahanCMF }}</td>
                        <td>{{ $dt->dampakPerubahanCMF }}</td>
                        <td>{{ $dt->deskripsiPerubahanCMF }}</td>
                        <td>
                          <a href="#" data-bs-toggle="tooltip" data-bs-offset="0,4" data-bs-placement="top" data-bs-html="true" title="<?php if($dt->statusProsesCMF ==  13){echo 'APPROVED';}elseif($dt->statusProsesCMF == 14){echo 'CANCELED';}else{echo 'ON PROGRESS';} ?>" data-bs-original-title="" class="btn rounded-pill btn-icon <?php if($dt->statusProsesCMF ==  13){echo 'btn-success  ';}elseif($dt->statusProsesCMF == 14){echo 'btn-danger';}else{echo 'btn-warning';} ?>" class="btn <?php if($dt->statusProsesCMF ==  13){echo 'btn-success  ';}elseif($dt->statusProsesCMF == 14){echo 'btn-danger';}else{echo 'btn-warning';} ?>" class="btn btn-icon btn-danger">
                            <?php if($dt->statusProsesCMF ==  13){echo '<span class="tf-icons bx bx-check"></span> ';}elseif($dt->statusProsesCMF == 14){echo '<span class="tf-icons bx bx-x"></span>';}else{echo '<span class="tf-icons bx bx-history"></span> PROSES REVIEW';} ?>
                        </a>
                        </td>
                        <td style="text-align: center;">
                          <div class="row">
                            <div class="col-md-3">
                                <a href="<?php echo route($role.'/OpenRiwayatCMF'); ?>?d=<?php echo $dt->encryKodeCMF; ?>&&e=<?php echo $role; ?>/RiwayatCMF&&f=Riwayat CMF" data-bs-toggle="tooltip" data-bs-offset="0,4" data-bs-placement="top" data-bs-html="true" title="" data-bs-original-title="<i class='bx bx-log-in-circle bx-xs' ></i> <span>Open CMF</span>" class="btn rounded-pill btn-icon btn-primary">
                                  <span class="tf-icons bx bx-log-in-circle"></span>
                              </a>
                            </div>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          <!-- /KONTEN -->
        </div>
      </div>
    </div>

      @include('Auth/footer')
        <!-- / Footer -->
    </div>
    <!-- / Layout page -->
  </div>

  <!-- Overlay -->
  <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->


<!--DATA TABLES ONLINE -->
<script src="https://code.jquery.com/jquery-3.7.0.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/jquery/jquery.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/popper/popper.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/js/bootstrap.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/js/menu.js"></script>
<!-- endbuild -->

<!-- Vendors JS -->
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/apex-charts/apexcharts.js"></script>

<!-- Main JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/main.js"></script>
<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>

<script>new DataTable('#example');</script>
</body>
</html>
