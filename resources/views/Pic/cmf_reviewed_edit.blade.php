@include('Auth.header')
@include($role.'/navigation')
@include('Auth.topBar')

      <!-- Content wrapper -->
      <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
          <div class="row">
            <!-- KONTEN -->
            <!-- Pills -->
            <div class="container-xxl flex-grow-1 container-p-y">
              <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Dashboard / <a href="<?php echo route($feedBack); ?>" class="text-muted fw-light"><?php echo $back; ?></a> /</span> Open CMF
              </h4>
              <div class="row">
                <div class="nav-align-top mb-4">
                    <div class="tab-content">
                      <div class="tab-pane fade show active" id="navs-pills-justified-home" role="tabpanel">
                        <div class="card-body">
                          <form method="POST" action="{{ route('Query/UpdateCMFAfter') }}" enctype="multipart/form-data">
                          @csrf
                          <div class="row">
                            <h4 class="card-header" style="text-align: left; margin-top: -30px;">Data CMF</h4><hr>
                            <div class="mb-3 col-md-2">
                              <label for="firstName" class="form-label">Department</label>
                              <input class="form-control" type="text" disabled value="<?php echo $dt->namaDepartment ?>" />
                            </div>
                            <div class="mb-3 col-md-2">
                              <label for="firstName" class="form-label">Area</label>
                              <input class="form-control" type="text" disabled value="<?php echo $dt->namaAreaTerkait ?>" />
                            </div>
                            <div class="mb-3 col-md-5">
                              <label for="lastName" class="form-label">Pemilik Proses</label>
                              <input class="form-control" type="text" disabled value="<?php echo $dt->namaKaryawan ?>" placeholder="Nama Lengkap Karyawan" />
                            </div>
                            <div class="mb-3 col-md-3">
                              <label for="lastName" class="form-label">Tanggal Pengajuan <small style="color: red;">*)</small></label>
                              <input class="form-control" type="date" disabled name="dateCMF" id="dateCMF" required="required" value="<?php echo $dt->dateCMF ?>" placeholder="Tanggal Pengajuan" />
                            </div>
                            <div class="mb-3 col-md-12">
                              <label for="organization" class="form-label">Judul Perubahan <small style="color: red;">*)</small></label>
                              <input type="text" class="form-control" disabled value="<?php echo $dt->judulPerubahanCMF ?>" name="judulPerubahanCMF" placeholder="Judul Perubahan" required="required" />
                            </div>
                            <div class="mb-3 col-md-12">
                              <label class="form-label" for="country">Perubahan <small style="color: red;">*)</small></label>
                              <div class="table-responsive">
                                <table class="table table-striped table-borderless ">
                                  <tbody>
                                    <td>
                                      <input class="form-check-input" name="bitMesin" <?php if($dt->bitMesin == 1){ print 'checked'; } ?> type="checkbox" value="1" />
                                      <label class="form-check-label" for="defaultCheck3"> Mesin </label>
                                    </td>
                                    <td>
                                      <input class="form-check-input" name="bitProses" <?php if($dt->bitProses == 1){ print 'checked'; } ?> type="checkbox" value="1" />
                                      <label class="form-check-label" for="defaultCheck3"> Proses </label>
                                    </td>
                                    <td>
                                      <input class="form-check-input" name="bitSystem" <?php if($dt->bitSystem == 1){ print 'checked'; } ?> type="checkbox" value="1" />
                                      <label class="form-check-label" for="defaultCheck3"> System </label>
                                    </td>
                                    <td>
                                      <input class="form-check-input" type="checkbox" <?php if($dt->bitLainnya !== ""){ print 'checked'; } ?> name="bitLainnya" value="1" id="lainnyaPerubahan" onclick="lainnya()" />
                                      <label class="form-check-label" for="defaultCheck3"> Lainnya </label>
                                    </td>
                                    <td>
                                      <input class="form-control" type="<?php if($dt->bitLainnya == ''){ print 'hidden'; }else{print 'text';} ?>" value="<?php echo $dt->bitLainnya ?>"  id="valuePerubahanCMF" placeholder="Lainnya ...." name="valueLainnya" />
                                    </td>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                            <div class="mb-3 col-md-12">
                              <label class="form-label" for="country">Jenis Perubahan <small style="color: red;">*)</small></label>
                              <div class="table-responsive">
                                <table class="table table-striped table-borderless ">
                                  <tbody>
                                    <tr>
                                      @php
                                          $no = 0;
                                      @endphp
                                      @foreach ($jenisPerubahan as $jns)
                                      
                                      @if ($no == 4 || $no == 8 || $no == 12 || $no == 16 || $no == 20)
                                      </tr>
                                      <tr>
                                      @endif
                                      <td>
                                          <input class="form-check-input" type="checkbox" @php if ($jns->valueJenisPerubahan == 1) {print 'checked';} @endphp name="@php if($jns->kodeJenisCMF == ''){print 'bitLainnya2';}else{echo $jns->kodeJenisCMF;} @endphp" @php if ($jns->namaJenisPerubahanCMF == 'Lainnya') {print 'id="lainnyaJenisPerubahan" onclick="lainnya2()"';} @endphp value="1" />
                                          <label class="form-check-label" for="defaultCheck3"> {{ $jns->namaJenisPerubahanCMF }} </label>
                                      </td>
                                      @php
                                          $no++
                                      @endphp
                                      @endforeach
                                      <td>
                                          <input class="form-control" type="@php if ($jenisPerubahanLainnya){if($jenisPerubahanLainnya->valueJenisPerubahan == 0){print 'hidden';}else{print 'text';}}else{print 'hidden';} @endphp" value="@php if ($jenisPerubahanLainnya){print $jenisPerubahanLainnya->lainnya;} @endphp"  id="valueJenisPerubahanCMF" placeholder="Lainnya ...." name="valueLainnya2" />
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="lastName" class="form-label">Target Implementasi <small style="color: red;">*)</small></label>
                              <input class="form-control" type="date" disabled required="required" name="dateImplementasiCMF" value="<?php echo $dt->dateImplementasiCMF ?>"  />
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="lastName" class="form-label">Type Perubahan <small style="color: red;">*)</small></label>
                              <div class="table-responsive">
                                <table class="table table-striped table-borderless ">
                                  <tbody>
                                    <td>
                                      <div class="form-check">
                                        <input name="typePerubahanCMF" class="form-check-input" type="radio" <?php if($dt->typePerubahanCMF == 1){ print 'checked'; } ?> id="defaultRadio1" value="1">
                                        <label class="form-check-label" for="defaultRadio1"> Temporary </label>
                                      </div>
                                    </td>
                                    <td>
                                      <div class="form-check">
                                        <input name="typePerubahanCMF" class="form-check-input" type="radio" <?php if($dt->typePerubahanCMF == 2){ print 'checked'; } ?> id="defaultRadio2" value="2">
                                        <label class="form-check-label" for="defaultRadio2"> Permanent </label>
                                      </div>
                                    </td>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                            <div class="mb-3 col-md-12">
                              <label for="email" class="form-label">Alasan Perubahan <small style="color: red;">*)</small></label>
                              <textarea class="form-control" disabled required="required" name="alasanPerubahanCMF" placeholder="Latar Belakang & Tujuan" ><?php echo $dt->alasanPerubahanCMF ?></textarea>
                            </div>
                            <div class="mb-3 col-md-12">
                              <label for="email" class="form-label">Dampak Terhadap Perubahan <small style="color: red;">*)</small></label>
                              <textarea class="form-control" disabled required="required" name="dampakPerubahanCMF" placeholder="Quality, Biaya, Food Safety, Keselamatan & Kesehatan Lingkungan atau Regulasi" ><?php echo $dt->dampakPerubahanCMF ?></textarea>
                            </div>
                            <div class="mb-3 col-md-12">
                              <label for="email" class="form-label">Deskripsi Perubahan <small style="color: red;">*)</small></label>
                              <textarea class="form-control" disabled required="required" name="deskripsiPerubahanCMF" placeholder="Keterangan Perubahan Produk" ><?php echo $dt->deskripsiPerubahanCMF ?></textarea>
                            </div>
                            <div class="mb-3 col-md-12">
                              <label for="email" class="form-label">Area Terkait <small style="color: red;">*)</small></label>
                            </div>
                            @foreach ($area_terkait as $dd)
                            <div class="mb-3 col-md-2">
                              <input class="form-check-input" type="checkbox" <?php if($dd->valueAreaCMF == 1){ print 'checked'; } ?> name="<?php echo $dd->kodeAreaCMF; ?>" value="1" />
                              <label class="form-check-label" for="defaultCheck3"> <?php echo $dd->namaAreaCMF; ?> </label>
                            </div>
                            @endforeach
                            <h4 class="card-header" style="text-align: left;">Risk Assesment</h4><hr>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">Resiko <small style="color: red;">*)</small></label>
                              <textarea class="form-control" disabled name="resikoCMF" required="required" placeholder="Resiko Dilakukannya Perubahan ..." ><?php echo $dt->resikoCMF ?></textarea>
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">Rencana Mitigasi <small style="color: red;">*)</small></label>
                              <textarea class="form-control" disabled name="mitigasiCMF" required="required" placeholder="Mitigasi Pencegahan dari Resiko ....." ><?php echo $dt->mitigasiCMF ?></textarea>
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">PIC <small style="color: red;">*)</small></label>
                              <input type="text" name="picRiskAssesmentCMF" required="required" value="<?php echo $dt->picRiskAssesmentCMF ?>" class="form-control" disabled placeholder="PIC Mitigasi">
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">Dead Line <small style="color: red;">*)</small></label>
                              <input type="date" name="deadLineCMF" required="required" value="<?php echo $dt->deadLineCMF ?>" class="form-control" disabled >
                            </div>
                            <?php 
                              if($dt->txtUploadDocument !== ""){ 
                                $tahun = date('Y', strtotime($dt->dateCMF));
                                $department = $dt->namaDepartment
                            ?>
                            <h4 class="card-header" style="text-align: left;">Lampiran Dokumen</h4><hr>
                            <div class="mb-3 col-md-8">
                              <label for="email" class="form-label">Lampiran Dokumen  </label>
                              <input type="file" name="txtUploadDocument" disabled class="form-control" accept=".pdf" >
                              <small style="color: red; "> *) Sistem hanya menerima dokumen dengan ekstensi .pdf</small>
                            </div>
                            <div class="mb-3 col-md-4">
                              <a href="<?php echo asset('upload').'/documents/'.$tahun.'/'.$department.'/'.$dt->txtUploadDocument ?>" target="_blank" data-bs-toggle="tooltip" data-bs-offset="0,4" data-bs-placement="top" data-bs-html="true" title="" data-bs-original-title="<i class='bx bx-bookmarks bx-xs' ></i> <span>Buka Lampiran</span>" style="margin-top: 25px;" class="btn rounded-pill btn-danger" class="btn rounded-pill btn-icon btn-danger">
                                  <span class="tf-icons bx bx-bookmarks"></span> Buka Lampiran
                              </a>
                            </div>
                            <?php } ?>
                            <?php if($dt->catatanDeptheadCMF1 !== ''){ ?>
                              <h4 class="card-header" style="text-align: left;">Persetujuan Dept Head Pemilik Proses</h4><hr>
                              <div class="mb-3 col-md-3">
                                <label for="email" class="form-label">CMF Telah Disetujui Oleh : </label>
                                <input type="text" class="form-control" disabled name="approveCMF1" value="<?php echo $dt->approveCMF1 ?>">
                              </div>
                              <div class="mb-3 col-md-3">
                                <label for="email" class="form-label">Tanggal Disetujui : </label>
                                <input type="text" class="form-control" disabled name="dateApproveCMF1" value="<?php echo date('d/m/Y', strtotime($dt->dateApproveCMF1)); ?>">
                              </div>
                              <div class="mb-3 col-md-6">
                                <label for="email" class="form-label">Catatan Dept Head </label>
                                <textarea class="form-control" disabled name="catatanDeptheadCMF1" required="required" placeholder="Catatan Dept Head ....." ><?php echo $dt->catatanDeptheadCMF1 ?></textarea>
                              </div>
                            <?php } ?>
                            <?php if($review_count !== 0){?>
                              <h4 class="card-header" style="text-align: left;">Review Departemen Terkait</h4><hr>
                            <?php foreach($review as $rev){  ?>
                              <div class="mb-3 col-md-3">
                                <label for="email" class="form-label">Direview Oleh : </label>
                                <input type="text" class="form-control" disabled name="approvedBy1" value="<?php echo $rev->approvedBy1 ?>">
                              </div>
                              <div class="mb-3 col-md-3">
                                <label for="email" class="form-label">Tanggal : </label>
                                <input type="text" class="form-control" disabled name="dateReviewCMF" value="<?php echo date('d/m/Y', strtotime($rev->dateReviewCMF)); ?>">
                              </div>
                              <div class="mb-3 col-md-2">
                                <label for="email" class="form-label">Hasil Review : </label><br>
                                  <a href="#" data-bs-toggle="tooltip" data-bs-offset="0,4" data-bs-placement="top" data-bs-html="true" title="" data-bs-original-title=""  class="btn <?php if($rev->adjustmentDepartment1CMF ==  1){echo 'btn-primary';}elseif($rev->adjustmentDepartment1CMF == -1){echo 'btn-danger';}else{echo 'btn-warning';} ?>" class="btn btn-icon btn-danger">
                                    <?php if($rev->adjustmentDepartment1CMF ==  1){echo '<span class="tf-icons bx bx-check"></span> SETUJU';}elseif($rev->adjustmentDepartment1CMF == -1){echo '<span class="tf-icons bx bx-x"></span> TIDAK SETUJU';}else{echo '<span class="tf-icons bx bx-history"></span> BELUM DI REVIEW';} ?>
                                </a>
                              </div>
                              <div class="mb-3 col-md-4">
                                <label for="email" class="form-label">Review Dept Head </label>
                                <textarea class="form-control" disabled name="ReviewDepartmentCMF" required="required" placeholder="Review Dept Head ....." ><?php echo $rev->reviewDepartmentCMF ?></textarea>
                              </div>
                            <?php } ?>
                            <?php } ?>
                            @if ($cmf_ditolak_count == 0)
                            <h4 class="card-header" style="text-align: left;">Mengetahui</h4><hr>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">System : </label>
                              <input type="text" class="form-control" disabled name="approveCMF2" value="<?php echo $dt->approveCMF2; ?>">
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">Tanggal : </label>
                              <input type="text" class="form-control" disabled name="dateApproveCMF2" value="<?php echo date('d/m/Y', strtotime($dt->dateApproveCMF2)); ?>">
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">MR : </label>
                              <input type="text" class="form-control" disabled name="approveCMF3" value="<?php echo $dt->approveCMF3; ?>">
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">Tanggal : </label>
                              <input type="text" class="form-control" disabled name="dateApproveCMF3" value="<?php echo date('d/m/Y', strtotime($dt->dateApproveCMF3)); ?>">
                            </div>
                            <div class="mb-3 col-md-6">
                                <label for="email" class="form-label">Manufacturing : </label>
                                <input type="text" class="form-control" disabled name="approveCMF4" value="<?php echo $dt->approveCMF4; ?>">
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">Tanggal : </label>
                              <input type="text" class="form-control" disabled name="dateApproveCMF4" value="<?php echo date('d/m/Y', strtotime($dt->dateApproveCMF4)); ?>">
                            </div>
                            <h4 class="card-header" style="text-align: left;">Review Setelah Dilakukan Perubahan</h4><hr>
                            <div class="mb-3 col-md-3">
                              <label for="email" class="form-label">Pemilik Proses : </label>
                              <input type="text" class="form-control" disabled name="approveCMF6" value="<?php echo $dt->approveCMF6; ?>">
                            </div>
                            <div class="mb-3 col-md-3">
                              <label for="email" class="form-label">Tanggal : </label>
                              <input type="text" class="form-control" disabled name="dateApproveCMF6" value="<?php echo date('d/m/Y', strtotime($dt->dateApproveCMF6)); ?>">
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">Detail Aktifitas : <small style="color: red;">*)</small></label>
                              <textarea class="form-control" name="detailAktifitasCMF" required="required" placeholder="Detail Aktifitas : (tanggal installasi, tanggal trial, tanggal implementasi) ... " ><?php echo $dt->detailAktifitasCMF ?></textarea>
                            </div>
                            <div class="mb-3 col-md-8">
                              <label for="email" class="form-label">Upload Dokumen Commissioning </label>
                              <input type="file" name="txtUploadCommissioning" class="form-control" accept=".pdf" >
                              <small style="color: red; "> *) Sistem hanya menerima dokumen dengan ekstensi .pdf</small>
                            </div>
                            @php
                                $tahun = date('Y', strtotime($dt->dateCMF));
                                $department = $dt->namaDepartment
                            @endphp
                            <div class="mb-3 col-md-4">
                              <a href="<?php echo asset('upload').'/commissioning/'.$tahun.'/'.$department.'/'.$dt->txtUploadCommissioning ?>" target="_blank" data-bs-toggle="tooltip" data-bs-offset="0,4" data-bs-placement="top" data-bs-html="true" title="" data-bs-original-title="<i class='bx bx-bookmarks bx-xs' ></i> <span>Buka Lampiran</span>" style="margin-top: 25px;" class="btn rounded-pill btn-danger" class="btn rounded-pill btn-icon btn-danger">
                                  <span class="tf-icons bx bx-bookmarks"></span> Buka Lampiran
                              </a>
                            </div>
                            @endif
                          </div>
                          <br><br>
                          <div class="mt-2">
                            <a href="{{ route($feedBack) }}" id="btnKembali" type="submit" class="btn btn-lg btn-secondary me-2" style="float: right;">Kembali</a>
                          </div>
                          <div class="mt-2">
                            <button type="submit" class="btn btn-lg btn-primary me-2" style="float: right;"><i class="menu-icon tf-icons bx bx-check"></i>Simpan</button>
                            <input type="hidden" name="feedBack" value="{{ route($feedBack) }}"/>
                            <input type="hidden" name="txtUploadCommissioningReject" value="{{ $dt->txtUploadCommissioning }}">
                            <input type="hidden" name="kodeCMF" value="{{ $dt->kodeCMF }}">
                            <input type="hidden" name="dateCMF" value="{{ $dt->dateCMF }}">
                            <input type="hidden" name="departmentCMF" value="{{ $dt->departmentCMF }}">
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Pills -->
            
            <!-- /KONTEN -->
          </div>
        </div>
      </div>

        @include('Auth/footer')
        <!-- / Footer -->
    </div>
    <!-- / Layout page -->
  </div>

  <!-- Overlay -->
  <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->


<!-- Core JS -->
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/jquery-3.1.0.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/jquery/jquery.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/popper/popper.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/js/bootstrap.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/js/menu.js"></script>
<!-- endbuild -->

<!-- Vendors JS -->
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/apex-charts/apexcharts.js"></script>

<!-- Main JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/main.js"></script>

<!-- Page JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/dashboards-analytics.js"></script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
</body>
</html>

<script>
  function retrunPengajuan(){
    var btnTolakCMF = document.getElementById('btnTolakCMF');
    var btnSetujui = document.getElementById('btnSetujui');
    var btnKembali = document.getElementById('btnKembali');
    var btnCancelCMF = document.getElementById('btnCancelCMF');
    var labelPenolakan = document.getElementById('labelPenolakan');
    var comentNOKCMF = document.getElementById('comentNOKCMF');
    var btnBatalPenolakan = document.getElementById('btnBatalPenolakan');
    var btnKirimPenolakan = document.getElementById('btnKirimPenolakan');

    btnTolakCMF.setAttribute('style', 'display: none;');
    btnSetujui.setAttribute('style', 'display: none;');
    btnKembali.setAttribute('style', 'display: none;');
    btnCancelCMF.setAttribute('style', 'display: none;');
    labelPenolakan.removeAttribute('style');
    comentNOKCMF.removeAttribute('style');
    btnBatalPenolakan.removeAttribute('style');
    btnBatalPenolakan.setAttribute('style', 'float: right;');
    btnKirimPenolakan.removeAttribute('style');
    btnKirimPenolakan.setAttribute('style', 'float: right;');
  }
</script>

<script>
  function batalPenolakan(){
    var btnTolakCMF = document.getElementById('btnTolakCMF');
    var btnSetujui = document.getElementById('btnSetujui');
    var btnKembali = document.getElementById('btnKembali');
    var btnCancelCMF = document.getElementById('btnCancelCMF');
    var labelPenolakan = document.getElementById('labelPenolakan');
    var comentNOKCMF = document.getElementById('comentNOKCMF');
    var btnBatalPenolakan = document.getElementById('btnBatalPenolakan');
    var btnKirimPenolakan = document.getElementById('btnKirimPenolakan');

    btnTolakCMF.removeAttribute('style');
    btnTolakCMF.setAttribute('style', 'float: right;');
    btnSetujui.removeAttribute('style');
    btnSetujui.setAttribute('style', 'float: right;');
    btnKembali.removeAttribute('style');
    btnKembali.setAttribute('style', 'float: right;');
    btnCancelCMF.removeAttribute('style');
    btnCancelCMF.setAttribute('style', 'float: right;');
    labelPenolakan.setAttribute('style', 'display: none;');
    comentNOKCMF.setAttribute('style', 'display: none;');
    btnBatalPenolakan.setAttribute('style', 'display: none;');
    btnKirimPenolakan.setAttribute('style', 'display: none;');
  }
</script>


<script>
  function lainnya(){
    var lainnyaPerubahan = document.getElementById('lainnyaPerubahan');
    var valuePerubahanCMF = document.getElementById('valuePerubahanCMF');
    if(lainnyaPerubahan.checked == true){
      valuePerubahanCMF.removeAttribute('type');
    }else{
      valuePerubahanCMF.setAttribute('type', 'hidden');
    }
  }
</script>

<script>
  function lainnya2(){
    var lainnyaJenisPerubahan = document.getElementById('lainnyaJenisPerubahan');
    var valueJenisPerubahanCMF = document.getElementById('valueJenisPerubahanCMF');
    if(lainnyaJenisPerubahan.checked == true){
      valueJenisPerubahanCMF.removeAttribute('type');
    }else{
      valueJenisPerubahanCMF.setAttribute('type', 'hidden');
    }
  }
</script>

