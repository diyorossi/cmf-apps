@include('Auth.header')
@include('Pic.navigation')
@include('Auth.topBar')

      <!-- Content wrapper -->
      <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
          <div class="row">
            <!-- KONTEN -->
            <!-- Pills -->
              <div class="container-xxl flex-grow-1 container-p-y">
              <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Dashboard / <a href="{{ route($role.'/ListCMF') }}" class="text-muted fw-light">Buat CMF</a> /</span> Open CMF
              </h4>
              <div class="row">
                <div class="nav-align-top mb-4">
                    <div class="tab-content">
                      <div class="tab-pane fade show active" id="navs-pills-justified-home" role="tabpanel">
                      <form method="POST" action="{{ route('Query/UpdateCMF') }}" enctype="multipart/form-data">
                        <div class="card-body">
                          <div class="row">
                            <h4 class="card-header" style="text-align: left; margin-top: -30px;">Data CMF</h4><hr>
                            <div class="mb-3 col-md-2">
                              <label for="firstName" class="form-label">Department</label>
                              <input class="form-control" type="text" disabled value="{{ $dt->namaDepartment }}" />
                            </div>
                            <div class="mb-3 col-md-2">
                              <label for="firstName" class="form-label">Area</label>
                              <input class="form-control" type="text" disabled value="{{ $dt->namaAreaTerkait }}" />
                            </div>
                            <div class="mb-3 col-md-5">
                              <label for="lastName" class="form-label">Pemilik Proses</label>
                              <input class="form-control" type="text" disabled value="{{ $dt->namaKaryawan }}" placeholder="Nama Lengkap Karyawan" />
                            </div>
                            <div class="mb-3 col-md-3">
                              <label for="lastName" class="form-label">Tanggal Pengajuan <small style="color: red;">*)</small></label>
                              <input class="form-control" type="date" disabled name="dateCMF" id="dateCMF" required="required" value="{{ $dt->dateCMF }}" placeholder="Tanggal Pengajuan" />
                            </div>
                            <div class="mb-3 col-md-12">
                              <label for="organization" class="form-label">Judul Perubahan <small style="color: red;">*)</small></label>
                              <input type="text" class="form-control" disabled value="{{ $dt->judulPerubahanCMF }}" name="judulPerubahanCMF" placeholder="Judul Perubahan" required="required" />
                            </div>
                            <div class="mb-3 col-md-12">
                              <label class="form-label" for="country">Perubahan <small style="color: red;">*)</small></label>
                              <div class="table-responsive">
                                <table class="table table-striped table-borderless ">
                                  <tbody>
                                    <td>
                                      <input class="form-check-input" name="bitMesin" <?php if($dt->bitMesin == 1){ print 'checked'; } ?> type="checkbox" value="1" />
                                      <label class="form-check-label" for="defaultCheck3"> Mesin </label>
                                    </td>
                                    <td>
                                      <input class="form-check-input" name="bitProses" <?php if($dt->bitProses == 1){ print 'checked'; } ?> type="checkbox" value="1" />
                                      <label class="form-check-label" for="defaultCheck3"> Proses </label>
                                    </td>
                                    <td>
                                      <input class="form-check-input" name="bitSystem" <?php if($dt->bitSystem == 1){ print 'checked'; } ?> type="checkbox" value="1" />
                                      <label class="form-check-label" for="defaultCheck3"> System </label>
                                    </td>
                                    <td>
                                      <input class="form-check-input" type="checkbox" <?php if($dt->bitLainnya !== ""){ print 'checked'; } ?> name="bitLainnya" value="1" id="lainnyaPerubahan" onclick="lainnya()" />
                                      <label class="form-check-label" for="defaultCheck3"> Lainnya </label>
                                    </td>
                                    <td>
                                      <input class="form-control" type="<?php if($dt->bitLainnya == ''){ print 'hidden'; }else{print 'text';} ?>" value="{{ $dt->bitLainnya }}"  id="valuePerubahanCMF" placeholder="Lainnya ...." name="valueLainnya" />
                                    </td>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                            <div class="mb-3 col-md-12">
                              <label class="form-label" for="country">Jenis Perubahan <small style="color: red;">*)</small></label>
                              <div class="table-responsive">
                                <table class="table table-striped table-borderless ">
                                  <tbody>
                                    <tr>
                                      <td>
                                        <input class="form-check-input" type="checkbox" <?php if($dt->bitInstallasi == 1){ print 'checked'; } ?> name="bitInstallasi" value="1" />
                                        <label class="form-check-label" for="defaultCheck3"> Installasi </label>
                                      </td>
                                      <td>
                                        <input class="form-check-input" type="checkbox" <?php if($dt->bitFormula == 1){ print 'checked'; } ?> name="bitFormula" value="1" />
                                        <label class="form-check-label" for="defaultCheck3"> Formula </label>
                                      </td>
                                      <td>
                                        <input class="form-check-input" type="checkbox" <?php if($dt->bitRawMaterial == 1){ print 'checked'; } ?> name="bitRawMaterial" value="1" />
                                        <label class="form-check-label" for="defaultCheck3"> Raw Material </label>
                                      </td>
                                      <td>
                                        <input class="form-check-input" type="checkbox" <?php if($dt->bitPackagingMaterial == 1){ print 'checked'; } ?> name="bitPackagingMaterial" value="1" />
                                        <label class="form-check-label" for="defaultCheck3"> Packaging Material </label>
                                      </td>
                                      <td>
                                        <input class="form-check-input" type="checkbox" <?php if($dt->bitSpesifikasiProduk == 1){ print 'checked'; } ?> name="bitSpesifikasiProduk" value="1" />
                                        <label class="form-check-label" for="defaultCheck3"> Spesifikasi Produk </label>
                                      </td>
                                      <td>
                                        <input class="form-check-input" type="checkbox" <?php if($dt->bitFoodSafetyManagement == 1){ print 'checked'; } ?> name="bitFoodSafetyManagement" value="1" />
                                        <label class="form-check-label" for="defaultCheck3"> Food Safety Management </label>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>
                                        <input class="form-check-input" type="checkbox" <?php if($dt->bitOracle == 1){ print 'checked'; } ?> name="bitOracle" value="1" />
                                        <label class="form-check-label" for="defaultCheck3"> Oracle </label>
                                      </td>
                                      <td>
                                        <input class="form-check-input" type="checkbox" <?php if($dt->bitSmk3 == 1){ print 'checked'; } ?> name="bitSmk3" value="1" />
                                        <label class="form-check-label" for="defaultCheck3"> SMK3 </label>
                                      </td>
                                      <td>
                                        <input class="form-check-input" type="checkbox" <?php if($dt->bitInstallasi == 1){ print 'checked'; } ?> name="bitHalalManagement" value="1" />
                                        <label class="form-check-label" for="defaultCheck3"> Halal Management </label>
                                      </td>
                                      <td>
                                        <input class="form-check-input" type="checkbox" <?php if($dt->bitDigitalisasi == 1){ print 'checked'; } ?> name="bitDigitalisasi" value="1" />
                                        <label class="form-check-label" for="defaultCheck3"> Digitalisasi </label>
                                      </td>
                                      <td>
                                        <input class="form-check-input" type="checkbox" <?php if($dt->bitLainnya2 !== ""){ print 'checked'; } ?> id="lainnyaJenisPerubahan" name="bitLainnya2" onclick="lainnya2()" />
                                        <label class="form-check-label" for="defaultCheck3"> Lainnya </label>
                                      </td>
                                      <td>
                                        <input class="form-control" type="<?php if($dt->bitLainnya2 == ''){ print 'hidden'; }else{print 'text';} ?>" value="{{ $dt->bitLainnya2 }}"  id="valueJenisPerubahanCMF" placeholder="Lainnya ...." name="valueLainnya" />
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="lastName" class="form-label">Target Implementasi <small style="color: red;">*)</small></label>
                              <input class="form-control" type="date" disabled required="required" name="dateImplementasiCMF" value="{{ $dt->dateImplementasiCMF }}"  />
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="lastName" class="form-label">Type Perubahan <small style="color: red;">*)</small></label>
                              <div class="table-responsive">
                                <table class="table table-striped table-borderless ">
                                  <tbody>
                                    <td>
                                      <div class="form-check">
                                        <input name="typePerubahanCMF" class="form-check-input" type="radio" <?php if($dt->typePerubahanCMF == 1){ print 'checked'; } ?> id="defaultRadio1" value="1">
                                        <label class="form-check-label" for="defaultRadio1"> Temporary </label>
                                      </div>
                                    </td>
                                    <td>
                                      <div class="form-check">
                                        <input name="typePerubahanCMF" class="form-check-input" type="radio" <?php if($dt->typePerubahanCMF == 2){ print 'checked'; } ?> id="defaultRadio2" value="2">
                                        <label class="form-check-label" for="defaultRadio2"> Permanent </label>
                                      </div>
                                    </td>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                            <div class="mb-3 col-md-12">
                              <label for="email" class="form-label">Alasan Perubahan <small style="color: red;">*)</small></label>
                              <textarea class="form-control" disabled required="required" name="alasanPerubahanCMF" placeholder="Latar Belakang & Tujuan" ><?php echo $dt->alasanPerubahanCMF ?></textarea>
                            </div>
                            <div class="mb-3 col-md-12">
                              <label for="email" class="form-label">Dampak Terhadap Perubahan <small style="color: red;">*)</small></label>
                              <textarea class="form-control" disabled required="required" name="dampakPerubahanCMF" placeholder="Quality, Biaya, Food Safety, Keselamatan & Kesehatan Lingkungan atau Regulasi" ><?php echo $dt->dampakPerubahanCMF ?></textarea>
                            </div>
                            <div class="mb-3 col-md-12">
                              <label for="email" class="form-label">Deskripsi Perubahan <small style="color: red;">*)</small></label>
                              <textarea class="form-control" disabled required="required" name="deskripsiPerubahanCMF" placeholder="Keterangan Perubahan Produk" ><?php echo $dt->deskripsiPerubahanCMF ?></textarea>
                            </div>
                            <div class="mb-3 col-md-12">
                              <label for="email" class="form-label">Area Terkait <small style="color: red;">*)</small></label>
                            </div>
                            @foreach ($area_terkait as $dd)
                            <div class="mb-3 col-md-2">
                              <input class="form-check-input" type="checkbox" <?php if($dd->valueAreaCMF == 1){ print 'checked'; } ?> name="<?php echo $dd->kodeAreaCMF; ?>" value="1" />
                              <label class="form-check-label" for="defaultCheck3"> <?php echo $dd->namaAreaCMF; ?> </label>
                            </div>
                            @endforeach
                            <h4 class="card-header" style="text-align: left;">Risk Assesment</h4><hr>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">Resiko <small style="color: red;">*)</small></label>
                              <textarea class="form-control" disabled name="resikoCMF" required="required" placeholder="Resiko Dilakukannya Perubahan ..." ><?php echo $dt->resikoCMF ?></textarea>
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">Rencana Mitigasi <small style="color: red;">*)</small></label>
                              <textarea class="form-control" disabled name="mitigasiCMF" required="required" placeholder="Mitigasi Pencegahan dari Resiko ....." ><?php echo $dt->mitigasiCMF ?></textarea>
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">PIC <small style="color: red;">*)</small></label>
                              <input type="text" name="picRiskAssesmentCMF" required="required" value="{{ $dt->picRiskAssesmentCMF }}" class="form-control" disabled placeholder="PIC Mitigasi">
                            </div>
                            <div class="mb-3 col-md-6">
                              <label for="email" class="form-label">PIC <small style="color: red;">*)</small></label>
                              <input type="date" name="deadLineCMF" required="required" value="{{ $dt->deadLineCMF }}" class="form-control" disabled >
                            </div>
                            <h4 class="card-header" style="text-align: left;">Upload Dokumen</h4><hr>
                            <div class="mb-3 col-md-8">
                              <label for="email" class="form-label">Upload Dokumen  </label>
                              <input type="file" name="txtUploadDocument" disabled class="form-control" accept=".pdf" >
                              <small style="color: red; "> *) Sistem hanya menerima dokumen dengan ekstensi .pdf</small>
                            </div>
                            <?php 
                              if($dt->txtUploadDocument !== ""){ 
                                $tahun = date('Y', strtotime($dt->dateCMF));
                                $department = $dt->namaDepartment;
                            ?>
                            <div class="mb-3 col-md-4">
                              <a href="<?php echo asset('upload').'/documents/'.$tahun.'/'.$department.'/'.$dt->txtUploadDocument; ?>" target="_blank" data-bs-toggle="tooltip" data-bs-offset="0,4" data-bs-placement="top" data-bs-html="true" title="" data-bs-original-title="<i class='bx bx-bookmarks bx-xs' ></i> <span>Buka Lampiran</span>" style="margin-top: 25px;" class="btn rounded-pill btn-danger" class="btn rounded-pill btn-icon btn-danger">
                                  <span class="tf-icons bx bx-bookmarks"></span> Buka Lampiran
                              </a>
                            </div>
                            <?php } ?>
                            <?php if($dt->catatanDeptheadCMF1 !== ''){ ?>
                              <h4 class="card-header" style="text-align: left;">Persetujuan Dept Head Pemilik Proses</h4><hr>
                              <div class="mb-3 col-md-3">
                                <label for="email" class="form-label">CMF Telah Disetujui Oleh : </label>
                                <input type="text" class="form-control" disabled name="approveCMF1" value="{{ $dt->approveCMF1 }}">
                              </div>
                              <div class="mb-3 col-md-3">
                                <label for="email" class="form-label">Tanggal Disetujui : </label>
                                <input type="text" class="form-control" disabled name="dateApproveCMF1" value="{{ date('d/m/Y', strtotime($dt->dateApproveCMF1)) }}">
                              </div>
                              <div class="mb-3 col-md-6">
                                <label for="email" class="form-label">Catatan Dept Head </label>
                                <textarea class="form-control" disabled name="catatanDeptheadCMF1" required="required" placeholder="Catatan Dept Head ....." ><?php echo $dt->catatanDeptheadCMF1 ?></textarea>
                              </div>
                            <?php } ?>
                            <?php if($review_count == 0){?>
                              <h4 class="card-header" style="text-align: left;">Review Departemen Terkait</h4><hr>
                            <?php foreach($review->result_array() as $rev){  ?>
                              <div class="mb-3 col-md-3">
                                <label for="email" class="form-label">Direview Oleh : </label>
                                <input type="text" class="form-control" disabled name="approvedBy1" value="{{ $rev->approvedBy1 }}">
                              </div>
                              <div class="mb-3 col-md-3">
                                <label for="email" class="form-label">Tanggal : </label>
                                <input type="text" class="form-control" disabled name="dateReviewCMF" value="{{ date('d/m/Y', strtotime($rev->dateReviewCMF)) }}">
                              </div>
                              <div class="mb-3 col-md-3">
                                <label for="email" class="form-label">Hasil Review : </label>
                                <input type="text" class="form-control" disabled name="adjustmentDepartment1CMF" value="<?php if($rev['adjustmentDepartment1CMF'] == 1){echo 'SETUJU';}elseif($rev['adjustmentDepartment1CMF'] == -1){echo 'TIDAK SETUJU';}else{echo 'BELUM DI REVIEW';} ?>">
                              </div>
                              <div class="mb-3 col-md-3">
                                <label for="email" class="form-label">Review Dept Head </label>
                                <textarea class="form-control" disabled name="ReviewDepartmentCMF" required="required" placeholder="Review Dept Head ....." ><?php echo $rev['reviewDepartmentCMF'] ?></textarea>
                              </div>
                            <?php } ?>
                            <?php } ?>
                          </div>
                          <br><br>
                          <div class="mt-2">
                            <a href="{{ route($role.'/ListCMF') }}" type="submit" class="btn btn-lg btn-secondary me-2" style="float: right;">Kembali</a>
                          </div>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Pills -->
            
            <!-- /KONTEN -->
          </div>
        </div>
      </div>

      @include('Auth/footer')
        <!-- / Footer -->
    </div>
    <!-- / Layout page -->
  </div>

  <!-- Overlay -->
  <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->


<!-- Core JS -->
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/jquery-3.1.0.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/jquery/jquery.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/popper/popper.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/js/bootstrap.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/js/menu.js"></script>
<!-- endbuild -->

<!-- Vendors JS -->
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/apex-charts/apexcharts.js"></script>

<!-- Main JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/main.js"></script>

<!-- Page JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/dashboards-analytics.js"></script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
</body>
</html>


<script>
  function lainnya(){
    var lainnyaPerubahan = document.getElementById('lainnyaPerubahan');
    var valuePerubahanCMF = document.getElementById('valuePerubahanCMF');
    if(lainnyaPerubahan.checked == true){
      valuePerubahanCMF.removeAttribute('type');
    }else{
      valuePerubahanCMF.setAttribute('type', 'hidden');
    }
  }
</script>

<script>
  function lainnya2(){
    var lainnyaJenisPerubahan = document.getElementById('lainnyaJenisPerubahan');
    var valueJenisPerubahanCMF = document.getElementById('valueJenisPerubahanCMF');
    if(lainnyaJenisPerubahan.checked == true){
      valueJenisPerubahanCMF.removeAttribute('type');
    }else{
      valueJenisPerubahanCMF.setAttribute('type', 'hidden');
    }
  }
</script>

<script>
  $('#btn_submit').click(function() {
        var klausul = $('#klausul').val();
        var tgl_kejadian = $('#tgl_kejadian_ncr').val();
        var uraian_kejadian = $('#uraian_kejadian_ncr').val();
        var jenis_masalah = $('#jenis_masalah').val();
        var gambar_ncr = $('#gambar_ncr').val();


  });
</script>
