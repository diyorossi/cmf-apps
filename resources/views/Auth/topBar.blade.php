
<?php 
    $hari = date ("D");
    $tgl = date ("d");
    $bln = date("m");
    $thn = date("Y");
 
    switch($hari){
        case 'Sun':
            $hari_ini = "Minggu";
        break;
 
        case 'Mon':         
            $hari_ini = "Senin";
        break;
 
        case 'Tue':
            $hari_ini = "Selasa";
        break;
 
        case 'Wed':
            $hari_ini = "Rabu";
        break;
 
        case 'Thu':
            $hari_ini = "Kamis";
        break;
 
        case 'Fri':
            $hari_ini = "Jumat";
        break;
 
        case 'Sat':
            $hari_ini = "Sabtu";
        break;
        
        default:
            $hari_ini = "Tidak di ketahui";     
        break;
    }

    switch($bln){
        case '01':
            $bulan = "Januari";
        break;
 
        case '02':         
            $bulan = "Februari";
        break;
 
        case '03':
            $bulan = "Maret";
        break;
 
        case '04':
            $bulan = "April";
        break;
 
        case '05':
            $bulan = "Mei";
        break;
 
        case '06':
            $bulan = "Juni";
        break;
 
        case '07':
            $bulan = "Juli";
        break;

        case '08':
            $bulan = "Agustus";
        break;

        case '09':
            $bulan = "September";
        break;

        case '10':
            $bulan = "Oktober";
        break;

        case '11':
            $bulan = "November";
        break;

        case '12':
            $bulan = "Desember";
        break;

        default:
            $bulan = "Tidak di ketahui";     
        break;
    }

    $dateDay = $hari_ini.", ".$tgl." ".$bulan. " ".$thn;
 ?>
 <script>
    var myVar = setInterval(myTimer, 1000);

    function myTimer() {
      var d = new Date();
      var t = d.toLocaleTimeString();
      document.getElementById("demo").innerHTML = t;
    }
</script>
<div class="layout-page">
  <nav
    class="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme"
    id="layout-navbar"
  >
    <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
      <a class="nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
        <i class="bx bx-menu bx-sm"></i>
      </a>
    </div>

    <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
      <!-- Search -->
      <div class="navbar-nav align-items-center">
        <div class="nav-item d-flex align-items-center">
          <i class="bx bx-search fs-4 lh-0"></i>
          <input
            type="text"
            class="form-control border-0 shadow-none"
            placeholder="Search..."
            aria-label="Search..."
          />
        </div>
      </div>
      <!-- /Search -->
      <ul class="navbar-nav flex-row align-items-center ms-auto">
        <p style="margin-top: 15px; padding-right: 15px;"><?php echo $dateDay; ?></p>
        <p id="demo" style="margin-top: 15px; padding-right: 15px;"></p>
        <!-- User -->
        <li class="nav-item navbar-dropdown dropdown-user dropdown">
          <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
            <div class="avatar avatar-online">
              <img src="<?php echo asset('img') ?>/profile/<?php echo $gambarUser ?>" alt class="w-px-40 h-auto rounded-circle" />
            </div>
          </a>
          <ul class="dropdown-menu dropdown-menu-end">
            <li>
              <a class="dropdown-item" href="#">
                <div class="d-flex">
                  <div class="flex-shrink-0 me-3">
                    <div class="avatar avatar-online">
                      <img src="<?php echo asset('img') ?>/profile/<?php echo $gambarUser ?>" alt class="w-px-40 h-auto rounded-circle" />
                    </div>
                  </div>
                  <div class="flex-grow-1">
                    <span class="fw-semibold d-block"><?php echo $namaKaryawan; ?></span>
                    <small class="text-muted"><?php echo $posisi; ?></small>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <div class="dropdown-divider"></div>
            </li>
            <li>
              <a class="dropdown-item" href="{{ route('logout') }}">
                <i class="bx bx-power-off me-2"></i>
                <span class="align-middle">Log Out</span>
              </a>
            </li>
          </ul>
        </li>
        <!--/ User -->
      </ul>
    </div>
  </nav>

  <!-- / Navbar -->