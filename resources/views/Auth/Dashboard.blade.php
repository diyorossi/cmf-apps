<!DOCTYPE html>

<html
  lang="en"
  class="light-style"
  dir="ltr"
  data-theme="theme-default"
  data-assets-path="<?php echo asset('cmf/') ?>/assets/"
  data-template="vertical-menu-template-free"
>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
    />

    <title>CMF ONLINE | Dashboard CMF Progress</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="<?php echo asset('cmf/') ?>/assets/img/favicon/favicon.ico" />

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.min.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
      rel="stylesheet"
    />

    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="<?php echo asset('cmf/') ?>/assets/vendor/fonts/boxicons.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="<?php echo asset('cmf/') ?>/assets/vendor/css/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="<?php echo asset('cmf/') ?>/assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="<?php echo asset('cmf/') ?>/assets/css/demo.css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="<?php echo asset('cmf/') ?>/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />

    <!-- Page CSS -->
    <!-- Page -->
    <link rel="stylesheet" href="<?php echo asset('cmf/') ?>/assets/vendor/css/pages/page-misc.css" />
    <!-- Helpers -->
    <script src="<?php echo asset('cmf/') ?>/assets/vendor/js/helpers.js"></script>

    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="<?php echo asset('cmf/') ?>/assets/js/config.js"></script>
    <link href="https://cdn.datatables.net/v/bs5/dt-1.13.4/ .min.css" rel="stylesheet"/>
  </head>

  <body>
    <!-- Content -->
    <div class="layout-wrapper layout-content-navbar">
    <div class="layout-container">

    <!-- Error -->
    <div class="container-xxl container-p-y">
        <h2 class="mb-2 mx-2">Dashboard CMF Progress</h2>
        <p class="mb-4 mx-2">Pantau Progress CMF Anda Tanpa Harus Login Terlebih Dahulu</p>
        <a href="{{ route('Auth/Login') }}" class="btn btn-primary mb-5">Halaman Login</a>
            <div class="mt-3">
            <div class="col-xl-12" style="margin-left: auto; margin-right: auto; display:block;">
            <div class="nav-align-top mb-4">
              <ul class="nav nav-tabs nav-fill" role="tablist">
                <li class="nav-item">
                  <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab" data-bs-target="#navs-justified-step1" aria-controls="navs-justified-home" aria-selected="true" >
                    <i class="tf-icons bx bx-user"></i> Step 1
                  </button>
                </li>
                <li class="nav-item">
                  <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-justified-step2" aria-controls="navs-justified-home" aria-selected="true">
                    <i class="tf-icons bx bx-user"></i> Step 2
                  </button>
                </li>
                <li class="nav-item">
                  <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-justified-step3" aria-controls="navs-justified-home" aria-selected="true">
                    <i class="tf-icons bx bx-user"></i> Step 3
                  </button>
                </li>
                <li class="nav-item">
                    <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-justified-step4" aria-controls="navs-justified-home" aria-selected="true">
                      <i class="tf-icons bx bx-user"></i> Step 4
                    </button>
                  </li>
                  <li class="nav-item">
                    <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-justified-step5" aria-controls="navs-justified-home" aria-selected="true">
                      <i class="tf-icons bx bx-user"></i> Step 5
                    </button>
                  </li>
                  <li class="nav-item">
                    <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-justified-step6" aria-controls="navs-justified-home" aria-selected="true">
                      <i class="tf-icons bx bx-user"></i> Step 6
                    </button>
                  </li>
                  <li class="nav-item">
                    <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-justified-step7" aria-controls="navs-justified-home" aria-selected="true">
                      <i class="tf-icons bx bx-user"></i> Step 7
                    </button>
                  </li>
                  <li class="nav-item">
                    <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-justified-step8" aria-controls="navs-justified-home" aria-selected="true">
                      <i class="tf-icons bx bx-user"></i> Step 8
                    </button>
                  </li>
                  <li class="nav-item">
                    <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-justified-step9" aria-controls="navs-justified-home" aria-selected="true">
                      <i class="tf-icons bx bx-user"></i> Step 9
                    </button>
                  </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane fade show active" id="navs-justified-step1" role="tabpanel">
                  <div class="card">
                    <h5 class="card-header">Step 1 : Pemilik Proses</h5>
                    <div class="tab-content">
                      <table id="example" class="display" style="width:100%">
                        <thead class="text-nowrap">
                          <tr>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">#</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Nomor CMF</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Pembuat CMF</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Department</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Proses Terakhir</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Tanggal Proses Terakhir</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Lama Proses</th>
                          </tr>
                        </thead>
                        <tbody>
                          @php
                              $no = 1;
                          @endphp
                          @foreach ($cmf as $dt)
                          <tr style="text-align: center">
                            <td>{{ $no ++; }}</td>
                            <td>{{ $dt->nomorCMF; }}</td>
                            <td>{{ $dt->namaKaryawan; }}</td>
                            <td>{{ $dt->namaDepartment; }}</td>
                            <td>@if ($dt->statusProsesCMF == -1)
                              Pengajuan Dikoreksi Oleh Atasan
                                @elseif ($dt->statusProsesCMF == -2)
                                Pengajuan Dikoreksi Oleh Document Control
                                @elseif ($dt->statusProsesCMF == -4)
                                Pengajuan Dikoreksi Oleh Dept Head Area Terkait
                                @elseif ($dt->statusProsesCMF == -5)
                                Pengajuan Dikoreksi Oleh Spv. System
                                @elseif ($dt->statusProsesCMF == -6)
                                Pengajuan Dikoreksi Oleh MR
                                @elseif ($dt->statusProsesCMF == -7)
                                Pengajuan Dikoreksi Oleh Manufacturing Head
                                @elseif ($dt->statusProsesCMF == -8)
                                Pengajuan Dikoreksi Oleh Document Control Setelah Review
                            @endif</td>
                            <td>{{ date('d-m-Y', strtotime($dt->dtmInsertedDate)) }}</td>
                            <td>@php
                                $date = new DateTime($dt->dtmInsertedDate);
                                $today = new DateTime();
                                $lamaProses = $today -> diff($date) -> format("%a");
                                echo $lamaProses . ' Hari';
                            @endphp</td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="navs-justified-step2" role="tabpanel">
                  <div class="card">
                    <h5 class="card-header">Step 2 : Dept Head Pemilik Proses</h5>
                    <div class="tab-content">
                      <table id="example2" class="display" style="width:100%">
                        <thead class="text-nowrap">
                          <tr>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">#</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Department Pembuat CMF</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Jumlah CMF Perlu Persetujuan Atasan</th>
                          </tr>
                          </tr>
                        </thead>
                        <tbody>
                          @php
                              $no = 1;
                          @endphp
                          @foreach ($cmf2 as $dt)
                          <tr style="text-align: center">
                            <td>{{ $no ++; }}</td>
                            <td>{{ $dt->namaDepartment; }}</td>
                            <td>
                              <div class="btn-group" role="group">
                                <button
                                  id="btnGroupDrop1"
                                  type="button"
                                  class="btn btn-outline-secondary dropdown-toggle"
                                  data-bs-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                >{{ $dt->jumlahCMF }}
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGrou pDrop1">
                                  <div class="table-responsive text-nowrap">
                                    <table class="table">
                                      <thead class="text-nowrap">
                                        <tr>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">#</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Nomor CMF</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Nama Pembuat</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Area Pemilik Proses</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Judul Perubahan</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Tanggal Proses Sebelumnya</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Lama Proses</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @php
                                            $kodeDepartmentCMF = $dt->departmentCMF;
                                            $no = 1;
                                            $data = DB::table('tr_cmf')
                                                      ->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
                                                      ->join('m_area_terkait', 'tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
                                                      ->where('departmentCMF', $kodeDepartmentCMF)
                                                      ->where('statusProsesCMF', 1)
                                                      ->orderBy('dateCMF', 'DESC')
                                                      ->get();
                                        @endphp
                                        @foreach ($data as $dt)
                                        <tr style="text-align: center">
                                          <td>{{ $no++ }}</td>
                                          <td>{{ $dt->nomorCMF }}</td>
                                          <td>{{ $dt->namaKaryawan }}</td>
                                          <td>{{ $dt->namaAreaTerkait }}</td>
                                          <td class="text-wrap">{{ $dt->judulPerubahanCMF }}</td>
                                          <td>{{ date('d-m-Y', strtotime($dt->dtmUpdatedDate)) }}</td>
                                          <td>@php
                                              $date = new DateTime($dt->dtmUpdatedDate);
                                              $today = new DateTime();
                                              $lamaProses = $today -> diff($date) -> format("%a");
                                              echo $lamaProses . ' Hari';
                                          @endphp</td>
                                        </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </td> 
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="navs-justified-step3" role="tabpanel">
                  <div class="card">
                    <h5 class="card-header">Step 3 : Dept Head Area Terkait</h5>
                    <div class="tab-content">
                      <table id="example3" class="display" style="width:100%">
                        <thead class="text-nowrap">
                          <tr>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">#</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Department Area Terkait</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Jumlah CMF Perlu Review Depthead</th>
                          </tr>
                        </thead>
                        <tbody>
                          @php
                              $no = 1;
                          @endphp
                          @foreach ($cmf3 as $dt)
                          <tr style="text-align: center">
                            <td>{{ $no ++; }}</td>
                            <td>{{ $dt->namaDepartment }}</td>
                            <td>
                              <div class="btn-group" role="group">
                                <button
                                  id="btnGroupDrop1"
                                  type="button"
                                  class="btn btn-outline-secondary dropdown-toggle"
                                  data-bs-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                >
                                @php
                                    $kodeDepartmentCMF = $dt->kodeDepartmentCMF;
                                    $queryCount = DB::table('tr_department_terkait')
                                                    ->join('tr_cmf', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
                                                    ->where('kodeDepartmentCMF', $kodeDepartmentCMF)
                                                    ->where('statusProsesCMF', 2)
                                                    ->count();
                                    echo $queryCount;
                                @endphp
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGrou pDrop1">
                                  <div class="table-responsive text-nowrap">
                                    <table class="table">
                                      <thead class="text-nowrap">
                                        <tr>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">#</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Nomor CMF</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Nama Pembuat CMF</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Area Pemilik Proses</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Judul Perubahan</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Tanggal Proses Sebelumnya</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Lama Proses</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @php
                                            $noo = 1;
                                            $data = DB::table('tr_cmf')
                                                      ->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
                                                      ->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
                                                      ->join('m_area_terkait', 'tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
                                                      ->where('kodeDepartmentCMF', $kodeDepartmentCMF)
                                                      ->where('statusProsesCMF', 2)
                                                      ->get();
                                        @endphp
                                        @foreach ($data as $dt)
                                        <tr style="text-align: center">
                                          <td>{{ $noo++ }}</td>
                                          <td>{{ $dt->nomorCMF }}</td>
                                          <td>{{ $dt->namaKaryawan }}</td>
                                          <td>{{ $dt->namaAreaTerkait }}</td>
                                          <td class="text-wrap">{{ $dt->judulPerubahanCMF }}</td>
                                          <td>{{ date('d-m-Y', strtotime($dt->dateApproveCMF1)) }}</td>
                                          <td>@php
                                              $date = new DateTime($dt->dateApproveCMF1);
                                              $today = new DateTime();
                                              $lamaProses = $today -> diff($date) -> format("%a");
                                              echo $lamaProses . ' Hari';
                                          @endphp</td>
                                        </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="navs-justified-step4" role="tabpanel">
                  <div class="card">
                    <h5 class="card-header">Step 4 : Spv. System, MR, Manufacturing Head</h5>
                    <div class="tab-content">
                      <table id="example4" class="display" style="width:100%">
                        <thead class="text-nowrap">
                          <tr>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">#</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Nomor CMF</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Pembuat CMF</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Department</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Proses Terakhir</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Tanggal Proses Terakhir</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Lama Proses</th>
                          </tr>
                        </thead>
                        <tbody>
                          @php
                              $no = 1;
                          @endphp
                          @foreach ($cmf4 as $dt)
                          <tr style="text-align: center">
                            <td>{{ $no ++; }}</td>
                            <td>{{ $dt->nomorCMF; }}</td>
                            <td>{{ $dt->namaKaryawan; }}</td>
                            <td>{{ $dt->namaDepartment; }}</td>
                            <td>@if ($dt->statusProsesCMF == 3)
                                Tunggu Persetujuan Spv. System
                                @elseif ($dt->statusProsesCMF == 4)
                                Tunggu Persetujuan MR
                                @elseif ($dt->statusProsesCMF == 5)
                                Tunggu Persetujuan Manufacturing Head
                            @endif</td>
                            <td>{{ date('d-m-Y', strtotime($dt->dtmUpdatedDate)) }}</td>
                            <td>@php
                                $date = new DateTime($dt->dtmUpdatedDate);
                                $today = new DateTime();
                                $lamaProses = $today -> diff($date) -> format("%a");
                                echo $lamaProses . ' Hari';
                            @endphp</td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="navs-justified-step5" role="tabpanel">
                  <div class="card">
                    <h5 class="card-header">Step 5 : Document Control</h5>
                    <div class="tab-content">
                      <table id="example5" class="display" style="width:100%">
                        <thead class="text-nowrap">
                          <tr>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">#</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Nomor CMF</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Pembuat CMF</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Department</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Proses Terakhir</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Tanggal Proses Terakhir</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Lama Proses</th>
                          </tr>
                        </thead>
                        <tbody>
                          @php
                              $no = 1;
                          @endphp
                          @foreach ($cmf5 as $dt)
                          <tr style="text-align: center">
                            <td>{{ $no ++; }}</td>
                            <td>{{ $dt->nomorCMF; }}</td>
                            <td>{{ $dt->namaKaryawan; }}</td>
                            <td>{{ $dt->namaDepartment; }}</td>
                            <td>@if ($dt->statusProsesCMF == 6)
                                Tunggu Perubahan Selesai
                            @endif</td>
                            <td>{{ date('d-m-Y', strtotime($dt->dateApproveCMF4)) }}</td>
                            <td>@php
                                $date = new DateTime($dt->dateApproveCMF4);
                                $today = new DateTime();
                                $lamaProses = $today -> diff($date) -> format("%a");
                                echo $lamaProses . ' Hari';
                            @endphp</td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="navs-justified-step6" role="tabpanel">
                  <div class="card">
                    <h5 class="card-header">Step 6 : Pemilik Proses</h5>
                    <div class="tab-content">
                      <table id="example6" class="display" style="width:100%">
                        <thead class="text-nowrap">
                          <tr>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">#</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Nomor CMF</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Pembuat CMF</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Department</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Proses Terakhir</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Tanggal Proses Terakhir</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Lama Proses</th>
                          </tr>
                        </thead>
                        <tbody>
                          @php
                              $no = 1;
                          @endphp
                          @foreach ($cmf6 as $dt)
                          <tr style="text-align: center">
                            <td>{{ $no ++; }}</td>
                            <td>{{ $dt->nomorCMF; }}</td>
                            <td>{{ $dt->namaKaryawan; }}</td>
                            <td>{{ $dt->namaDepartment; }}</td>
                            <td>@if ($dt->statusProsesCMF == 7)
                                Pengisian Detail Aktifitas & Upload Bukti Commissioning
                                @elseif ($dt->statusProsesCMF == -9)
                                Dikoreksi Oleh Atasan
                                @elseif ($dt->statusProsesCMF == -10)
                                Dikoreksi Oleh Dept Head Area Terkait
                                @elseif ($dt->statusProsesCMF == -11)
                                Dikoreksi Oleh Verifikator Tim Food Safety
                                @elseif ($dt->statusProsesCMF == -9)
                                Dikoreksi Oleh Document Control
                                @elseif ($dt->statusProsesCMF == -9)
                                Dikoreksi Oleh MR
                            @endif</td>
                            <td>{{ date('d-m-Y', strtotime($dt->dateApproveCMF5)) }}</td>
                            <td>@php
                                $date = new DateTime($dt->dateApproveCMF5);
                                $today = new DateTime();
                                $lamaProses = $today -> diff($date) -> format("%a");
                                echo $lamaProses . ' Hari';
                            @endphp</td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="navs-justified-step7" role="tabpanel">
                  <div class="card">
                    <h5 class="card-header">Step 7 : Dept Head Pemilik Proses</h5>
                    <div class="tab-content">
                      <table id="example7" class="display" style="width:100%">
                        <thead class="text-nowrap">
                          <tr>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">#</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Department Pembuat CMF</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Jumlah CMF Perlu Persetujuan Atasan</th>
                          </tr>
                          </tr>
                        </thead>
                        <tbody>
                          @php
                              $no = 1;
                          @endphp
                          @foreach ($cmf7 as $dt)
                          <tr style="text-align: center">
                            <td>{{ $no ++; }}</td>
                            <td>{{ $dt->namaDepartment; }}</td>
                            <td>
                              <div class="btn-group" role="group">
                                <button
                                  id="btnGroupDrop1"
                                  type="button"
                                  class="btn btn-outline-secondary dropdown-toggle"
                                  data-bs-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                >{{ $dt->jumlahCMF }}
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGrou pDrop1">
                                  <div class="table-responsive text-nowrap">
                                    <table class="table">
                                      <thead class="text-nowrap">
                                        <tr>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">#</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Nomor CMF</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Nama Pembuat</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Area Pemilik Proses</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Judul Perubahan</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Tanggal Proses Sebelumnya</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Lama Proses</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @php
                                            $kodeDepartmentCMF = $dt->departmentCMF;
                                            $nooo = 1;
                                            $data = DB::table('tr_cmf')
                                                      ->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
                                                      ->join('m_area_terkait', 'tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
                                                      ->where('departmentCMF', $kodeDepartmentCMF)
                                                      ->where('statusProsesCMF', 1)
                                                      ->orderBy('dateCMF', 'DESC')
                                                      ->get();
                                        @endphp
                                        @foreach ($data as $dt)
                                        <tr style="text-align: center">
                                          <td>{{ $nooo++ }}</td>
                                          <td>{{ $dt->nomorCMF }}</td>
                                          <td>{{ $dt->namaKaryawan }}</td>
                                          <td>{{ $dt->namaAreaTerkait }}</td>
                                          <td class="text-wrap">{{ $dt->judulPerubahanCMF }}</td>
                                          <td>{{ date('d-m-Y', strtotime($dt->dateApproveCMF6)) }}</td>
                                          <td>@php
                                              $date = new DateTime($dt->dateApproveCMF6);
                                              $today = new DateTime();
                                              $lamaProses = $today -> diff($date) -> format("%a");
                                              echo $lamaProses . ' Hari';
                                          @endphp</td>
                                        </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </td> 
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="navs-justified-step8" role="tabpanel">
                  <div class="card">
                    <h5 class="card-header">Step 8 : Department Area Terkait</h5>
                    <div class="tab-content">
                      <table id="example8" class="display" style="width:100%">
                        <thead class="text-nowrap">
                          <tr>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">#</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Department Area Terkait</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Jumlah CMF Perlu Evaluasi Depthead</th>
                          </tr>
                        </thead>
                        <tbody>
                          @php
                              $no = 1;
                          @endphp
                          @foreach ($cmf8 as $dt)
                          <tr style="text-align: center">
                            <td>{{ $no ++; }}</td>
                            <td>{{ $dt->namaDepartment }}</td>
                            <td>
                              <div class="btn-group" role="group">
                                <button
                                  id="btnGroupDrop1"
                                  type="button"
                                  class="btn btn-outline-secondary dropdown-toggle"
                                  data-bs-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                >
                                @php
                                    $kodeDepartmentCMF = $dt->kodeDepartmentCMF;
                                    $queryCount = DB::table('tr_department_terkait')
                                                    ->where('kodeDepartmentCMF', $kodeDepartmentCMF)
                                                    ->count();
                                    echo $queryCount;
                                @endphp
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGrou pDrop1">
                                  <div class="table-responsive text-nowrap">
                                    <table class="table">
                                      <thead class="text-nowrap">
                                        <tr>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">#</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Nomor CMF</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Nama Pembuat CMF</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Area Pemilik Proses</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Judul Perubahan</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Tanggal Proses Sebelumnya</th>
                                          <th style="font-weight: bold; font-size: 15px; text-align: center;">Lama Proses</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @php
                                            $no = 1;
                                            $data = DB::table('tr_cmf')
                                                      ->join('tr_department_terkait', 'tr_cmf.kodeCMF', '=', 'tr_department_terkait.kodeCMF')
                                                      ->join('m_user', 'tr_cmf.pemilikProsesCMF', '=', 'm_user.nik')
                                                      ->join('m_area_terkait', 'tr_cmf.areaCMF', '=', 'm_area_terkait.kodeAreaTerkait')
                                                      ->where('kodeDepartmentCMF', $kodeDepartmentCMF)
                                                      ->get();
                                        @endphp
                                        @foreach ($data as $dt)
                                        <tr style="text-align: center">
                                          <td>{{ $no++ }}</td>
                                          <td>{{ $dt->nomorCMF }}</td>
                                          <td>{{ $dt->namaKaryawan }}</td>
                                          <td>{{ $dt->namaAreaTerkait }}</td>
                                          <td class="text-wrap">{{ $dt->judulPerubahanCMF }}</td>
                                          <td>{{ date('d-m-Y', strtotime($dt->dateApproveCMF7)) }}</td>
                                          <td>@php
                                              $date = new DateTime($dt->dateApproveCMF7);
                                              $today = new DateTime();
                                              $lamaProses = $today -> diff($date) -> format("%a");
                                              echo $lamaProses . ' Hari';
                                          @endphp</td>
                                        </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="navs-justified-step9" role="tabpanel">
                  <div class="card">
                    <h5 class="card-header">Step 9 : Verifikator, Document Control, MR</h5>
                    <div class="tab-content">
                      <table id="example9" class="display" style="width:100%">
                        <thead class="text-nowrap">
                          <tr>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">#</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Nomor CMF</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Pembuat CMF</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Department</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Proses Terakhir</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Tanggal Proses Terakhir</th>
                            <th style="font-weight: bold; font-size: 15px; text-align: center;">Lama Proses</th>
                          </tr>
                        </thead>
                        <tbody>
                          @php
                              $no = 1;
                          @endphp
                          @foreach ($cmf9 as $dt)
                          <tr style="text-align: center">
                            <td>{{ $no ++; }}</td>
                            <td>{{ $dt->nomorCMF; }}</td>
                            <td>{{ $dt->namaKaryawan; }}</td>
                            <td>{{ $dt->namaDepartment; }}</td>
                            <td>@if ($dt->statusProsesCMF == 10)
                                Tunggu Verifikasi Tim Food Safety
                                @elseif ($dt->statusProsesCMF == 11)
                                Tunggu Verifikasi Dokumen
                                @elseif ($dt->statusProsesCMF == 12)
                                Tunggu Persetujuan MR
                            @endif</td>
                            <td>{{ date('d-m-Y', strtotime($dt->dtmUpdatedDate)) }}</td>
                            <td>@php
                                $date = new DateTime($dt->dtmUpdatedDate);
                                $today = new DateTime();
                                $lamaProses = $today -> diff($date) -> format("%a");
                                echo $lamaProses . ' Hari';
                            @endphp</td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
</div>
    <!-- /Error -->

    <!-- / Content -->


    <!-- DATA TABLES ONLINE -->
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
    <!-- build:js assets/vendor/js/core.js -->
    <script src="<?php echo asset('cmf/') ?>/assets/vendor/libs/jquery/jquery.js"></script>
    <script src="<?php echo asset('cmf/') ?>/assets/vendor/libs/popper/popper.js"></script>
    <script src="<?php echo asset('cmf/') ?>/assets/vendor/js/bootstrap.js"></script>
    <script src="<?php echo asset('cmf/') ?>/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

    <script src="<?php echo asset('cmf/') ?>/assets/vendor/js/menu.js"></script>
    <!-- endbuild -->

    <!-- Vendors JS -->

    <!-- Main JS -->
    <script src="<?php echo asset('cmf/') ?>/assets/js/main.js"></script>

    <!-- Page JS -->

    <!-- Place this tag in your head or just before your close body tag. -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <script>new DataTable('#example');</script>
    <script>new DataTable('#example2');</script>
    <script>new DataTable('#example3');</script>
    <script>new DataTable('#example4');</script>
    <script>new DataTable('#example5');</script>
    <script>new DataTable('#example6');</script>
    <script>new DataTable('#example7');</script>
    <script>new DataTable('#example8');</script>
    <script>new DataTable('#example9');</script>
</body>
</html>
