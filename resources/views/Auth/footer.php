
<footer class="content-footer footer bg-footer-theme">
  <div class="container-xxl d-flex flex-wrap justify-content-between py-2 flex-md-row flex-column">
    <div class="mb-2 mb-md-0">
    </div>
    <div>
      <marquee style="float: right; margin-right: 50px;"><img src="<?php echo asset('img'); ?>/ios_logo.png" style="width: 20px; height: 20px;"> <strong> CMF ONLINE</strong> &copy; 2023 - <?php echo date("Y"); ?> (Created By : IOS System Development | Call Service (812 : Admin Document Controller) | "Pastikan diri selalu menjaga kesehatan dan selalu menerapkan protokol kesehatan di seluruh area Perusahaan. Budayakan selalu mencuci tangan menggunakan sabun, menggunakan masker dan menjaga jarak minimal 1,5 meter")  <img src="<?php echo asset('img'); ?>/ios_logo.png" style="width: 20px; height: 20px;"></marquee>
    </div>
  </div>
</footer>