@include('Auth.header')
@include($role.'/navigation')
@include('Auth.topBar')

      <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
          <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Dashboard / <a href="<?php echo route('Dc/User'); ?>" class="text-muted fw-light">User</a> /</span> Edit</h4>

          <div class="row">
            <div class="col-md-12">
              <ul class="nav nav-pills flex-column flex-md-row mb-3">
                <li class="nav-item">
                  <a class="nav-link active" href="javascript:void(0);"><i class="bx bx-user me-1"></i> Inputkan data profile yang akan Anda input sesuai dengan kaidah dan aturan yang sudah ditetapkan !</a>
                </li>
              </ul>
              <div class="card mb-4">
                <h5 class="card-header">Profile Details</h5>
                  <form method="POST" action="<?php echo route('Query/updateUser'); ?>" enctype="multipart/form-data">
                   @csrf
                  <!-- Account -->
                  <div class="card-body">
                    <div class="d-flex align-items-start align-items-sm-center gap-4">
                      <img
                        src="<?php echo asset('img') ?>/profile/<?php echo $dt->gambarUser ?>"
                        alt="user-avatar"
                        class="d-block rounded"
                        height="100"
                        width="100"
                        id="uploadedAvatar"
                      />
                      <div class="button-wrapper">
                        <label for="upload" class="btn btn-primary me-2 mb-4" tabindex="0">
                          <span class="d-none d-sm-block">Upload new photo</span>
                          <i class="bx bx-upload d-block d-sm-none"></i>
                          <input
                            type="file"
                            id="upload"
                            name="gambarUser"
                            class="account-file-input"
                            hidden
                            accept="image/png, image/jpeg"
                          />
                        </label>
                        <button type="button" class="btn btn-outline-secondary account-image-reset mb-4">
                          <i class="bx bx-reset d-block d-sm-none"></i>
                          <span class="d-none d-sm-block">Reset</span>
                        </button>

                        <p class="text-muted mb-0">Allowed JPG, JPEG or PNG. </p>
                      </div>
                    </div>
                  </div>
                  <hr class="my-0" />
                  <div class="card-body">
                    <div class="row">
                      <div class="mb-3 col-md-6">
                        <label for="firstName" class="form-label">NIK</label>
                        <input
                          class="form-control"
                          type="text"
                          name="nik"
                          placeholder="NIK"
                          required
                          value="{{ $dt->nik }}"
                        />
                      </div>
                      <div class="mb-3 col-md-6">
                        <label for="lastName" class="form-label">Nama Karyawan</label>
                        <input class="form-control" type="text" required name="namaKaryawan" value="<?php echo $dt->namaKaryawan; ?>" placeholder="Nama Lengkap Karyawan" />
                      </div>
                      <div class="mb-3 col-md-6">
                        <label for="lastName" class="form-label">Nama Inisial</label>
                        <input class="form-control" type="text" value="<?php echo $dt->namaPanggilan; ?>" required name="namaPanggilan" placeholder="Nama Inisial" />
                      </div>
                      <div class="mb-3 col-md-6">
                        <label for="organization" class="form-label">Job Title</label>
                        <input
                          type="text"
                          class="form-control"
                          name="posisi"
                          placeholder="Job Title"
                          required
                          value="<?php echo $dt->posisi; ?>"
                        />
                      </div>
                      <div class="mb-3 col-md-6">
                        <label class="form-label" for="country">Department</label>
                        <select name="department" class="select2 form-select">
                           <option value="<?php echo $dt->department; ?>"><?php echo $dt->namaDepartment; ?></option>
                          <?php 
                            foreach($allDepAktif as $dt3){
                           ?>
                          <option value="<?php echo $dt3->kodeDepartmentMaster; ?>"><?php echo $dt3->namaDepartment; ?></option>
                         <?php } ?>
                        </select>
                      </div>
                      <div class="mb-3 col-md-6">
                        <label class="form-label" for="country">Area</label>
                        <select name="area" class="select2 form-select">
                          <option value="<?php echo $dt->area; ?>"><?php echo $dt->namaAreaTerkait; ?></option>
                          <?php 
                            foreach($allAreaAktif2 as $dt4){
                           ?>
                          <option value="<?php echo $dt4->kodeAreaTerkait; ?>"><?php echo $dt4->namaAreaTerkait; ?></option>
                         <?php } ?>
                        </select>
                      </div>
                      <div class="mb-3 col-md-6">
                        <label for="email" class="form-label">E-mail</label>
                        <input
                          class="form-control"
                          type="text"
                          name="email"
                          placeholder="Email Kantor"
                          required
                          value="<?php echo $dt->email; ?>"
                        />
                      </div>
                      <div class="mb-3 col-md-6">
                        <label class="form-label" for="country">Role Akses</label>
                        <select name="levelCMF" class="select2 form-select">
                          <option value="<?php echo $dt->levelCMF; ?>"><?php echo $dt->namaLevel; ?></option>
                          <?php 
                            foreach($allLevel as $dt2){
                           ?>
                          <option value="<?php echo $dt2->kodeLevel; ?>"><?php echo $dt2->namaLevel; ?></option>
                         <?php } ?>
                        </select>
                      </div>
                      <div class="mb-3 col-md-6">
                        <label class="form-label" for="country">Password</label>
                        <input
                          class="form-control"
                          type="text"
                          name="password"
                          placeholder="Password"
                        />
                      </div>
                      <div class="mb-3 col-md-6">
                        <label class="form-label" for="country">Username Komputer</label>
                        <input
                          class="form-control"
                          type="text"
                          name="username"
                          placeholder="Username Login Komputer"
                          required
                          value="<?php echo $dt->username; ?>"
                        />
                      </div>
                      <div class="mb-3 col-md-6">
                        <label class="form-label" for="country">Status</label>
                        <select name="bitActive" class="select2 form-select">
                           <option value="<?php echo $dt->bitActive; ?>">
                             <?php 
                              if($dt->bitActive == 1){
                                echo "AKTIF";
                              }else{
                                echo "NON AKTIF";
                              }
                              ?>
                           </option>
                          <option value="1">AKTIF</option>
                          <option value="0">NON AKTIF</option>
                        </select>
                      </div>
                    </div>
                    <div class="mt-2">
                      <button type="submit" class="btn btn-primary me-2">Save changes</button>
                      <button type="reset" class="btn btn-outline-secondary">Cancel</button>
                      <input type="hidden" name="feedBack" value="Dc/User">
                      <input type="hidden" name="gambarUserReject" value="<?php echo $dt->gambarUser; ?>">
                      <input type="hidden" name="kodeUser" value="<?php echo $dt->kodeUser; ?>">
                    </div>
                  </div>
                <!-- /Account -->
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- / Content -->

        <!-- Footer -->
        @include('Auth/footer')
        
        <!-- / Footer -->

        <div class="content-backdrop fade"></div>
      </div>

<!-- Content wrapper -->
    </div>
    <!-- / Layout page -->
  </div>

  <!-- Overlay -->
  <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->


<!-- Core JS -->
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/jquery/jquery.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/popper/popper.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/js/bootstrap.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/js/menu.js"></script>
<!-- endbuild -->

<!-- Vendors JS -->

<!-- Main JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/main.js"></script>

<!-- Page JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/pages-account-settings-account.js"></script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
</body>
</html>
