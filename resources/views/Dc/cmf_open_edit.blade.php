@include('Auth.header')
@include($role.'/navigation')
@include('Auth.topBar')

      <!-- Content wrapper -->
      <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
          <div class="row">
            <!-- KONTEN -->
            <!-- Pills -->
            <div class="container-xxl flex-grow-1 container-p-y">
              <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Dashboard / </a></span> Open CMF
              </h4>
              <div class="row">
                <div class="nav-align-top mb-4">
                    <div class="tab-content">
                      <div class="tab-pane fade show active" id="navs-pills-justified-home" role="tabpanel">
                        <form method="POST" action="{{ route('Query/EditCMF') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                              <div class="row">
                                <h4 class="card-header" style="text-align: left; margin-top: -30px;">Data CMF</h4><hr>
                                <div class="mb-3 col-md-2">
                                  <label for="firstName" class="form-label">Department <small style="color: red;">*)</small></label>
                                  <select name="departmentCMF" class="form-control">
                                      <option value="{{ $dt->departmentCMF }}">{{ $dt->namaDepartment }}</option>
                                      @foreach($department as $dp)
                                      <option value="{{ $dp->kodeDepartmentMaster }}">{{ $dp->namaDepartment }}</option>
                                      @endforeach
                                  </select>
                                </div>
                                <div class="mb-3 col-md-2">
                                  <label for="firstName" class="form-label">Area <small style="color: red;">*)</small></label>
                                  <select name="areaCMF" class="form-control">
                                      <option value="{{ $dt->areaCMF }}">{{ $dt->namaAreaTerkait }}</option>
                                      @foreach($area as $ar)
                                      <option value="{{ $ar->kodeAreaTerkait }}">{{ $ar->namaAreaTerkait }}</option>
                                      @endforeach
                                  </select>
                                </div>
                                <div class="mb-3 col-md-3">
                                  <label for="lastName" class="form-label">Pemilik Proses <small style="color: red;">*)</small></label>
                                  <select name="pemilikProsesCMF" class="form-control">
                                      <option value="{{ $dt->pemilikProsesCMF }}">{{ $dt->namaKaryawan }}</option>
                                      @foreach($namaKaryawan as $ds)
                                      <option value="{{ $ds->nik }}">{{ $ds->namaKaryawan }}</option>
                                      @endforeach
                                  </select>
                                </div>
                                <div class="mb-3 col-md-3">
                                  <label for="lastName" class="form-label">Nomor Capex </label>
                                  <input class="form-control" type="text" name="nomorCapex"  value="{{ $dt->nomorCapex }}" placeholder="Nomor Capex" />
                                </div>
                                <div class="mb-3 col-md-2">
                                  <label for="lastName" class="form-label">Tanggal Pengajuan <small style="color: red;">*)</small></label>
                                  <input class="form-control" type="date" name="dateCMF" id="dateCMF" required="required" value="{{ $dt->dateCMF }}" placeholder="Tanggal Pengajuan" />
                                </div>
                                <div class="mb-3 col-md-12">
                                  <label for="organization" class="form-label">Judul Perubahan <small style="color: red;">*)</small></label>
                                  <input type="text" class="form-control" value="{{ $dt->judulPerubahanCMF }}" name="judulPerubahanCMF" placeholder="Judul Perubahan" required="required" />
                                </div>
                                <div class="mb-3 col-md-12">
                                  <label class="form-label" for="country">Perubahan <small style="color: red;">*)</small></label>
                                  <div class="table-responsive">
                                    <table class="table table-striped table-borderless ">
                                      <tbody>
                                        <td>
                                          <input class="form-check-input" name="bitMesin" @php if ($dt->bitMesin == 1) {print 'checked';} @endphp type="checkbox" value="1" />
                                          <label class="form-check-label" for="defaultCheck3"> Mesin </label>
                                        </td>
                                        <td>
                                          <input class="form-check-input" name="bitProses" @php if ($dt->bitProses == 1) {print 'checked';} @endphp  type="checkbox" value="1" />
                                          <label class="form-check-label" for="defaultCheck3"> Proses </label>
                                        </td>
                                        <td>
                                          <input class="form-check-input" name="bitSystem" @php if ($dt->bitSystem == 1) {print 'checked';} @endphp  type="checkbox" value="1" />
                                          <label class="form-check-label" for="defaultCheck3"> System </label>
                                        </td>
                                        <td>
                                          <input class="form-check-input" type="checkbox" @php if ($dt->bitLainnya !== '') {print 'checked';} @endphp  name="bitLainnya" value="1" id="lainnyaPerubahan" onclick="lainnya()" />
                                          <label class="form-check-label" for="defaultCheck3"> Lainnya </label>
                                        </td>
                                        <td>
                                          <input class="form-control" type="@php if($dt->bitLainnya == ''){print 'hidden';}else{print 'text';} @endphp " value="{{ $dt->bitLainnya }}"  id="valuePerubahanCMF" placeholder="Lainnya ...." name="valueLainnya" />
                                        </td>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                                <div class="mb-3 col-md-12">
                                  <label class="form-label" for="country">Jenis Perubahan <small style="color: red;">*)</small></label>
                                  <div class="table-responsive">
                                    <table class="table table-striped table-borderless ">
                                      <tbody>
                                        <tr>
                                          @php
                                              $no = 0;
                                          @endphp
                                          @foreach ($jenisPerubahan as $jns)
                                          
                                          @if ($no == 4 || $no == 8 || $no == 12 || $no == 16 || $no == 20)
                                        </tr>
                                        <tr>
                                          @endif
                                          <td>
                                            <input class="form-check-input" type="checkbox" @php if ($jns->valueJenisPerubahan == 1) {print 'checked';} @endphp name="@php if($jns->kodeJenisCMF == ''){print 'bitLainnya2';}else{echo $jns->kodeJenisCMF;} @endphp" @php if ($jns->namaJenisPerubahanCMF == 'Lainnya') {print 'id="lainnyaJenisPerubahan" onclick="lainnya2()"';} @endphp value="1" />
                                            <label class="form-check-label" for="defaultCheck3"> {{ $jns->namaJenisPerubahanCMF }} </label>
                                          </td>
                                          @php
                                              $no++
                                          @endphp
                                          @endforeach
                                          <td>
                                            <input class="form-control" type="@php if ($jenisPerubahanLainnya){if($jenisPerubahanLainnya->valueJenisPerubahan == 0){print 'hidden';}else{print 'text';}}else{print 'hidden';} @endphp" value="@php if ($jenisPerubahanLainnya){print $jenisPerubahanLainnya->lainnya;} @endphp"  id="valueJenisPerubahanCMF" placeholder="Lainnya ...." name="valueLainnya2" />
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                                <div class="mb-3 col-md-6">
                                  <label for="lastName" class="form-label">Target Implementasi <small style="color: red;">*)</small></label>
                                  <input class="form-control" type="date" required="required" name="dateImplementasiCMF" value="{{ $dt->dateImplementasiCMF }}"  />
                                </div>
                                <div class="mb-3 col-md-6">
                                  <label for="lastName" class="form-label">Type Perubahan <small style="color: red;">*)</small></label>
                                  <div class="table-responsive">
                                    <table class="table table-striped table-borderless ">
                                      <tbody>
                                        <td>
                                          <div class="form-check">
                                            <input name="typePerubahanCMF" required class="form-check-input" type="radio" <?php if($dt->typePerubahanCMF == 1){ print 'checked'; } ?> id="defaultRadio1" value="1">
                                            <label class="form-check-label" for="defaultRadio1"> Temporary </label>
                                          </div>
                                        </td>
                                        <td>
                                          <div class="form-check">
                                            <input name="typePerubahanCMF" required class="form-check-input" type="radio" <?php if($dt->typePerubahanCMF == 2){ print 'checked'; } ?> id="defaultRadio2" value="2">
                                            <label class="form-check-label" for="defaultRadio2"> Permanent </label>
                                          </div>
                                        </td>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                                <div class="mb-3 col-md-12">
                                  <label for="email" class="form-label">Alasan Perubahan <small style="color: red;">*)</small></label>
                                  <textarea class="form-control" required="required" name="alasanPerubahanCMF" placeholder="Latar Belakang & Tujuan" >{{ $dt->alasanPerubahanCMF }}</textarea>
                                </div>
                                <div class="mb-3 col-md-12">
                                  <label for="email" class="form-label">Dampak Terhadap Perubahan <small style="color: red;">*)</small></label>
                                  <textarea class="form-control" required="required" name="dampakPerubahanCMF" placeholder="Quality, Biaya, Food Safety, Keselamatan & Kesehatan Lingkungan atau Regulasi" >{{ $dt->dampakPerubahanCMF }}</textarea>
                                </div>
                                <div class="mb-3 col-md-12">
                                  <label for="email" class="form-label">Deskripsi Perubahan <small style="color: red;">*)</small></label>
                                  <textarea class="form-control" required="required" name="deskripsiPerubahanCMF" placeholder="Keterangan Perubahan Produk" >{{ $dt->deskripsiPerubahanCMF }}</textarea>
                                </div>
                                <div class="mb-3 col-md-12">
                                  <label for="email" class="form-label">Area Terkait <small style="color: red;">*)</small></label>
                                </div>
                                @foreach ($area_terkait as $dd)
                                <div class="mb-3 col-md-2">
                                  <input class="form-check-input" type="checkbox" @php if($dd->valueAreaCMF == 1){print 'checked';} @endphp name="{{ $dd->kodeAreaCMF }}" value="1" />
                                  <label class="form-check-label" for="defaultCheck3"> {{ $dd->namaAreaCMF }} </label>
                                </div>
                                @endforeach
                                <h4 class="card-header" style="text-align: left;">Risk Assesment</h4><hr>
                                <div class="mb-3 col-md-6">
                                  <label for="email" class="form-label">Resiko <small style="color: red;">*)</small></label>
                                  <textarea class="form-control" name="resikoCMF" required="required" placeholder="Resiko Dilakukannya Perubahan ..." >{{ $dt->resikoCMF }}</textarea>
                                </div>
                                <div class="mb-3 col-md-6">
                                  <label for="email" class="form-label">Rencana Mitigasi <small style="color: red;">*)</small></label>
                                  <textarea class="form-control" name="mitigasiCMF" required="required" placeholder="Mitigasi Pencegahan dari Resiko ....." >{{ $dt->mitigasiCMF }}</textarea>
                                </div>
                                <div class="mb-3 col-md-6">
                                  <label for="email" class="form-label">PIC <small style="color: red;">*)</small></label>
                                  <input type="text" name="picRiskAssesmentCMF" required="required" value="{{ $dt->picRiskAssesmentCMF }}" class="form-control" placeholder="PIC Mitigasi">
                                </div>
                                <div class="mb-3 col-md-6">
                                  <label for="email" class="form-label">Dead Line <small style="color: red;">*)</small></label>
                                  <input type="date" name="deadLineCMF" required="required" value="{{ $dt->deadLineCMF }}" class="form-control" >
                                </div>
                                <h4 class="card-header" style="text-align: left;">Upload Dokumen</h4><hr>
                                <div class="mb-3 col-md-6">
                                  <label for="email" class="form-label">Upload Dokumen  </label>
                                  <input type="file" name="txtUploadDocument" class="form-control" accept=".pdf" >
                                  <small style="color: red; "> *) Sistem hanya menerima dokumen dengan ekstensi .pdf</small>
                                </div>
                                <?php 
                                if($dt->txtUploadDocument !== ""){ 
                                    $tahun = date('Y', strtotime($dt->dateCMF));
                                    $department = $dt->namaDepartment
                                ?>
                                <div class="mb-3 col-md-4">
                                    <a href="<?php echo asset('upload').'/documents/'.$tahun.'/'.$department.'/'.$dt->txtUploadDocument ?>" target="_blank" data-bs-toggle="tooltip" data-bs-offset="0,4" data-bs-placement="top" data-bs-html="true" title="" data-bs-original-title="<i class='bx bx-bookmarks bx-xs' ></i> <span>Buka Lampiran</span>" style="margin-top: 25px;" class="btn rounded-pill btn-danger" class="btn rounded-pill btn-icon btn-danger">
                                        <span class="tf-icons bx bx-bookmarks"></span> Buka Lampiran
                                    </a>
                                  </div>
                                <?php } ?>
                                {{-- DEPT HEAD PEMILIK PROSES --}}
                                <h4 class="card-header" style="text-align: left;">Persetujuan Dept Head Pemilik Proses</h4><hr>
                                <div class="mb-3 col-md-3">
                                    <label for="email" class="form-label">CMF Telah Disetujui Oleh : </label>
                                    <select name="approveCMF1" class="form-control">
                                        <option value="{{ $dt->approveCMF1 }}">{{ $dt->approveCMF1 }}</option>
                                        @foreach($namaKaryawan as $ds)
                                        <option value="{{ $ds->namaKaryawan }}">{{ $ds->namaKaryawan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-3 col-md-3">
                                    <label for="email" class="form-label">Tanggal Disetujui : </label>
                                    <input type="date" class="form-control" name="dateApproveCMF1" value="<?php echo date('Y-m-d', strtotime($dt->dateApproveCMF1)); ?>">
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="email" class="form-label">Catatan Dept Head </label>
                                    <textarea class="form-control" name="catatanDeptheadCMF1" placeholder="Catatan Dept Head ....." ><?php echo $dt->catatanDeptheadCMF1 ?></textarea>
                                </div>
                                <h4 class="card-header" style="text-align: left;">Review Departemen Terkait</h4><hr>
                                <?php foreach($review as $rev){  ?>
                                <div class="mb-3 col-md-2">
                                    <label for="email" class="form-label">Direview Oleh : </label>
                                    <select name="{{ 'approvedBy1'.$rev->kodeDepartmentTerkaitCMF }}" class="form-control">
                                        <option value="{{ $rev->approvedBy1 }}">{{ $rev->approvedBy1 }}</option>
                                        @foreach($namaKaryawan as $ds)
                                        <option value="{{ $ds->namaKaryawan }}">{{ $ds->namaKaryawan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-3 col-md-2">
                                    <label for="email" class="form-label">Department : </label>
                                    <input type="text" disabled class="form-control" name="" value="<?php echo $rev->namaDepartment; ?>">
                                </div>
                                <div class="mb-3 col-md-2">
                                    <label for="email" class="form-label">Tanggal : </label>
                                    <input type="date" class="form-control" name="{{ 'dateReviewCMF'.$rev->kodeDepartmentTerkaitCMF }}" value="<?php echo date('Y-m-d', strtotime($rev->dateReviewCMF)); ?>">
                                </div>
                                <div class="mb-3 col-md-2">
                                    <label for="email" class="form-label">Hasil Review : </label><br>
                                    <select name="{{ 'adjustmentDepartment1CMF'.$rev->kodeDepartmentTerkaitCMF }}" class="form-control" >
                                        <option value="{{ $rev->adjustmentDepartment1CMF }}">{{ $rev->adjustmentDepartment1CMF }}</option>
                                        <option value="0">0. BELUM DIREVIEW</option>
                                        <option value="1">1. SETUJU</option>
                                        <option value="2">2. TIDAK SETUJU</option>
                                    </select>
                                </div>
                                <div class="mb-3 col-md-4">
                                    <label for="email" class="form-label">Review Dept Head </label>
                                    <textarea class="form-control" name="{{ 'reviewDepartmentCMF'.$rev->kodeDepartmentTerkaitCMF }}" placeholder="Review Dept Head ....." ><?php echo $rev->reviewDepartmentCMF ?></textarea>
                                </div>
                                <input type="hidden" name="{{ $rev->kodeDepartmentTerkaitCMF }}" value="{{ $rev->kodeDepartmentTerkaitCMF }}">
                                <?php } ?>
                                <h4 class="card-header" style="text-align: left;">Mengetahui</h4><hr>
                                <div class="mb-3 col-md-6">
                                <label for="email" class="form-label">System : </label>
                                <select name="approveCMF2" class="form-control">
                                    <option value="{{ $dt->approveCMF2 }}">{{ $dt->approveCMF2 }}</option>
                                    @foreach($namaKaryawan as $ds)
                                    <option value="{{ $ds->namaKaryawan }}">{{ $ds->namaKaryawan }}</option>
                                    @endforeach
                                </select>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="email" class="form-label">Tanggal : </label>
                                    <input type="date" class="form-control" name="dateApproveCMF2" value="<?php echo date('Y-m-d', strtotime($dt->dateApproveCMF2)); ?>">
                                </div>
                                <div class="mb-3 col-md-6">
                                <label for="email" class="form-label">MR : </label>
                                <select name="approveCMF3" class="form-control">
                                    <option value="{{ $dt->approveCMF3 }}">{{ $dt->approveCMF3 }}</option>
                                    @foreach($namaKaryawan as $ds)
                                    <option value="{{ $ds->namaKaryawan }}">{{ $ds->namaKaryawan }}</option>
                                    @endforeach
                                </select>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="email" class="form-label">Tanggal : </label>
                                    <input type="date" class="form-control" name="dateApproveCMF3" value="<?php echo date('Y-m-d', strtotime($dt->dateApproveCMF3)); ?>">
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="email" class="form-label">Manufacturing : </label>
                                    <select name="approveCMF4" class="form-control">
                                        <option value="{{ $dt->approveCMF4 }}">{{ $dt->approveCMF4 }}</option>
                                        @foreach($namaKaryawan as $ds)
                                        <option value="{{ $ds->namaKaryawan }}">{{ $ds->namaKaryawan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="email" class="form-label">Tanggal : </label>
                                    <input type="date" class="form-control" name="dateApproveCMF4" value="<?php echo date('Y-m-d', strtotime($dt->dateApproveCMF4)); ?>">
                                </div>
                                <h4 class="card-header" style="text-align: left;">Review Setelah Dilakukan Perubahan</h4><hr>
                                <div class="mb-3 col-md-4">
                                <label for="email" class="form-label">Pemilik Proses : </label>
                                <select name="approveCMF6" class="form-control">
                                    <option value="{{ $dt->approveCMF6 }}">{{ $dt->approveCMF6 }}</option>
                                    @foreach($namaKaryawan as $ds)
                                    <option value="{{ $ds->namaKaryawan }}">{{ $ds->namaKaryawan }}</option>
                                    @endforeach
                                </select>
                                </div>
                                <div class="mb-3 col-md-4">
                                    <label for="email" class="form-label">Tanggal : </label>
                                    <input type="date" class="form-control" name="dateApproveCMF6" value="<?php echo date('Y-m-d', strtotime($dt->dateApproveCMF6)); ?>">
                                </div>
                                <div class="mb-3 col-md-4">
                                    <label for="email" class="form-label">Detail Aktifitas : </label>
                                    <textarea class="form-control" name="detailAktifitasCMF"  placeholder="Detail Aktifitas : (tanggal installasi, tanggal trial, tanggal implementasi) ... " ><?php echo $dt->detailAktifitasCMF ?></textarea>
                                </div>
                                <div class="mb-3 col-md-9">
                                <label for="email" class="form-label">Lampiran Dokumen Commissioning </label>
                                <input type="file" name="txtUploadCommissioning" class="form-control" accept=".pdf" >
                                <small style="color: red; "> *) Sistem hanya menerima dokumen dengan ekstensi .pdf</small>
                                </div>
                                @if($dt->txtUploadCommissioning !== "")
                                @php
                                    $tahun = date('Y', strtotime($dt->dateCMF));
                                    $department = $dt->namaDepartment
                                @endphp
                                <div class="mb-3 col-md-3">
                                    <a href="<?php echo asset('upload').'/commissioning/'.$tahun.'/'.$department.'/'.$dt->txtUploadCommissioning ?>" target="_blank" data-bs-toggle="tooltip" data-bs-offset="0,4" data-bs-placement="top" data-bs-html="true" title="" data-bs-original-title="<i class='bx bx-bookmarks bx-xs' ></i> <span>Buka Lampiran</span>" style="margin-top: 25px;" class="btn rounded-pill btn-danger" class="btn rounded-pill btn-icon btn-danger">
                                        <span class="tf-icons bx bx-bookmarks"></span> Buka Lampiran
                                    </a>
                                </div>
                                @endif
                                <div class="mb-3 col-md-4">
                                    <label for="email" class="form-label">Dept Head Pemilik Proses : </label>
                                    <select name="approveCMF7" class="form-control">
                                        <option value="{{ $dt->approveCMF7 }}">{{ $dt->approveCMF7 }}</option>
                                        @foreach($namaKaryawan as $ds)
                                        <option value="{{ $ds->namaKaryawan }}">{{ $ds->namaKaryawan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-3 col-md-4">
                                    <label for="email" class="form-label">Tanggal : </label>
                                    <input type="date" class="form-control" name="dateApproveCMF7" value="<?php echo date('Y-m-d', strtotime($dt->dateApproveCMF7)); ?>">
                                </div>
                                <div class="mb-3 col-md-4">
                                    <label for="email" class="form-label">Catatan Dept Head : </label>
                                    <textarea class="form-control" name="catatanDeptheadCMF2" placeholder="Catatan Dept Head ... " ><?php echo $dt->catatanDeptheadCMF2 ?></textarea>
                                </div>
                                <h4 class="card-header" style="text-align: left;">Evaluasi & Verifikasi Departemen Terkait</h4><hr>
                                <?php foreach($evaluasi as $eva){  ?>
                                <div class="mb-3 col-md-2">
                                    <label for="email" class="form-label">Dievaluasi Oleh : </label>
                                    <select name="{{ 'approvedBy2'.$eva->kodeDepartmentTerkaitCMF }}" class="form-control">
                                        <option value="{{ $eva->approvedBy2 }}">{{ $eva->approvedBy2 }}</option>
                                        @foreach($namaKaryawan as $ds)
                                        <option value="{{ $ds->namaKaryawan }}">{{ $ds->namaKaryawan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-3 col-md-2">
                                  <label for="email" class="form-label">Department : </label>
                                  <input type="text" disabled class="form-control" name="" value="<?php echo $eva->namaDepartment; ?>">
                              </div>
                                <div class="mb-3 col-md-2">
                                    <label for="email" class="form-label">Tanggal : </label>
                                    <input type="date" class="form-control" name="{{ 'dateEvaluasiCMF'.$eva->kodeDepartmentTerkaitCMF }}" value="<?php echo date('Y-m-d', strtotime($eva->dateEvaluasiCMF)); ?>">
                                </div>
                                <div class="mb-3 col-md-2">
                                    <label for="email" class="form-label">Hasil Evaluasi : </label><br>
                                    <select name="{{ 'adjustmentDepartment2CMF'.$eva->kodeDepartmentTerkaitCMF }}" class="form-control" >
                                        <option value="{{ $eva->adjustmentDepartment2CMF }}">{{ $eva->adjustmentDepartment2CMF }}</option>
                                        <option value="0">0. BELUM DIREVIEW</option>
                                        <option value="1">1. SETUJU</option>
                                        <option value="-1">2. TIDAK SETUJU</option>
                                    </select>
                                </div>
                                <div class="mb-3 col-md-4">
                                    <label for="email" class="form-label">Evaluasi Dept Head </label>
                                    <textarea class="form-control" name="{{ 'evaluasiDepartmentCMF'.$eva->kodeDepartmentTerkaitCMF }}" placeholder="Evaluasi Dept Head ....." ><?php echo $eva->evaluasiDepartmentCMF ?></textarea>
                                </div>
                                <?php } ?>
                                <h4 class="card-header" style="text-align: left;">Verifikasi Hasil Perubahan</h4><hr>
                                <div class="mb-3 col-md-6">
                                    <label for="email" class="form-label">Food Safety Team : </label>
                                    <select name="approveCMF8" class="form-control">
                                        <option value="{{ $dt->approveCMF8 }}">{{ $dt->approveCMF8 }}</option>
                                        @foreach($namaKaryawan as $ds)
                                        <option value="{{ $ds->namaKaryawan }}">{{ $ds->namaKaryawan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="email" class="form-label">Tanggal : </label>
                                    <input type="date" class="form-control" name="dateApproveCMF8" value="<?php echo date('Y-m-d', strtotime($dt->dateApproveCMF8)); ?>">
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="email" class="form-label">Document Controller : </label>
                                    <select name="approveCMF9" class="form-control">
                                        <option value="{{ $dt->approveCMF9 }}">{{ $dt->approveCMF9 }}</option>
                                        @foreach($namaKaryawan as $ds)
                                        <option value="{{ $ds->namaKaryawan }}">{{ $ds->namaKaryawan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="email" class="form-label">Tanggal : </label>
                                    <input type="date" class="form-control" name="dateApproveCMF9" value="<?php echo date('Y-m-d', strtotime($dt->dateApproveCMF9)); ?>">
                                </div>
                            <div class="mb-3 col-md-12">
                              <label for="email" class="form-label">Dokumen Perlu Direvisi Dampak Perubahan : </label>
                              <table class="table">
                              <thead class="text-nowrap">
                                <tr>
                                  <th>No. </th>
                                  <th style="text-align: center;">Post</th>
                                  <th style="text-align: center;">Applicable / Not</th>
                                  <th style="text-align: center;">Detail Document</th>
                                </tr>
                              </thead>
                              <tbody>
                                @if ($dokumen_count > 0)
                                <?php 
                                  $no = 1;
                                  foreach($dokumen as $dk){
                                ?>
                                <tr>
                                  <td><?php echo $no++; ?></td>
                                  <td><?php echo $dk->postDokumenCMF; ?></td>
                                  <td style="text-align: center;"><input class="form-check-input" type="checkbox" value="1" name="<?php echo $dk->postDokumenCMF ?>" <?php if($dk->valueDokumenCMF == 1){ print 'checked'; } ?> /></td>
                                  <td><textarea class="form-control" name="<?php echo $dk->kodeDokumenCMF ?>" /><?php echo $dk->perubahanDokumenCMF; ?></textarea></td>
                                </tr>
                                <?php } ?>
                                @else
                                <?php 
                                  $no = 1;
                                  foreach($dokumen_master as $dk){
                                ?>
                                <tr>
                                  <td><?php echo $no++; ?></td>
                                  <td><?php echo $dk->postDokumen; ?></td>
                                <td style="text-align: center;"><input class="form-check-input" type="checkbox" name="<?php echo $dk->postDokumen ?>" value="1" /></td>
                                <td><textarea class="form-control" name="<?php echo $dk->kodeDokumen ?>"/></textarea></td>
                                </tr>
                                <?php } ?>
                                @endif
                              </tbody>
                            </table>
                            </div>
                            <h4 class="card-header" style="text-align: left;">Mengetahui</h4><hr>
                            <div class="mb-3 col-md-6">
                                <label for="email" class="form-label">MR : </label>
                                <select name="approveCMF10" class="form-control">
                                    <option value="{{ $dt->approveCMF10 }}">{{ $dt->approveCMF10 }}</option>
                                    @foreach($namaKaryawan as $ds)
                                    <option value="{{ $ds->namaKaryawan }}">{{ $ds->namaKaryawan }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3 col-md-6">
                                <label for="email" class="form-label">Tanggal : </label>
                                <input type="date" class="form-control" name="dateApproveCMF10" value="<?php echo date('Y-m-d', strtotime($dt->dateApproveCMF10)); ?>">
                            </div>
                            <h4 class="card-header" style="text-align: left;">Data Hidden</h4><hr>
                            <div class="mb-3 col-md-4">
                                <label for="email" class="form-label">PIC NOK CMF : </label>
                                <select name="picNOKCMF" class="form-control">
                                    <option value="{{ $dt->picNOKCMF }}">{{ $dt->picNOKCMF }}</option>
                                    @foreach($namaKaryawan as $ds)
                                    <option value="{{ $ds->namaKaryawan }}">{{ $ds->namaKaryawan }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3 col-md-4">
                                <label for="email" class="form-label">Tanggal : </label>
                                <input type="date" class="form-control" name="dateNOKCMF" value="<?php echo date('Y-m-d', strtotime($dt->dateNOKCMF)); ?>">
                            </div>
                            <div class="mb-3 col-md-4">
                                <label for="email" class="form-label">Point Koreksi : </label>
                                <textarea class="form-control" name="comentNOKCMF" placeholder="Point Koreksi ....." ><?php echo $dt->comentNOKCMF ?></textarea>
                            </div>
                            <div class="mb-3 col-md-6">
                                <label for="email" class="form-label">Nomor CMF : </label>
                                <input type="text" class="form-control" name="nomorCMF" value="<?php echo $dt->nomorCMF; ?>">
                            </div>
                            <div class="mb-3 col-md-6">
                                <label for="email" class="form-label">Status Proses CMF : </label>
                                <select name="statusProsesCMF" class="form-control">
                                    <option value="{{ $dt->statusProsesCMF }}">{{ $dt->statusProsesCMF }}</option>
                                    <option value="1">1. Tunggu Persetujuan Atasan</option>
                                    <option value="1.5">1.5 Tunggu Approve DC</option>
                                    <option value="2">2. Sudah Approve DC, Tunggu Review Dept Head Area Terkait</option>
                                    <option value="3">3. Sudah Diriview, Tunggu Approve Spv. System</option>
                                    <option value="4">4. Tunggu Approve MR</option>
                                    <option value="5">5. Tunggu Approve Mnf. Head</option>
                                    <option value="6">6. Tunggu Perubahan Selesai / Approve DC</option>
                                    <option value="7">7. Pengisian Detail Aktifitas Oleh Pemilik Proses</option>
                                    <option value="8">8. Pengecekan Data Commissioning Oleh Atasan</option>
                                    <option value="9">9. Tunggu Evaluasi Oleh Dept Head Terkait</option>
                                    <option value="10">10. Tunggu Verifikasi Oleh Tim Food Safety</option>
                                    <option value="11">11. Tunggu Verifikasi Dokumen</option>
                                    <option value="12">12. Tunggu Approve MR</option>
                                    <option value="13">13. CMF Closed</option>
                                    <option value="14">14. CMF Canceled</option>
                                    <option value="-1">-1. Dikoreksi Atasan</option>
                                    <option value="-2">-2. Dikoreksi DC</option>
                                    <option value="-3">-3. CMF Tidak Disetujui Oleh Salah Satu Dept Head</option>
                                    <option value="-4">-4. Dikoreksi Dept Head Area Terkait</option>
                                    <option value="-5">-5. Dikoreksi Spv. System</option>
                                    <option value="-6">-6. Dikoreksi MR</option>
                                    <option value="-7">-7. Dikoreksi Mng. Head</option>
                                    <option value="-8">-8. Dikoreksi DC Setelah Review</option>
                                    <option value="-9">-9. Dikoreksi Atasan Setelah Perubahan</option>
                                    <option value="-10">-10. Dikoreksi Dept Head Area Terkait Saat Evaluasi</option>
                                    <option value="-11">-11. Dikoreksi Verifikasi Food Safety</option>
                                    <option value="-12">-12. Dikoreksi Saat Verifikasi Dokumen</option>
                                    <option value="-13">-13. Dikoreksi MR Setelah Evaluasi</option>
                                </select>
                            </div>
                            <br><br>
                            <div class="mt-5">
                                <button type="submit" class="btn btn-lg btn-primary me-2" style="float: right;"><i class="menu-icon tf-icons bx bx-check"></i>Simpan</button>
                                <input type="hidden" name="feedBack" value="{{ route($role.'/home') }}"/>
                                <input type="hidden" name="txtUploadDocumentReject" value="{{ $dt->txtUploadDocument }}">
                                <input type="hidden" name="txtUploadCommissioningReject" value="{{ $dt->txtUploadCommissioning }}">
                                <input type="hidden" name="kodeCMF" value="{{ $dt->kodeCMF }}">
                            </div>
                          </div>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Pills -->
            
            <!-- /KONTEN -->
          </div>
        </div>
      </div>

        @include('Auth/footer')
        <!-- / Footer -->
    </div>
    <!-- / Layout page -->
  </div>

  <!-- Overlay -->
  <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->


<!-- Core JS -->
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/jquery-3.1.0.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/jquery/jquery.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/popper/popper.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/js/bootstrap.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/js/menu.js"></script>
<!-- endbuild -->

<!-- Vendors JS -->
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/apex-charts/apexcharts.js"></script>

<!-- Main JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/main.js"></script>

<!-- Page JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/dashboards-analytics.js"></script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
</body>
</html>

<script>
  function retrunPengajuan(){
    var btnTolakCMF = document.getElementById('btnTolakCMF');
    var btnSetujui = document.getElementById('btnSetujui');
    var btnKembali = document.getElementById('btnKembali');
    var labelPenolakan = document.getElementById('labelPenolakan');
    var comentNOKCMF = document.getElementById('comentNOKCMF');
    var btnBatalPenolakan = document.getElementById('btnBatalPenolakan');
    var btnKirimPenolakan = document.getElementById('btnKirimPenolakan');

    btnTolakCMF.setAttribute('style', 'display: none;');
    btnSetujui.setAttribute('style', 'display: none;');
    btnKembali.setAttribute('style', 'display: none;');
    labelPenolakan.removeAttribute('style');
    comentNOKCMF.removeAttribute('style');
    btnBatalPenolakan.removeAttribute('style');
    btnBatalPenolakan.setAttribute('style', 'float: right;');
    btnKirimPenolakan.removeAttribute('style');
    btnKirimPenolakan.setAttribute('style', 'float: right;');
  }
</script>

<script>
  function batalPenolakan(){
    var btnTolakCMF = document.getElementById('btnTolakCMF');
    var btnSetujui = document.getElementById('btnSetujui');
    var btnKembali = document.getElementById('btnKembali');
    var labelPenolakan = document.getElementById('labelPenolakan');
    var comentNOKCMF = document.getElementById('comentNOKCMF');
    var btnBatalPenolakan = document.getElementById('btnBatalPenolakan');
    var btnKirimPenolakan = document.getElementById('btnKirimPenolakan');

    btnTolakCMF.removeAttribute('style');
    btnTolakCMF.setAttribute('style', 'float: right;');
    btnSetujui.removeAttribute('style');
    btnSetujui.setAttribute('style', 'float: right;');
    btnKembali.removeAttribute('style');
    btnKembali.setAttribute('style', 'float: right;');
    labelPenolakan.setAttribute('style', 'display: none;');
    comentNOKCMF.setAttribute('style', 'display: none;');
    btnBatalPenolakan.setAttribute('style', 'display: none;');
    btnKirimPenolakan.setAttribute('style', 'display: none;');
  }
</script>


<script>
  function lainnya(){
    var lainnyaPerubahan = document.getElementById('lainnyaPerubahan');
    var valuePerubahanCMF = document.getElementById('valuePerubahanCMF');
    if(lainnyaPerubahan.checked == true){
      valuePerubahanCMF.removeAttribute('type');
    }else{
      valuePerubahanCMF.setAttribute('type', 'hidden');
    }
  }
</script>

<script>
  function lainnya2(){
    var lainnyaJenisPerubahan = document.getElementById('lainnyaJenisPerubahan');
    var valueJenisPerubahanCMF = document.getElementById('valueJenisPerubahanCMF');
    if(lainnyaJenisPerubahan.checked == true){
      valueJenisPerubahanCMF.removeAttribute('type');
    }else{
      valueJenisPerubahanCMF.setAttribute('type', 'hidden');
    }
  }
</script>

<script>
  $('#btn_submit').click(function() {
        var klausul = $('#klausul').val();
        var tgl_kejadian = $('#tgl_kejadian_ncr').val();
        var uraian_kejadian = $('#uraian_kejadian_ncr').val();
        var jenis_masalah = $('#jenis_masalah').val();
        var gambar_ncr = $('#gambar_ncr').val();


  });
</script>
