@include('Auth.header')
@include('Dc.navigation')
@include('Auth.topBar')      
     
     <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
          <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Dashboard / <a href="<?php echo route('Dc/JenisPerubahan'); ?>" class="text-muted fw-light">Jenis Perubahan</a> /</span> Edit</h4>

          <div class="row">
            <div class="col-md-12">
              <ul class="nav nav-pills flex-column flex-md-row mb-3">
                <li class="nav-item">
                </li>
              </ul>
              <div class="card mb-4">
                <h5 class="card-header">Data Jenis Perubahan</h5>
                  <form method="POST" action="<?php echo route('Query/updateJenisPerubahan'); ?>" enctype="multipart/form-data">
                   @csrf
                   <!-- Account -->
                  <hr class="my-0" />
                  <div class="card-body">
                    <div class="row">
                      <div class="mb-3 col-md-6">
                        <label for="firstName" class="form-label">Nama Jenis Perubahan</label>
                        <input
                          class="form-control"
                          type="text"
                          name="namaJenisPerubahan"
                          placeholder="Nama Jenis Perubahan"
                          required
                          value = "<?php echo $dt->namaJenisPerubahan ?>"
                        />
                      </div>
                      <div class="mb-3 col-md-6">
                        <label class="form-label" for="country">Status</label>
                        <select name="bitActiveJenisPerubahan" class="select2 form-select">
                           <option value="<?php echo $dt->bitActiveJenisPerubahan; ?>">
                             <?php 
                              if($dt->bitActiveJenisPerubahan == 1){
                                echo "AKTIF";
                              }else{
                                echo "NON AKTIF";
                              }
                              ?>
                           </option>
                          <option value="1">AKTIF</option>
                          <option value="0">NON AKTIF</option>
                        </select>
                      </div>
                    </div>
                    <div class="mt-2">
                      <button type="submit" class="btn btn-primary me-2">Save changes</button>
                      <button type="reset" class="btn btn-outline-secondary">Cancel</button>
                      <input type="hidden" name="feedBack" value="Dc/JenisPerubahan">
                      <input type="hidden" name="kodeJenisPerubahan" value="<?php echo $dt->kodeJenisPerubahan; ?>">
                    </div>
                  </div>
                <!-- /Account -->
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- / Content -->

        <!-- Footer -->
        @include('Auth/footer')
        
        <!-- / Footer -->

        <div class="content-backdrop fade"></div>
      </div>

<!-- Content wrapper -->
    </div>
    <!-- / Layout page -->
  </div>

  <!-- Overlay -->
  <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->


<!-- DATA TABLES ONLINE -->
<script src="https://code.jquery.com/jquery-3.7.0.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/jquery/jquery.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/popper/popper.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/js/bootstrap.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/js/menu.js"></script>
<!-- endbuild -->

<!-- Vendors JS -->
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/apex-charts/apexcharts.js"></script>

<!-- Main JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/main.js"></script>

<!-- Page JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/dashboards-analytics.js"></script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<script>new DataTable('#example');</script>
</body>
</html>
