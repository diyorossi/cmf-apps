@include('Auth.header')
@include('Dc.navigation')
@include('Auth.topBar')      
      
      
      <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
          <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Dashboard / <a href="{{ route('Dc/User') }}" class="text-muted fw-light">User</a> /</span> Tambah</h4>

          <div class="row">
            <div class="col-md-12">
              <ul class="nav nav-pills flex-column flex-md-row mb-3">
                <li class="nav-item">
                  <a class="nav-link active" href="javascript:void(0);"><i class="bx bx-user me-1"></i> Inputkan data profile yang akan Anda input sesuai dengan kaidah dan aturan yang sudah ditetapkan !</a>
                </li>
              </ul>
              <div class="card mb-4">
                <h5 class="card-header">Profile Details</h5>
                  <form method="POST" action="{{ route('Query/insertUser') }}" enctype="multipart/form-data">
                  <!-- Account -->
                  @csrf
                  <div class="card-body">
                    <div class="d-flex align-items-start align-items-sm-center gap-4">
                      <img
                        src="{{ asset('cmf') }}/assets/img/avatars/1.png"
                        alt="user-avatar"
                        class="d-block rounded"
                        height="100"
                        width="100"
                        id="uploadedAvatar"
                      />
                      <div class="button-wrapper">
                        <label for="upload" class="btn btn-primary me-2 mb-4" tabindex="0">
                          <span class="d-none d-sm-block">Upload new photo</span>
                          <i class="bx bx-upload d-block d-sm-none"></i>
                          <input
                            type="file"
                            id="upload"
                            name="gambarUser"
                            class="account-file-input"
                            hidden
                            accept="image/png, image/jpeg"
                          />
                        </label>
                        <button type="button" class="btn btn-outline-secondary account-image-reset mb-4">
                          <i class="bx bx-reset d-block d-sm-none"></i>
                          <span class="d-none d-sm-block">Reset</span>
                        </button>

                        <p class="text-muted mb-0">Allowed JPG, JPEG or PNG. </p>
                      </div>
                    </div>
                  </div>
                  <hr class="my-0" />
                  <div class="card-body">
                    <div class="row">
                      <div class="mb-3 col-md-6">
                        <label for="firstName" class="form-label">NIK</label>
                        <input
                          class="form-control"
                          type="text"
                          name="nik"
                          placeholder="NIK"
                          required
                        />
                      </div>
                      <div class="mb-3 col-md-6">
                        <label for="lastName" class="form-label">Nama Karyawan</label>
                        <input class="form-control" type="text" required name="namaKaryawan" placeholder="Nama Lengkap Karyawan" />
                      </div>
                      <div class="mb-3 col-md-6">
                        <label for="lastName" class="form-label">Nama Inisial</label>
                        <input class="form-control" type="text" required name="namaPanggilan" placeholder="Nama Inisial" />
                      </div>
                      <div class="mb-3 col-md-6">
                        <label for="organization" class="form-label">Job Title</label>
                        <input
                          type="text"
                          class="form-control"
                          name="posisi"
                          placeholder="Job Title"
                          required
                        />
                      </div>
                      <div class="mb-3 col-md-6">
                        <label class="form-label" for="country">Department</label>
                        <select name="department" class="select2 form-select">
                          @foreach ($allDepAktif as $dt)
                          
                            <option value="{{ $dt->kodeDepartmentMaster }}">{{ $dt->namaDepartment }}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="mb-3 col-md-6">
                        <label class="form-label" for="country">Area</label>
                        <select name="area" class="select2 form-select">
                          @foreach($allAreaAktif2 as $ds)
                            <option value="{{ $ds->kodeAreaTerkait }} ">{{ $ds->namaAreaTerkait }}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="mb-3 col-md-6">
                        <label for="email" class="form-label">E-mail</label>
                        <input
                          class="form-control"
                          type="text"
                          name="email"
                          placeholder="Email Kantor"
                          required
                        />
                      </div>
                      <div class="mb-3 col-md-6">
                        <label class="form-label" for="country">Role Akses</label>
                        <select name="levelCMF" class="select2 form-select">
                          @foreach($allLevel as $dt2)
                            <option value="{{ $dt2->kodeLevel }}">{{ $dt2->namaLevel }}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="mb-3 col-md-6">
                        <label class="form-label" for="country">Password</label>
                        <input
                          class="form-control"
                          type="text"
                          name="password"
                          placeholder="Password"
                          required
                        />
                      </div>
                      <div class="mb-3 col-md-6">
                        <label class="form-label" for="country">Username Komputer</label>
                        <input
                          class="form-control"
                          type="text"
                          name="username"
                          placeholder="Username Login Komputer"
                          required
                        />
                      </div>
                    </div>
                    <div class="mt-2">
                      <button type="submit" class="btn btn-primary me-2">Save changes</button>
                      <button type="reset" class="btn btn-outline-secondary">Cancel</button>
                      <input type="hidden" name="feedBack" value="Dc/User">
                      <input type="hidden" name="gambarUserReject" value="a">
                    </div>
                  </div>
                <!-- /Account -->
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- / Content -->

        <!-- Footer -->
        @include('Auth/footer')
        
        <!-- / Footer -->

        <div class="content-backdrop fade"></div>
      </div>

<!-- Content wrapper -->
    </div>
    <!-- / Layout page -->
  </div>

  <!-- Overlay -->
  <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->


<!-- Core JS -->
<!-- build:js assets/vendor/js/core.js -->
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/jquery/jquery.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/popper/popper.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/js/bootstrap.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/js/menu.js"></script>
<!-- endbuild -->

<!-- Vendors JS -->

<!-- Main JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/main.js"></script>

<!-- Page JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/pages-account-settings-account.js"></script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
</body>
</html>
