@include('Auth.header')
@include('Dc.navigation')
@include('Auth.topBar')  

      <!-- Content wrapper -->
      <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
          <div class="row">
            <!-- KONTEN -->

            <div class="container-xxl flex-grow-1 container-p-y">
              <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Dashboard /</span> Jenis Perubahan 
                <a href="<?php echo route('Dc/JenisPerubahanAdd') ?>" class="btn rounded-pill btn-primary" style="float: right;">
                  <span class="bx bx-plus me-sm-1"></span>&nbsp; Tambah
                </a>
              </h4>
              @if(Session::has('fail'))
                <div class="alert alert-danger">
                    {{Session::get('fail')}}
                </div>
              @elseif(Session::has('success'))
                <div class="alert alert-success">
                    {{Session::get('success')}}
                </div>
              @endif
              <div class="card">
                <h5 class="card-header">LIST KATEGORI JENIS PERUBAHAN CHANGE MANAGEMENT</h5>
                <div class="tab-content">
                  <table id="example" class="display" style="width:100%">
                    <thead class="text-nowrap">
                      <tr>
                        <th>#</th>
                        <th>NAMA JENIS PERUBAHAN</th>
                        <th>STATUS</th>
                        <th>ACTION</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php 
                      $no = 1;
                      foreach ($allJenisPerubahan as $dt){
                     ?>
                      <tr>
                        <th><?php echo $no ++; ?></th>
                        <td><?php echo $dt->namaJenisPerubahan; ?></td>
                        <td>
                          <?php 
                            if($dt->bitActiveJenisPerubahan == 1){

                           ?>
                           <span class="badge bg-label-success me-1">AKTIF</span>
                         <?php }else{ ?>
                           <span class="badge bg-label-danger me-1">NON AKTIF</span>
                         <?php } ?>
                        </td>
                        <td><a href="<?php echo route('Dc/JenisPerubahanEdit'); ?>?d=<?php echo $dt->kodeJenisPerubahan; ?>" class="btn rounded-pill btn-icon btn-primary">
                              <span class="tf-icons bx bx-pen"></span>
                            </a></td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          <!-- /KONTEN -->
          </div>
        </div>
      </div>

        <!-- Footer -->
        @include('Auth/footer')
        <!-- / Footer -->
    </div>
    <!-- / Layout page -->
  </div>

  <!-- Overlay -->
  <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->


<!-- DATA TABLES ONLINE -->
<script src="https://code.jquery.com/jquery-3.7.0.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/jquery/jquery.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/popper/popper.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/js/bootstrap.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/js/menu.js"></script>
<!-- endbuild -->

<!-- Vendors JS -->
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/apex-charts/apexcharts.js"></script>

<!-- Main JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/main.js"></script>

<!-- Page JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/dashboards-analytics.js"></script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<script>new DataTable('#example');</script>
</body>
</html>
