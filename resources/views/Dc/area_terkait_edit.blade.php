@include('Auth.header')
@include('Dc.navigation')
@include('Auth.topBar')

     <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
          <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Dashboard / <a href="<?php echo route('Dc/AreaTerkait'); ?>" class="text-muted fw-light">Area Terkait</a> /</span> Edit</h4>

          <div class="row">
            <div class="col-md-12">
              <ul class="nav nav-pills flex-column flex-md-row mb-3">
                <li class="nav-item">
                </li>
              </ul>
              <div class="card mb-4">
                <h5 class="card-header">Data Area Terkait</h5>
                  <form method="POST" action="<?php echo route('Query/updateAreaTerkait'); ?>" enctype="multipart/form-data">
                   @csrf
                   <!-- Account -->
                  <hr class="my-0" />
                  <div class="card-body">
                    <div class="row">
                      <div class="mb-3 col-md-6">
                        <label for="firstName" class="form-label">Nama Area Terkait</label>
                        <input
                          class="form-control"
                          type="text"
                          name="namaAreaTerkait"
                          placeholder="Nama Area Terkait"
                          required
                          value="<?php echo $dt->namaAreaTerkait; ?>"
                        />
                      </div>
                      <div class="mb-3 col-md-6">
                        <label class="form-label" for="country">Department</label>
                        <select name="kodeDepartment" class="select2 form-select">
                          <option value="<?php echo $dt->kodeDepartment ?>"><?php echo $dt->namaDepartment; ?></option>
                          <?php 
                            foreach($allDepAktif as $dt2){
                           ?>
                          <option value="<?php echo $dt2->kodeDepartmentMaster; ?>"><?php echo $dt2->namaDepartment; ?></option>
                         <?php } ?>
                        </select>
                      </div>
                      <div class="mb-3 col-md-6">
                        <label class="form-label" for="country">Status</label>
                        <select name="bitActiveArea" class="select2 form-select">
                           <option value="<?php echo $dt->bitActiveArea; ?>">
                             <?php 
                              if($dt->bitActiveArea == 1){
                                echo "AKTIF";
                              }else{
                                echo "NON AKTIF";
                              }
                              ?>
                           </option>
                          <option value="1">AKTIF</option>
                          <option value="0">NON AKTIF</option>
                        </select>
                      </div>
                    </div>
                    <div class="mt-2">
                      <button type="submit" class="btn btn-primary me-2">Save changes</button>
                      <button type="reset" class="btn btn-outline-secondary">Cancel</button>
                      <input type="hidden" name="feedBack" value="Dc/AreaTerkait">
                      <input type="hidden" name="kodeAreaTerkait" value="<?php echo $dt->kodeAreaTerkait ?>">
                    </div>
                  </div>
                <!-- /Account -->
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- / Content -->

        <!-- Footer -->
        @include('Auth/footer')
        
        <!-- / Footer -->

        <div class="content-backdrop fade"></div>
      </div>

<!-- Content wrapper -->
    </div>
    <!-- / Layout page -->
  </div>

  <!-- Overlay -->
  <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->


<!-- Core JS -->
<!-- build:js assets/vendor/js/core.js -->
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/jquery/jquery.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/popper/popper.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/js/bootstrap.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/js/menu.js"></script>
<!-- endbuild -->

<!-- Vendors JS -->
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/apex-charts/apexcharts.js"></script>

<!-- Main JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/main.js"></script>

<!-- Page JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/dashboards-analytics.js"></script>
</body>
</html>
