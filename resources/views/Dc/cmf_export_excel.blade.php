


<!DOCTYPE html>
<html>
<head>
	<title>Export to Excel</title>
	<style type="text/css">
		.table1 {
		    font-family: sans-serif;
		    color: #232323;
		    border-collapse: collapse;
		}
		 
		.table1, th, td {
		    border: 1px solid #999;
		    padding: 8px 20px;
		    text-align: center;
		}
	</style>
</head>
<body>
    <?php
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=ALL CMF.xls");
	?>
	<h1 align="center">Export Data to Excel</h1>
	
	<br>
	

	<table class="table1" align="center">
		<thead style="background-color: skyblue;">
			<tr>
				<th>NO</th>
				<th>KODE CMF</th>
				<th>NOMOR CMF</th>
                <th>PEMILIK PROSES CMF</th>
                <th>TANGGAL CMF</th>
                <th>DEPARTMENT CMF</th>
                <th>AREA CMF</th>
                <th>JUDUL PERUBAHAN</th>
                <th>TANGGAL IMPLEMENTASI CMF</th>
                <th>TYPE CMF</th>
                <th>ALASAN PERUBAHAN</th>
                <th>DAMPAK PERUBAHAN</th>
                <th>DESKRIPSI PERUBAHAN</th>
                <th>UPLOAD DOKUMEN</th>
                <th>ATASAN PEMILIK PROSES</th>
                <th>TANGGAL APPROVE ATASAN</th>
                <th>CATATAN ATASAN PEMILIK PROSES</th>
                <th>DC</th>
                <th>TANGGAL APPROVE DC</th>
                <th>SPV SYSTEM</th>
                <th>TANGGAL APPROVE SPV SYSTEM</th>
                <th>MR</th>
                <th>TANGGAL APPROVE MR</th>
                <th>MNF. HEAD</th>
                <th>TANGGAL APPROVE MNF. HEAD</th>
                <th>APPROVE DC SETELAH PERUBAHAN</th>
                <th>TANGGAL APPROVE DC SETELAH PERUBAHAN</th>
                <th>PEMILIK PROSES (Setelah Perubahan)</th>
                <th>TANGGAL PEMILIK PROSES (Setelah Perubahan)</th>
                <th>DETAIL AKTIFITAS</th>
                <th>DOKUMEN COMMISSIONING</th>
                <th>ATASAN PEMILIK PROSES (Setelah Perubahan)</th>
                <th>TANGGAL APPROVE ATASAN (Setelah Perubahan)</th>
                <th>VERIFIKASI TEM FOOD SAFETY</th>
                <th>TANGGAL VERIFIKASI</th>
                <th>VERIFIKASI DOCUMENT</th>
                <th>TANGGAL VERIFIKASI DC</th>
                <th>MR</th>
                <th>TANGGAL MR</th>
                <th>PIC KOREKSI CMF</th>
                <th>TANGGAL KOREKSI</th>
                <th>ALASAN KOREKSI</th>
                <th>NOMOR CAPEX</th>
			</tr>
		</thead>
		@php
            $no = 1;
            @endphp
            @foreach ($cmf as $dt)
		<tbody>
			<tr>
				<td><?php echo $no++; ?></td>
				<td><?php echo $dt->kodeCMF; ?></td>
				<td><?php echo $dt->nomorCMF; ?></td>
				<td><?php echo $dt->namaKaryawan; ?></td>
				<td><?php echo date('d/m/Y', strtotime($dt->dateCMF)); ?></td>
				<td><?php echo $dt->namaDepartment; ?></td>
				<td><?php echo $dt->namaAreaTerkait; ?></td>
				<td><?php echo $dt->judulPerubahanCMF; ?></td>
				<td><?php echo date('d/m/Y', strtotime($dt->dateImplementasiCMF)); ?></td>
				<td>
                    @php
                        if($dt->typePerubahanCMF == 1){
                            echo 'Temporary';
                        }elseif($dt->typePerubahanCMF == 2){
                            echo 'Permanent';
                        }
                    @endphp
                </td>
				<td><?php echo $dt->alasanPerubahanCMF; ?></td>
				<td><?php echo $dt->dampakPerubahanCMF; ?></td>
				<td><?php echo $dt->deskripsiPerubahanCMF; ?></td>
				<td><?php echo $dt->txtUploadDocument; ?></td>
				<td><?php echo $dt->approveCMF1; ?></td>
				<td><?php echo date('d/m/Y', strtotime($dt->dateApproveCMF1)); ?></td>
				<td><?php echo $dt->catatanDeptheadCMF1; ?></td>
				<td><?php echo $dt->approveCMFDC; ?></td>
				<td><?php echo date('d/m/Y', strtotime($dt->dateApproveCMFDC)); ?></td>
				<td><?php echo $dt->approveCMF2; ?></td>
				<td><?php echo date('d/m/Y', strtotime($dt->dateApproveCMF2)); ?></td>
                <td><?php echo $dt->approveCMF3; ?></td>
				<td><?php echo date('d/m/Y', strtotime($dt->dateApproveCMF3)); ?></td>
                <td><?php echo $dt->approveCMF4; ?></td>
				<td><?php echo date('d/m/Y', strtotime($dt->dateApproveCMF4)); ?></td>
                <td><?php echo $dt->approveCMF5; ?></td>
				<td><?php echo date('d/m/Y', strtotime($dt->dateApproveCMF5)); ?></td>
                <td><?php echo $dt->approveCMF6; ?></td>
				<td><?php echo date('d/m/Y', strtotime($dt->dateApproveCMF6)); ?></td>
				<td><?php echo $dt->detailAktifitasCMF; ?></td>
				<td><?php echo $dt->txtUploadCommissioning; ?></td>
                <td><?php echo $dt->approveCMF7; ?></td>
				<td><?php echo date('d/m/Y', strtotime($dt->dateApproveCMF7)); ?></td>
                <td><?php echo $dt->approveCMF8; ?></td>
				<td><?php echo date('d/m/Y', strtotime($dt->dateApproveCMF8)); ?></td>
                <td><?php echo $dt->approveCMF9; ?></td>
				<td><?php echo date('d/m/Y', strtotime($dt->dateApproveCMF9)); ?></td>
                <td><?php echo $dt->approveCMF10; ?></td>
				<td><?php echo date('d/m/Y', strtotime($dt->dateApproveCMF10)); ?></td>
                <td><?php echo $dt->picNOKCMF; ?></td>
				<td><?php echo date('d/m/Y', strtotime($dt->dateNOKCMF)); ?></td>
				<td><?php echo $dt->comentNOKCMF; ?></td>
				<td><?php echo $dt->nomorCapex; ?></td>
			</tr>
		</tbody>
        @endforeach
	</table>
</body>
</html>