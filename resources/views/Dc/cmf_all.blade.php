@include('Auth.header')
@include($role.'/navigation')
@include('Auth.topBar')

      <!-- Content wrapper -->
      <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
          <div class="row">
            <!-- KONTEN -->

            <div class="container-xxl flex-grow-1 container-p-y">
              <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Dashboard /</span> Semua CMF
              </h4>
              <div class="col-lg-12 col-md-4 order-1">
                <div class="row">
                    <div class="col-lg-9 mb-4 order-0">
                        <div class="card">
                            <div class="d-flex align-items-end row">
                            <div class="col-sm-7">
                                <div class="card-body">
                                <h5 class="card-title text-primary">Rekap CMF Semua Tahun ! 🎉</h5>
                                <p class="mb-4">
                                    Total Keseluruhan Ada <span class="fw-bold">{{ $cmf_count }} CMF</span> Dari Tahun 2011 Hingga Tahun {{ date('Y'); }}
                                </p>
            
                                <a href="{{ route($role.'/SemuaCMFAllYear') }}" class="btn btn-sm btn-outline-primary">Klik Masuk</a>
                                </div>
                            </div>
                            <div class="col-sm-5 text-center text-sm-left">
                                <div class="card-body pb-0 px-0 px-md-4">
                                <img
                                    src="<?php echo asset('cmf') ?>/assets/img/illustrations/man-with-laptop-light.png"
                                    height="140"
                                    alt="View Badge User"
                                    data-app-dark-img="illustrations/man-with-laptop-dark.png"
                                    data-app-light-img="illustrations/man-with-laptop-light.png"
                                />
                                </div>
                            </div>
                            </div>
                        </div>
                    </div><br>
              @foreach ($cmf as $dt) 
              @php
                  $tahun = $dt->tahun;
                  $statusRiwayat = array(13, 14);
                  $queryCount = DB::table('tr_cmf')
                                ->selectRaw('COUNT(kodeCMF) as jumlahCMF, YEAR(dateCMF) as tahunCMF')
                                ->whereYear('dateCMF', $tahun)
                                ->whereIn('statusProsesCMF', $statusRiwayat)
                                ->groupBy('tahunCMF')
                                ->get();
              @endphp
                    @foreach($queryCount as $ds)
              
              <div class="col-lg-4 mb-4 order-0">
                <div class="card">
                  <div class="d-flex align-items-end row">
                    <div class="col-sm-7">
                      <div class="card-body">
                        <h5 class="card-title text-primary">Rekap CMF Tahun {{ $ds->tahunCMF }} 🎉</h5>
                        <p class="mb-4">
                          Total <span class="fw-bold">{{ $ds->jumlahCMF }} CMF</span> 
                        </p>
  
                        <a href="{{ route($role.'/SemuaCMFTahun') }}?d={{ $ds->tahunCMF }}" class="btn btn-sm btn-outline-primary">Klik Masuk</a>
                      </div>
                    </div>
                    <div class="col-sm-5 text-center text-sm-left">
                      <div class="card-body pb-0 px-0 px-md-0">
                        <img
                          src="<?php echo asset('cmf') ?>/assets/img/illustrations/girl-doing-yoga-light.png"
                          height="140"
                          style="margin-left: -40px;"
                          alt="View Badge User"
                          data-app-dark-img="illustrations/man-with-laptop-dark.png"
                          data-app-light-img="illustrations/man-with-laptop-light.png"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
              @endforeach
            </div>
            </div>
        </div>
          <!-- /KONTEN -->
          </div>
        </div>
      </div>

        @include('Auth/footer')
        <!-- / Footer -->
    </div>
    <!-- / Layout page -->
  </div>

  <!-- Overlay -->
  <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->


<!-- DATA TABLES ONLINE -->
<script src="https://code.jquery.com/jquery-3.7.0.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/jquery/jquery.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/popper/popper.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/js/bootstrap.js"></script>
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

<script src="<?php echo asset('cmf') ?>/assets/vendor/js/menu.js"></script>
<!-- endbuild -->

<!-- Vendors JS -->
<script src="<?php echo asset('cmf') ?>/assets/vendor/libs/apex-charts/apexcharts.js"></script>

<!-- Main JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/main.js"></script>

<!-- Page JS -->
<script src="<?php echo asset('cmf') ?>/assets/js/dashboards-analytics.js"></script>
<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>

<script>new DataTable('#example');</script>
</body>
</html>
